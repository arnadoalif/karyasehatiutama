<!DOCTYPE html>
<html lang="en">
  <head>
    <?php require_once(APPPATH .'views/include/admin/admin_style.php'); ?>
    <style type="text/css">
      .view .tools {
          color: #fff;
          text-align: center;
          font-size: 17px;
          padding: 7px;
          background: rgba(0,0,0,.35);
          margin: 95px 0 0;
      }
      .view .content, .view .mask {
          position: absolute;
          width: 100%;
          height: 100%;
          overflow: hidden;
          top: 0;
          left: 0;
      }
      .thumbnail .image {
          height: 180px;
          overflow: hidden;
      }
    </style>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url('admin/home') ?>" class="site_title"><span>Karya Sehati Utama</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url('asset_admin/images/Logo_KSU.png') ?> " alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>Administrator</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <?php $this->load->view('adminpages/menu_bar'); ?>

          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <?php $this->load->view('adminpages/nav_menu'); ?>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              
              <div class="title_left">
                <h3>Slide Show</h3>
              </div>
            </div>

            <div class="clearfix"></div>
            
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <a class="btn btn-primary btn-sm" data-toggle="modal" href='#modal-id'><i class="fa fa-plus"></i> Add Slide</a>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      
                        <?php $i= 1; foreach ($data_slide as $dt_slide): ?>
                          <div class="col-md-6">
                            <div class="thumbnail">
                              <div class="image view view-first">
                                <img style="width: 100%; display: block;" src="<?php echo base_url('storage_img/img_slide/'.$dt_slide->Cover_name); ?>" alt="image" />
                                <div class="mask">
                                  <p>Slide  <?php echo $i++ ?> </p>
                                  <div class="tools tools-bottom">
                                    <!-- <a data-toggle="modal" href='#modal-id<?php echo $dt_g_proyek->Kd_Gproyek ?>'><i class="fa fa-pencil"></i></a> -->
                                    <a href="<?php echo base_url('admin/remove_slide/'.$dt_slide->Kd_Slide); ?>"><i class="fa fa-trash"></i> Hapus slide </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        <?php endforeach ?>

                        <div class="modal fade" id="modal-id">
                          <div class="modal-dialog">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Add Slide</h4>
                              </div>
                              <form action="<?php echo base_url('admin/action_save_slide'); ?>" method="POST" role="form" enctype="multipart/form-data">
                                <div class="modal-body">
                                    <div class="form-group">
                                      <label for="">Judul / Name</label>
                                      <input type="text" name="judul_slide" class="form-control" id="" placeholder="Name Slide">
                                    </div>
                                    <div class="form-group">
                                      <label for="">Cover </label>
                                      <input type="file" name="cover_slide" class="form-control" id="" placeholder="">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  <button type="submit" class="btn btn-primary">Upload</button>
                                </div>
                              </form>
                            </div>
                          </div>
                        </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <?php $this->load->view('adminpages/footer'); ?>
      </div>
    </div>

    

    <?php require_once(APPPATH .'views/include/admin/admin_script.php'); ?>
  </body>
</html>