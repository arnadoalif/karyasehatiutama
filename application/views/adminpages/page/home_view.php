<!DOCTYPE html>
<html lang="en">
  <head>
    <?php require_once(APPPATH .'views/include/admin/admin_style.php'); ?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url('admin/home') ?>" class="site_title"><span>Karya Sehati Utama</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url('asset_admin/images/Logo_KSU.png') ?> " alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>Administrator</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <?php $this->load->view('adminpages/menu_bar'); ?>

          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          
          <?php $this->load->view('adminpages/nav_menu'); ?>

        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          
          <!-- top tiles -->
          <div class="row tile_count">
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top red">New Pesan</span>
              <div class="count"><i class="red fa fa-envelope-o"></i> <?php echo $count_pesan ?></div>
              <span class="count_bottom"><a href="<?php echo base_url('admin/pesan'); ?>" title=""><i class="fa fa-arrow-right"></i> Lihat Detail</a></span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"> Produk</span>
              <div class="count"><i class="fa fa-building"></i> <?php echo $count_produk ?></div>
              <span class="count_bottom"><a href="<?php echo base_url('admin/new_property'); ?>" title=""><i class="fa fa-arrow-right"></i> Lihat Detail</a></span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"> Property</span>
              <div class="count green"><i class="fa fa-home"></i> <?php echo $count_property ?></div>
               <span class="count_bottom"><a href="<?php echo base_url('admin/data-type-produk'); ?>" title=""><i class="fa fa-arrow-right"></i> Lihat Detail</a></span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"> Portfolio </span>
              <div class="count"><i class="fa fa-trophy"></i> <?php echo $count_portfolio; ?></div>
              <span class="count_bottom"><a href="<?php echo base_url('admin/portofolio-property'); ?>" title=""><i class="fa fa-arrow-right"></i> Lihat Detail</a></span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"> Testimoni </span>
              <div class="count"><i class="fa fa-comments"></i> <?php echo $count_testimoni; ?></div>
              <span class="count_bottom"><a href="<?php echo base_url('admin/testimoni'); ?>" title=""><i class="fa fa-arrow-right"></i> Lihat Detail</a></span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"> Comming </span>
              <div class="count"><i class="fa fa-cogs"></i> 0</div>
              <span class="count_bottom"><a href="" title=""><i class="fa fa-arrow-right"></i> Lihat Detail</a></span>
            </div>
          </div>
          <!-- /top tiles -->

          <div class="row">
            <hr>
            <div class="bs-callout bs-callout-info">
              <h4>Sekilas Info</h4>
              Selamat Datang di dashboard, dengan ini kami ingin memberi tahu dalam penggunaan system dashboard ini 
            </div>
            <div class="bs-callout bs-callout-warning">
              <h4>Warning</h4>
              <ul>
                  <li>Jangan tekan tombol hapus, tomboh hapus masih di bawah mantenance programmer</li>
                  <li>Jika Menemukan icon symbol berikut <strong><i class="fa fa-cogs"></i></strong> system sedang di proses building</li>
                  <li>Jika mengalami <strong>Bug (Error) bisa beri tahu web programmer dengan menekan tombol berikut </strong> <a class="btn btn-sm btn-warning" href="https://api.whatsapp.com/send?phone=6282137984316" target="_blank" role="button"><i class="fa fa-exclamation-triangle"></i></a> Chat Programmer </li>
                  <li>System versi beta, dalam uji coba system</li>
              </ul>
            </div>
            <div class="bs-callout bs-callout-info">
                <h4>* Penjelasan Singkat</h4>
                <ul>
                   <li>- <span class="label label-success">[Link]</span> dapat ketuk link tersebut untuk melihat detail deskripsi produk</li>
                   <li>- <a class="btn btn-default btn-xs disabled" href="#" role="button"><i class="fa fa-upload"></i>Upload </a> Digunakan untuk upload brosur berupa link, link yang di kait kan dengan meletakan pada google drive dan menyingkat link dengan berikut <span class="label label-info"><a href="https://goo.gl/" target="_blank">Singkat Link</a></span></li>
                   <li>- <a class="btn btn-success btn-xs disabled" href="#" role="button"><i class="fa fa-download"></i> Download </a> digunakan untuk download brosur</li>
                </ul>
            </div>
            <h4>Produk</h4>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <table id="datatable" class="table table-hover">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Logo</th>
                    <th>Cover</th>
                    <th>Name & Detail View </th>
                    <th>Status</th>
                    <th>Upload & Edit</th>
                    <th>Download Brosur</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $i=1; foreach ($data_produk as $dt_produk): ?>
                  <tr>
                    <td><?php echo $i++; ?></td>
                    <td><img width="80" height="80" src="<?php echo base_url('storage_img/img_logo_produk/'.$dt_produk->Logo_Produk); ?>" class="img-responsive" alt="Image"></td>
                    <td><img width="50" height="50" src="<?php echo base_url('storage_img/img_cover_produk/'.$dt_produk->Cover_Produk); ?>" class="img-responsive" alt="<?php echo $dt_produk->Nama_Produk ?>"></td>
                    <td> 
                      <?php echo $dt_produk->Nama_Produk ?> 
                      <p><span class="label label-success"><a style="color: #fff;" target="_blank" href="<?php echo base_url('page/detail/'.$dt_produk->Slug_name); ?>" title=""> <?php echo base_url('page/detail/'.$dt_produk->Slug_name); ?> </a></span></p>
                    </td>
                    <td>
                      <?php if ($dt_produk->Status == 1): ?>
                        <span class="label label-info">Publish</span>
                      <?php else: ?>
                        <span class="label label-danger">Suspanse</span>
                      <?php endif ?>
                    </td>
                    <td>
                      <a class="btn btn-default btn-xs" data-toggle="modal" href='#modal-id<?php echo $dt_produk->Kd_Produk ?>' role="button"><i class="fa fa-upload"></i> Upload & Edit</a>
                    </td>
                    <td>
                      <?php if ($dt_produk->Brosur_name == ""): ?>
                        <a class="btn btn-danger disabled btn-xs" href="#" role="button"><i class="fa fa-download"></i> Download</a>
                      <?php else: ?>
                        <a class="btn btn-success btn-xs" href="<?php echo $dt_produk->Brosur_name ?>" target="_blank" role="button"><i class="fa fa-download"></i> Download</a>
                      <?php endif ?>
                    </td>
                  </tr>

                  <div class="modal fade" id="modal-id<?php echo $dt_produk->Kd_Produk ?>">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                          <h4 class="modal-title">Upload Browsur</h4>
                        </div>
                        <form action="<?php echo base_url('admin/upload_brosur_produk'); ?>" method="post" enctype="multipart/form-data">
                          <div class="modal-body">
                            <div class="form-group">
                              <label>Upload & Edit Link Brosur</label>
                              <input type="text" name="file_brosur" class="form-control" value="<?php echo $dt_produk->Brosur_name ?>" required="required" placeholder="Link Brosur">
                              <input type="hidden" name="kode_produk" class="form-control" value="<?php echo $dt_produk->Kd_Produk ?>" required="required">
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup Jendela</button>
                            <button type="submit" class="btn btn-primary">Upload</button>
                          </div>
                        </form>

                      </div>
                    </div>
                  </div>
                  <?php endforeach ?>
                </tbody>
              </table>
            </div>
          </div>

        </div>
        <!-- /page content -->

        <?php $this->load->view('adminpages/footer'); ?>
      </div>
    </div>

    

    <?php require_once(APPPATH .'views/include/admin/admin_script.php'); ?>
  </body>
</html>