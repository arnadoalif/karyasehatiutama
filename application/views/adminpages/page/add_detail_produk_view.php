<!DOCTYPE html>
<html lang="en">
  <head>
    <?php require_once(APPPATH .'views/include/admin/admin_style.php'); ?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url('admin/home') ?>" class="site_title"><span>Karya Sehati Utama</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url('asset_admin/images/Logo_KSU.png') ?> " alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>Administrator</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <?php $this->load->view('adminpages/menu_bar'); ?>

          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <?php $this->load->view('adminpages/nav_menu'); ?>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">

          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Setting Detail Produk <strong> <?php echo $data_produk->Nama_Produk ?></strong></h3>

              </div>
            </div>
            <span class="pull-right"><img width="100" height="100" src="<?php echo base_url('storage_img/img_logo_produk/'.$data_produk->Logo_Produk); ?>" class="img-responsive" alt="Image"> </span>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">

                  <?php if (isset($_SESSION['message_data'])): ?>
                    <div class="alert alert-success" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">×</span>
                      </button>
                      <?php echo $_SESSION['message_data'] ?>
                    </div>
                    <?php endif ?>

                    <?php if (isset($_SESSION['error_data'])): ?>
                    <div class="alert alert-danger" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">×</span>
                      </button>
                      <?php echo $_SESSION['error_data'] ?>
                    </div>
                  <?php endif ?>

                  <div class="x_title">
                    <h2>Setting Slide</h2>
                    <div class="clearfix"></div>
                    <div class="bs-callout bs-callout-info">
                        <h4>* Penjelasan Singkat</h4>
                        <ul>
                            <li>- Upload gambar dapat dilakukan banyak slide dengan ukuran <strong><i>(1200 x 601)</i></strong></li>
                            <li>- <a class="btn btn-xs btn-primary" href="#" role="button">Data Slide</a> menampilkan data slide yang di upload, dan untuk mengubah slide</li>
                        </ul>
                    </div>
                  </div>
                  <div class="x_content">
                     <form action="<?php echo base_url('admin/action_save_detail_produk_slide'); ?>" method="post" enctype="multipart/form-data">
                       
                       <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                           <div class="form-group">
                             <label for="">Upload Office View</label>
                             <input type="file" multiple name="img_slide[]" class="form-control" id="images" onchange="preview_images()" placeholder="">
                             <input type="hidden" name="kode_produk" value="<?php echo $kode_produk ?>" placeholder="">
                           </div>
                       </div>
                       <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group">
                               <label for="">Preview Slide</label>
                               <div id="slide"></div>
                            </div>
                       </div>
                       <div class="col-lg-12">
                        <div class="form-group pull-right">
                          <a class="btn btn-primary" data-toggle="modal" href='#data-slide'>Data Slide</a>
                          <button type="reset" class="btn btn-danger">Reset</button>
                          <button type="submit" class="btn btn-success">Save</button>
                        </div>
                       </div>

                     </form>


                     <div class="modal fade" id="data-slide">
                       <div class="modal-dialog modal-lg">
                         <div class="modal-content">
                           <div class="modal-header">
                             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                             <h4 class="modal-title">Data Slide</h4>
                           </div>
                           <div class="modal-body">
                             <div class="table-responsive">
                               <table class="table table-hover">
                                 <thead>
                                   <tr>
                                     <th><i class="fa fa-trash"></i> Hapus </th>
                                     <th><i class="fa fa-image"></i></th>
                                     <th> <i class="fa fa-pencil"></i> Ubah </th>
                                   </tr>
                                 </thead>
                                 <tbody>
                                  <?php foreach ($data_berkas as $dt_berkas): ?>
                                   <tr>
                                     <td><a class="btn btn-sm btn-danger" href="<?php echo base_url('admin/remove_detail_slide/'.$dt_berkas->Kd_Berkas.'/'.$data_produk->Kd_Produk.''); ?>" role="button"><i class="fa fa-trash"></i> Hapus</a></td>
                                     <td>
                                       <img width="150" height="150" src="<?php echo base_url('storage_img/img_slide_produk/'.$dt_berkas->Nama_img); ?>" class="img-responsive" alt="Image">
                                     </td>
                                     <td><a class="btn btn-info" data-toggle="modal" href="#ubah-slide<?php echo $dt_berkas->Kd_Berkas ?>" role="button">Ubah</a></td>
                                   </tr>

                                   <div class="modal fade" id="ubah-slide<?php echo $dt_berkas->Kd_Berkas ?>">
                                     <div class="modal-dialog">
                                       <div class="modal-content">
                                         <div class="modal-header">
                                           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                         </div>
                                         <div class="modal-body">
                                           <form action="<?php echo base_url('admin/action_edit_detail_produk_slide'); ?>" method="POST" enctype="multipart/form-data">
                                           
                                             <div class="form-group">
                                               <label for="">Upload New Slide</label>
                                               <input type="file" name="new_slide" class="form-control" id="">
                                               <input type="hidden" name="kode_slide" id="inputKode_slide" class="form-control" value="<?php echo $dt_berkas->Kd_Berkas ?>" required="required">
                                               <input type="hidden" name="kode_produk" id="inputKode_produk" class="form-control" value="<?php echo $data_produk->Kd_Produk ?>" required="required">
                                             </div>
                                           
                                             <button type="submit" class="btn btn-primary">Ubah Slide</button>
                                           </form>
                                         </div>
                                       </div>
                                     </div>
                                   </div>
                                   <?php endforeach ?>
                                 </tbody>
                               </table>
                             </div>
                           </div>
                           <div class="modal-footer">
                             <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                           </div>
                         </div>
                       </div>
                     </div>

                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Detail Deskripsi Lengkap Produk</h2>
                    <div class="clearfix"></div>
                      <div class="bs-callout bs-callout-info">
                        <h4>* Penjelasan Singkat</h4>
                        <ul>
                            <li>- Tuliskan deskripsi lengkap tentang produk ini</li>
                            <li>- Penjelasan ini akan di tampilkan pada detail produk, ketika halaman detail produk di tampilkan</li>
                        </ul>
                      </div>
                  </div>
                  <div class="x_content">
                     <form action="<?php echo base_url('admin/action_update_data_deskripsi_lengkap'); ?>" method="post" enctype="multipart/form-data">
                       
                       <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                           <div class="form-group">
                               <label for="">Detail Deskripsi Produk</label>
                               <textarea name="deskripsi_2" id="ckeditor4" class="form-control" rows="5" required="required"><?php echo $data_produk->Deskripsi_1; ?></textarea>
                               <input type="hidden" name="kode_produk" id="inputKode" class="form-control" value="<?php echo $data_produk->Kd_Produk ?>" required="required" pattern="" title="">
                            </div>
                       </div>
                       
                       <div class="col-lg-12">
                        <div class="form-group pull-right">
                          <!-- <button type="reset" class="btn btn-danger">Reset</button> -->
                          <button type="submit" class="btn btn-success">Update</button>
                        </div>
                       </div>

                     </form>

                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Spesifikasi Produk</h2>
                    <div class="clearfix"></div>
                    <div class="bs-callout bs-callout-info">
                        <h4>* Penjelasan Singkat</h4>
                        <ul>
                            <li>- Spesifikasi Grid 1 <strong><i>(pondasi, dinding, lantai, atap)</i></strong></li>
                            <li>- Spesifikasi Grid 2 <strong><i>(kusen,pintu & jendela, plafond)</i></strong></li>
                            <li>- Spesifikasi Grid 3 <strong><i>(kamar mandi, sanitair, airbersihm elektrikal)</i></strong></li>
                        </ul>
                    </div>
                  </div>
                  <div class="x_content">
                     <form action="<?php echo base_url('admin/action_update_data_spesifikasi'); ?>" method="post" enctype="multipart/form-data">
                       
                       <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                           <div class="form-group">
                               <label for="">Spesifikasi Grid 1</label>
                               <textarea name="spesifikasi_grid_1" id="ckeditor1" class="form-control" rows="5" required="required"><?php echo $data_produk->Grid_1; ?></textarea>
                               <input type="hidden" name="kode_produk" id="inputKode" class="form-control" value="<?php echo $data_produk->Kd_Produk ?>" required="required" pattern="" title="">
                            </div>
                       </div>
                       <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                           <div class="form-group">
                               <label for="">Spesifikasi Grid 2</label>
                               <textarea name="spesifikasi_grid_2" id="ckeditor2" class="form-control" rows="5" required="required"><?php echo $data_produk->Grid_2; ?></textarea>
                            </div>
                       </div>
                       <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group">
                               <label for="">Spesifikasi Grid 3</label>
                               <textarea name="spesifikasi_grid_3" id="ckeditor3" class="form-control" rows="5" required="required"><?php echo $data_produk->Grid_3; ?></textarea>
                            </div>
                       </div>
                       <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                          <div class="form-group">
                             <label for="">Example Grid</label>
                             <img src="<?php echo base_url('asset_front/images/asset_new/grid_spesifikasi.png'); ?>" class="img-responsive" alt="Image">
                          </div>
                       </div>
                       <div class="col-lg-12">
                        <div class="form-group pull-right">
                          <button type="reset" class="btn btn-danger">Reset</button>
                          <button type="submit" class="btn btn-success">Save</button>
                        </div>
                       </div>

                     </form>

                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Fasilitas Lainnya</h2>
                    <div class="clearfix"></div>
                    <div class="bs-callout bs-callout-info">
                        <h4>* Penjelasan Singkat</h4>
                        <ul>
                            <li>- Upload gambar dapat dilakukan hanya 1 gambar fasilitas dengan ukuran <strong><i>(378 x 303)</i></strong></li>
                            <li>- <a class="btn btn-xs btn-primary" href="#" role="button">Data Fasiltas [Nama Produk]</a> menampilkan data fasilitas yang di upload, dan untuk mengubah fasilitas</li>
                            <li>- Jika fasilitas memiliki video dapat di inputkan id link dari youtube dengan cara copy dan paste id di form input <strong class="text-danger">tanpa memberi cover video</strong></li>
                        </ul>
                    </div>
                  </div>
                  <div class="x_content">
                     <form action="<?php echo base_url('admin/action_save_fasilitas'); ?>" method="post" enctype="multipart/form-data">
                       
                       <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <div class="form-group">
                               <label for="">Title</label>
                               <input type="text" name="title" id="inputJudul" class="form-control" value="" required="required" title="">
                               <input type="hidden" name="kode_produk" id="inputKode_" class="form-control" value="<?php echo $data_produk->Kd_Produk; ?>" required="required" pattern="" title="">
                            </div>

                            <div class="form-group">
                               <label for="">Body Text</label>
                               <input type="text" name="body_text" id="inputBody" class="form-control" value="" required="required" title="">
                            </div>

                            <div class="form-group">
                               <label for="">Upload Fasiltas </label>
                               <input type="file" name="img_fasilitas" class="form-control" id="" onchange="readFasilitasURL(this)" placeholder="">
                            </div>

                            <div class="form-group">
                               <label for="">Link Url Youtube <sup class="text-warning">Copy Id embed video</sup> <br> <sub>Ex. id video : https://www.youtube.com/embed/<strong class="text-danger">9076RuiO7oU</strong> <i class="fa fa-arrow-left"></i> Copy dan Paste Id Video</sub></label>
                               <input type="text" name="link_url" id="inputLink_url" class="form-control" placeholder="9076RuiO7oU" value="">
                            </div>


                       </div>
                       <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                          <label for="">Preview Fasiltas</label>
                         <img id="fasilitas" src="" style="display: none;" class="img-responsive" alt="Preview">
                       </div>
                       <div class="col-lg-12">
                        <div class="form-group pull-right">
                          <a class="btn btn-primary" data-toggle="modal" href='#fasilitas<?php echo $data_produk->Kd_Produk ?>' role="button">Data Fasiltas <?php echo $data_produk->Nama_Produk ?></a>
                          <button type="reset" class="btn btn-danger">Reset</button>
                          <button type="submit" class="btn btn-success">Save</button>
                        </div>
                       </div>

                     </form>

                     <div class="modal fade" id="fasilitas<?php echo $data_produk->Kd_Produk ?>">
                       <div class="modal-dialog modal-lg">
                         <div class="modal-content">
                           <div class="modal-header">
                             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                             <h4 class="modal-title">Fasilitas <?php echo $data_produk->Nama_Produk ?></h4>
                           </div>
                           <div class="modal-body">
                             
                             <div class="table-responsive">
                               <table class="table table-hover">
                                 <thead>
                                   <tr>
                                     <th><i class="fa fa-trash"></i></th>
                                     <th><i class="fa fa-image"></i> Gambar / <i class="fa fa-play"></i> Link Video</th>
                                     <th>Title</th>
                                     <th>Body Text</th>
                                     <th>Update</th>
                                   </tr>
                                 </thead>
                                 <tbody>
                                  <?php foreach ($data_fasilitas as $dt_fasilitas): ?>
                                   <tr>
                                     <td><a class="btn btn-xs btn-danger" href="<?php echo base_url('admin/remove_fasilitas/'.$dt_fasilitas->Kd_Fasilitas.'/'.$data_produk->Kd_Produk); ?>" title="<?php echo $dt_fasilitas->Title ?>"><i class="fa fa-trash"></i> Hapus</a></td>
                                     <td> 
                                        <?php if (empty($dt_fasilitas->Link_Url)): ?>
                                          <img width="200" height="200" src="<?php echo base_url('storage_img/img_fasilitas/'.$dt_fasilitas->Nama_img); ?>" class="img-responsive" alt="<?php echo $dt_fasilitas->Title ?>">
                                        <?php else: ?>
                                          <iframe class="media-video" width="100%" src="https://www.youtube.com/embed/<?php echo $dt_fasilitas->Link_Url ?>" frameborder="0" allowfullscreen style="height: 220px;"></iframe>
                                        <?php endif ?>
                                      </td>
                                     <td><?php echo $dt_fasilitas->Title ?></td>
                                     <td><?php echo $dt_fasilitas->Body_text ?></td>
                                     <td>
                                       <a class="btn btn-xs btn-info" data-toggle="modal" href='#data-fasilitas<?php echo $dt_fasilitas->Kd_Fasilitas ?>' role="button"><i class="fa fa-pencil"></i> Ubah</a>
                                     </td>
                                   </tr>

                                    <div class="modal fade" id="data-fasilitas<?php echo $dt_fasilitas->Kd_Fasilitas ?>">
                                      <div class="modal-dialog">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title"><?php echo $data_produk->Nama_Produk ?> | Edit Fasilitas <?php echo $dt_fasilitas->Title ?></h4>
                                          </div>
                                          <form action="<?php echo base_url('admin/action_update_data_fasilitas'); ?>" method="post" enctype="multipart/form-data" accept-charset="utf-8">
                                            
                                          <div class="modal-body">
                                            <div class="form-group">
                                               <label for="">Title</label>
                                               <input type="text" name="title" id="inputJudul" class="form-control" value="<?php echo $dt_fasilitas->Title ?>" required="required" title="">
                                               <input type="hidden" name="kode_produk" id="inputKode_Produk" class="form-control" value="<?php echo $data_produk->Kd_Produk; ?>" required="required">
                                               <input type="hidden" name="kode_fasilitas" id="inputKode_Fasilitas" class="form-control" value="<?php echo $dt_fasilitas->Kd_Fasilitas; ?>" required="required">
                                               <input type="hidden" name="old_img_fasilitas" id="inputKode_Fasilitas" class="form-control" value="<?php echo $dt_fasilitas->Nama_img; ?>" required="required">
                                            </div>

                                            <div class="form-group">
                                               <label for="">Body Text</label>
                                               <input type="text" name="body_text" id="inputBody" class="form-control" value="<?php echo $dt_fasilitas->Body_text ?>" required="required" title="">
                                            </div>

                                            <div class="form-group">
                                               <label for="">Link Url Youtube <sup class="text-warning">Copy Id embed video</sup> <br> <sub>Ex. id video : https://www.youtube.com/embed/<strong class="text-danger">9076RuiO7oU</strong> <i class="fa fa-arrow-left"></i> Copy dan Paste Id Video</sub></label>
                                               <input type="text" name="link_url" id="inputLink_url" class="form-control" placeholder="9076RuiO7oU" value="<?php echo $dt_fasilitas->Link_Url ?>">
                                            </div>

                                            <div class="form-group">
                                               <label for="">Upload New Gambar Fasiltas</label>
                                               <input type="file" name="new_img_fasilitas" class="form-control" id="" onchange="readFasilitasURL(this)" placeholder="">
                                            </div>

                                            <div class="form-group">
                                              <label for="">New Preview Fasiltas</label>
                                              <img width="100%" height="50" id="new_fasilitas" src="" style="display: none;" class="img-responsive" alt="Preview">
                                            </div>

                                            <div class="form-group">
                                              <label for="">Old Preview Fasiltas</label>
                                              <img width="100%" height="50" src="<?php echo base_url('storage_img/img_fasilitas/fasilitas_grand_tlogoadi_site_plan.png'); ?>" class="img-responsive" alt="Preview">
                                            </div>
                                          </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup Jendela</button>
                                            <button type="submit" class="btn btn-primary">Ubah Fasilitas</button>
                                          </div>

                                          </form>
                                        </div>
                                      </div>
                                    </div>

                                   <?php endforeach ?>
                                 </tbody>
                               </table>
                             </div>

                           </div>
                           <div class="modal-footer">
                             <button type="button" class="btn btn-default" data-dismiss="modal">Tutup Jendela</button>
                           </div>
                         </div>
                       </div>
                     </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
          
        </div>
        <!-- /page content -->

        <?php $this->load->view('adminpages/footer'); ?>
      </div>
    </div>

    

    <?php require_once(APPPATH .'views/include/admin/admin_script.php'); ?>
    <script type="text/javascript">
      function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                    $('#blah').css('display', '');
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        function readFasilitasURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#fasilitas').attr('src', e.target.result);
                    $('#fasilitas').css('display', '');
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        function readFasilitasURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#new_fasilitas').attr('src', e.target.result);
                    $('#new_fasilitas').css('display', '');
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        function preview_images() 
        {
         $('.col-img').remove();
         var total_file=document.getElementById("images").files.length;
         for(var i=0;i<total_file;i++)
         {
          $('#slide').append("<div class='col-img col-md-3'><img class='img-data img-responsive' src='"+URL.createObjectURL(event.target.files[i])+"'></div>");
         }
        }
    </script>
  </body>
</html>