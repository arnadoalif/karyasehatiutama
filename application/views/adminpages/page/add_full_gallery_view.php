<!DOCTYPE html>
<html lang="en">
  <head>
    <?php require_once(APPPATH .'views/include/admin/admin_style.php'); ?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url('admin/home') ?>" class="site_title"><span>Karya Sehati Utama</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url('asset_admin/images/Logo_KSU.png') ?> " alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>Administrator</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <?php $this->load->view('adminpages/menu_bar'); ?>

          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <?php $this->load->view('adminpages/nav_menu'); ?>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Album Gallery <?php echo $data_gallery->Nama_Gallery ?></h3>
                <ol class="breadcrumb">
                  <li>
                    <a href="<?php echo base_url('admin/gallery'); ?>"><i class="fa fa-book"></i> Gallery</a>
                  </li>
                  <li class="active">Data Album <?php echo $data_gallery->Nama_Gallery ?></li>
                </ol>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="bs-callout bs-callout-info">
                <h4>* Penjelasan Singkat</h4>
                <ul>
                    <li>- Hati Hati Ketika Menakan Tombol <strong> <i class="fa fa-trash"></i> Hapus </strong>, Belum ada notifikasi dalam menghapus data</li>
                    <li>- Halaman digunakan untuk membuat album baru </li>
                    <li>- Upload dapat dilakukan banyak file gambar <strong>Rekomendasi gambar png dan size normal < 1MB </strong> </li>
                    <li>- Tombol Edit untuk mengedit gambar dan hanya dapat memilih 1 gambar</li>
                </ul>
            </div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Buat Album</h2>
                    <div class="clearfix"></div>
                    <?php if (isset($_SESSION['message_data'])): ?>
                      <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <?php echo $_SESSION['message_data'] ?>
                      </div>
                      <?php endif ?>

                      <?php if (isset($_SESSION['error_data'])): ?>
                      <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <?php echo $_SESSION['error_data'] ?>
                      </div>
                    <?php endif ?>
                  </div>
                  <div class="x_content">
                     <form action="<?php echo base_url('admin/action_save_create_album'); ?>" method="post" enctype="multipart/form-data">
                       
                       <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <div class="form-group">
                             <label for="">Upload Gambar Album <sup style="color: red">(* Lebih dari 1)</sup></label>
                             <input type="file" multiple name="img_album[]" class="form-control" id="images" onchange="preview_images()" placeholder="">
                             <input type="hidden" name="kode_gallery" value="<?php echo $data_gallery->Kd_Gallery ?>" placeholder="">
                           </div>
                       </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                          <div class="form-group">
                             <label for="">Preview Slide</label>
                             <div id="slide"></div>
                          </div>
                        </div>
                       <div class="col-lg-12">
                        <div class="form-group pull-right">
                          <button type="reset" class="btn btn-danger">Reset</button>
                          <button type="submit" class="btn btn-success">Upload</button>
                        </div>
                       </div>

                     </form>

                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <?php $i= 1; foreach ($data_berkas as $dt_berkas): ?>
              <div class="col-md-55">
                <div class="thumbnail">
                  <div class="image view view-first">
                    <img style="width: 100%; display: block;" src="<?php echo base_url('storage_img/img_album/'.$dt_berkas->Nama_img); ?>" alt="image" />
                    <div class="mask">
                      <p>Gallery  <?php echo $i++ ?> </p>
                      <div class="tools tools-bottom">
                        <a data-toggle="modal" href='#modal-id<?php echo $dt_berkas->Kd_Berkas ?>'><i class="fa fa-pencil"></i></a>
                        <a href="<?php echo base_url('admin/remove_foto_gallery/'.$dt_berkas->Kd_Berkas.'/'.$dt_berkas->Kd_Gallery); ?>"><i class="fa fa-times"></i></a>
                      </div>
                    </div>
                  </div>
                  
                </div>
              </div>

              <div class="modal fade" id="modal-id<?php echo $dt_berkas->Kd_Berkas ?>">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      <h4 class="modal-title">Edit Gallery </h4>
                    </div>
                    <form action="<?php echo base_url('admin/action_update_foto_gallery'); ?>" method="POST" role="form" enctype="multipart/form-data">
                      <div class="modal-body">
                          <div class="form-group">
                            <label for="">Upload Foto</label>
                            <input type="file" name="img_new_galleri" class="form-control" id="" placeholder="Input field">
                            <input type="hidden" class="form-control" value="<?php echo $dt_berkas->Kd_Berkas ?>" name="kode_berkas" id="">
                            <input type="hidden" class="form-control" value="<?php echo $dt_berkas->Kd_Gallery ?>" name="kode_gallery" id="">
                          </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup Jendela</button>
                        <button type="submit" class="btn btn-primary">Update Image</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>

              <?php endforeach ?>

            </div>
          </div>

        </div>
        <!-- /page content -->

        <?php $this->load->view('adminpages/footer'); ?>
      </div>
    </div>

    

    <?php require_once(APPPATH .'views/include/admin/admin_script.php'); ?>
    <script type="text/javascript">
      function preview_images() 
        {
         $('.col-img').remove();
         var total_file=document.getElementById("images").files.length;
         for(var i=0;i<total_file;i++)
         {
          $('#slide').append("<div class='col-img col-md-3'><img class='img-data img-responsive' src='"+URL.createObjectURL(event.target.files[i])+"'></div>");
         }
        }
    </script>
  </body>
</html>