<!DOCTYPE html>
<html lang="en">
  <head>
    <?php require_once(APPPATH .'views/include/admin/admin_style.php'); ?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url('admin/home') ?>" class="site_title"><span>Karya Sehati Utama</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url('asset_admin/images/Logo_KSU.png') ?> " alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>Administrator</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <?php $this->load->view('adminpages/menu_bar'); ?>

          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <?php $this->load->view('adminpages/nav_menu'); ?>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Data Type Property</h3>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="bs-callout bs-callout-info">
                <h4>* Penjelasan Singkat</h4>
                <ul>
                    <li>- Hati Hati Ketika Menakan Tombol <strong> <i class="fa fa-trash"></i> Hapus </strong>, Belum ada notifikasi dalam menghapus data</li>
                    <li>- <span class="label label-info"><i class="fa fa-pencil"></i> Ganti Cover Slide</span> Digunakan untuk mengganti cover </li>
                    <li>- <a class="btn btn-xs btn-warning disabled" href="#" role="button"><i class="fa fa-image"></i>Tambah Slide</a> Digunakan untuk menambhakan slide, upload file dapat banyak, <strong>Rekomendasi Max 3 Gambar dengan ukuran (1214x632)</strong></li>
                    <li>- <i class="fa fa-pencil"></i> Digunakan untuk mengedit data property</li>
                    <li>- <i class="fa fa-eye"></i> Digunakan untuk melihat detail property</li>
                    <li>- Jika terjadi masalah dalam penggunaan ini dapat hubungi web programmer</li>
                </ul>
            </div>

            <?php if (isset($_SESSION['message_data'])): ?>
              <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
                <?php echo $_SESSION['message_data'] ?>
              </div>
              <?php endif ?>

              <?php if (isset($_SESSION['error_data'])): ?>
              <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
                <?php echo $_SESSION['error_data'] ?>
              </div>
            <?php endif ?>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Type Property</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                     
                     <div class="table-responsive">
                       <table id="datatable" class="table table-hover">
                         <thead>
                           <tr>
                             <th><i class="fa fa-trash"></i> Hapus</th>
                             <th><i class="fa fa-image"></i> Cover Property</th>
                             <th>Nama Property</th>
                             <th>Nama Produk</th>
                             <th>Kategori Property</th>
                             <th>Create_at</th>
                             <th>Tambah / Edit Slide</th>
                             <th>Edit / View / Slide</th>
                           </tr>
                         </thead>
                         <tbody>
                          <?php $this->load->model('Time_lib'); $time_lib = new Time_lib(); ?>
                          <?php foreach ($data_property as $dt_property): ?>
                            <tr>
                              <td><a class="btn btn-xs btn-danger btn-sm" href="<?php echo base_url('admin/remove_property/'.$dt_property->Kd_Property.'/'.$dt_property->Kd_Produk); ?>" title="<?php echo "Hapus ".$dt_property->Nama_Property ?>"><i class="fa fa-trash"></i></a></td>
                              <td><img width="100" height="100" src="<?php echo base_url('storage_img/img_cover_property/'.$dt_property->Cover_Name); ?>" class="img-responsive" alt="Image"><span class="label label-info"><a  data-toggle="modal" href='#ganti-cover<?php echo $dt_property->Kd_Property ?>' title=""><i class="fa fa-pencil"></i> Ganti Cover Cover</a></span></td>
                              <td><?php echo $dt_property->Nama_Property ?></td>
                              <td><?php echo $dt_property->Nama_Produk ?></td>
                              <td>
                                <?php if ($dt_property->Kategori == "1_lantai"): ?>
                                  Lantai 1
                                <?php elseif ($dt_property->Kategori == "2_lantai"): ?>
                                  Lantai 2
                                <?php else: ?>
                                  Lainnya
                                <?php endif ?>
                                </td>
                              <td><?php echo $time_lib->getHowLongAgo($dt_property->Create_at) ?></td>
                              <td>
                                <a class="btn btn-xs btn-warning" data-toggle="modal" href='#tambah-slide<?php echo $dt_property->Kd_Property ?>'><i class="fa fa-image"></i> Tambah Slide</a>
                                <a class="btn btn-xs btn-info" data-toggle="modal" href='<?php echo base_url('admin/edit_slide_produk/'.$dt_property->Kd_Property); ?>'><i class="fa fa-image"></i> Edit Slide</a>
                              </td>
                              <td>
                                <a class="btn btn-xs btn-info" data-toggle="modal" href='#edit-data<?php echo $dt_property->Kd_Property ?>' role="button"><i class="fa fa-pencil"></i></a>
                                <a class="btn btn-xs btn-default" href="#" role="button"><i class="fa fa-eye"></i></a>
                              </td>
                            </tr>

                            <div class="modal fade" id="ganti-cover<?php echo $dt_property->Kd_Property ?>">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Ganti Cover Property <?php echo $dt_property->Nama_Property ?></h4>
                                  </div>
                                  <div class="modal-body">
                                    <form action="<?php echo base_url('admin/action_edit_cover_property'); ?>" method="POST" role="form" enctype="multipart/form-data">
                                    
                                      <div class="form-group">
                                        <label for="">New Cover Property</label>
                                        <input type="hidden" name="kode_produk" class="form-control" value="<?php echo $dt_property->Kd_Produk ?>" required="required">
                                        <input type="hidden" name="kode_property" class="form-control" value="<?php echo $dt_property->Kd_Property ?>" required="required">
                                        <input type="file" name="new_img_cover_property" class="form-control" id="" onchange="readURL(this)" placeholder="">
                                        <label for="">Preview new Cover Property</label>
                                        <img id="blah" src="" style="display: none;" class="img-responsive" alt="Preview">
                                      </div>
                                    
                                      <button type="submit" class="btn btn-primary">Ubah Cover</button>
                                    </form>
                                  </div>
                                  <!-- <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup Jendela</button>
                                    <button type="button" class="btn btn-primary">Save changes</button>
                                  </div> -->
                                </div>
                              </div>
                            </div>

                            <div class="modal fade" id="tambah-slide<?php echo $dt_property->Kd_Property ?>">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Tambah Slide Property <?php echo $dt_property->Nama_Property ?></h4>
                                  </div>
                                  <form action="<?php echo base_url('admin/action_tambah_slide_property'); ?>" method="POST" role="form" enctype="multipart/form-data">
                                    <div class="modal-body">
                
                                        <div class="form-group">
                                          <label for="">Slide Show</label>
                                          <input type="file" name="img_slide_property[]" multiple class="form-control" id="images" onchange="preview_images()">
                                          <input type="hidden" name="kode_property" id="" class="form-control" value="<?php echo $dt_property->Kd_Property ?>" required="required">
                                          <label for="">Preview Slide</label>
                                          <div id="slide"></div>
                                        </div>
                      
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Tutup Jendela</button>
                                      <button type="submit" class="btn btn-primary">Tambah Slide</button>
                                    </div>
                                  </form>

                                  </div>
                                </div>
                              </div>
                            </div>

                          <?php endforeach ?>
                           
                         </tbody>
                       </table>
                     </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <?php $this->load->view('adminpages/footer'); ?>
      </div>
    </div>

    

    <?php require_once(APPPATH .'views/include/admin/admin_script.php'); ?>
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                    $('#blah').css('display', '');
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        function preview_images() 
        {
         $('.img-data').remove();
         var total_file=document.getElementById("images").files.length;
         for(var i=0;i<total_file;i++)
         {
          $('#slide').append("<img width='200' height='200' class='img-data img-responsive' src='"+URL.createObjectURL(event.target.files[i])+"'>");
         }
        }
    </script>
  </body>
</html>