<!DOCTYPE html>
<html lang="en">
  <head>
    <?php require_once(APPPATH .'views/include/admin/admin_style.php'); ?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url('admin/home') ?>" class="site_title"><span>Karya Sehati Utama</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url('asset_admin/images/Logo_KSU.png') ?> " alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>Administrator</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <?php $this->load->view('adminpages/menu_bar'); ?>

          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <?php $this->load->view('adminpages/nav_menu'); ?>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Portfolio</h3>
              </div>
            </div>

            <div class="clearfix"></div>
            <div class="bs-callout bs-callout-info">
                <h4>* Penjelasan Singkat</h4>
                <ul>
                    <li>- Hati Hati Ketika Menakan Tombol <strong> <i class="fa fa-trash"></i> Hapus </strong>, Belum ada notifikasi dalam menghapus data</li>
                    <li>- Halaman digunakan untuk menambahkan portfolio </li>
                    <li>- Upload gambar dapat dilakukan banyak <strong>Rekomendasi Max 3 Gambar</strong> </li>
                    <li>- <i class="fa fa-pencil"></i> Digunakan untuk mengedit data portfolio</li>
                    <li>- <i class="fa fa-eye"></i> Digunakan untuk melihat detail portfolio</li>
                    <li>- <i class="fa fa-image"></i> Pilih gambar cover untuk mengubah cover portfolio</li>
                </ul>
            </div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>New Portfolio</h2>
                    <div class="clearfix"></div>

                    <?php if (isset($_SESSION['message_data'])): ?>
                      <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <?php echo $_SESSION['message_data'] ?>
                      </div>
                      <?php endif ?>

                      <?php if (isset($_SESSION['error_data'])): ?>
                      <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <?php echo $_SESSION['error_data'] ?>
                      </div>
                    <?php endif ?>

                  </div>
                  <div class="x_content">
                     <form action="<?php echo base_url('admin/action_save_portfolio'); ?>" method="post" enctype="multipart/form-data">
                       
                       <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                           <div class="form-group">
                             <label for="">Upload Cover Portfolio</label>
                             <input type="file" name="img_portfolio" class="form-control" id="img_slide_1" onchange="view_img()" placeholder="">
                             <label for="">Preview Image</label>
                             <div id="img_slide"></div>
                           </div>
                       </div>
                       <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group">
                               <label for="">Nama Portfolio</label>
                               <input type="text" name="nama" id="inputNama" class="form-control" value="" required="required" title="">
                            </div>

                            <div class="form-group">
                               <label for="">Address</label>
                               <input type="text" name="alamat" id="inputAlamat" class="form-control" value="" required="required" title="">
                            </div>
                       </div>
                       <div class="col-lg-12">
                        <div class="form-group pull-right">
                          <button type="reset" class="btn btn-danger">Reset</button>
                          <button type="submit" class="btn btn-success">Save</button>
                        </div>
                       </div>

                     </form>

                  </div>
                </div>
              </div>
            </div>


            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Data Portfolio</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


                    <div class="table-responsive">
                      <table id="datatable" class="table table-hover">
                        <thead>
                          <tr>
                             <th><i class="fa fa-trash"></i> Hapus</th>
                             <th><i class="fa fa-image"></i> Cover</th>
                             <th>Nama</th>
                             <th>Alamat</th>
                             <th>Tambah / Edit Slide</th>
                             <th>Create</th>
                             <th>Edit / View</th>
                           </tr>
                        </thead>
                        <tbody>
                          <?php $this->load->model('Time_lib'); $time_lib = new Time_lib();?>
                          <?php foreach ($data_portfolio as $dt_portfolio): ?>
                            
                          <tr>
                            <td><a class="btn btn-xs btn-sm btn-danger" href="<?php echo base_url('admin/action_delete_portfolio/'.$dt_portfolio->Kd_Portfolio); ?>" role="button"><i class="fa fa-trash"></i></a></td>
                            <td><a data-toggle="modal" href="#edit-idcover<?php echo $dt_portfolio->Kd_Portfolio ?>"><img height="80" width="80" src="<?php echo base_url('storage_img/img_cover_portfolio/'.$dt_portfolio->Cover_Img); ?>" class="img-responsive" alt="Image"></a></td>
                            <td><?php echo $dt_portfolio->Nama_Portfolio ?></td>
                            <td><?php echo $dt_portfolio->Alamat_Portfolio ?></td>
                            <td>
                              <a class="btn btn-xs btn-warning" data-toggle="modal" href="#modal-id<?php echo $dt_portfolio->Kd_Portfolio; ?>" role="button"><i class="fa fa-plus"></i> Tambah</a>
                              <a class="btn btn-xs btn-default" href="<?php echo base_url('admin/edit_slide_portfolio/'.$dt_portfolio->Kd_Portfolio); ?>" role="button"><i class="fa fa-pencil"></i> Edit</a>
                            </td>
                            <td><?php echo $time_lib->getHowLongAgo($dt_portfolio->Create_at) ?></td>
                            <td>
                              <a class="btn btn-xs btn-default" data-toggle="modal" href="#edit-id<?php echo $dt_portfolio->Kd_Portfolio ?>" role="button"><i class="fa fa-pencil"></i> Edit Data</a>
                            </td>
                          </tr>

                          <div class="modal fade" id="modal-id<?php echo $dt_portfolio->Kd_Portfolio; ?>">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Tambah Slide Portfolio <?php echo $dt_portfolio->Nama_Portfolio ?></h4>
                                  </div>
                                  <form action="<?php echo base_url('admin/action_save_slide_portfolio'); ?>" method="POST" role="form" enctype="multipart/form-data">
                                    <div class="modal-body">
                
                                        <div class="form-group">
                                          <label for="">Slide Show</label>
                                          <input type="file" name="img_slide_portfolio[]" multiple class="form-control" id="images" onchange="preview_images()">
                                          <input type="hidden" name="kode_portfolio" id="" class="form-control" value="<?php echo $dt_portfolio->Kd_Portfolio ?>" required="required">
                                          <label for="">Preview Slide</label>
                                          <div id="slide"></div>
                                        </div>
                      
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Tutup Jendela</button>
                                      <button type="submit" class="btn btn-primary">Tambah Slide</button>
                                    </div>
                                  </form>

                                  </div>
                                </div>
                              </div>
                          </div>

                          <div class="modal fade" id="edit-id<?php echo $dt_portfolio->Kd_Portfolio ?>">
                            <div class="modal-dialog">
                               <div class="modal-content">
                                 <div class="modal-header">
                                   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                   <h4 class="modal-title">Edit Data <?php echo $dt_portfolio->Nama_Portfolio ?></h4>
                                 </div>
                                 <form action="<?php echo base_url('admin/action_edit_portfolio'); ?>" method="POST">
                                   <div class="modal-body">
                                  
                                      <div class="form-group">
                                         <label for="">Nama Portfolio</label>
                                         <input type="text" name="nama" id="inputNama" class="form-control" value="<?php echo $dt_portfolio->Nama_Portfolio ?>" required="required" title="">
                                         <input type="hidden" name="kode_portfolio" id="inputKode" class="form-control" value="<?php echo $dt_portfolio->Kd_Portfolio ?>" required="required" title="">
                                      </div>

                                      <div class="form-group">
                                         <label for="">Address</label>
                                         <input type="text" name="alamat" id="inputAlamat" class="form-control" value="<?php echo $dt_portfolio->Alamat_Portfolio ?>" required="required" title="">
                                      </div>
                                    
                                   </div>
                                   <div class="modal-footer">
                                     <button type="button" class="btn btn-default" data-dismiss="modal">Tutup Jendela</button>
                                     <button type="submit" class="btn btn-primary">Perbaharui Data</button>
                                   </div>
                                 </form>
                               </div>
                             </div>
                          </div>

                          <div class="modal fade" id="edit-idcover<?php echo $dt_portfolio->Kd_Portfolio ?>">
                            <div class="modal-dialog">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                  <h4 class="modal-title">Edit Cover <?php echo $dt_portfolio->Nama_Portfolio ?></h4>
                                </div>
                                <form action="<?php echo base_url('admin/action_update_cover_portfolio'); ?>" method="post" enctype="multipart/form-data">
                                  <div class="modal-body">
                                    
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                         <div class="form-group">
                                           <label for="">Upload Cover Portfolio</label>
                                           <input type="file" name="img_portfolio" class="form-control" onchange="readURL()" placeholder="">
                                           <input type="hidden" name="kode_portfolio" id="inputKode" class="form-control" value="<?php echo $dt_portfolio->Kd_Portfolio ?>" required="required">
                                            <div class="col-md-6 col-lg-6">
                                                <label>New Preview Img</label>
                                                <img id="blah" src="" style="display: none;" class="img-responsive" alt="Preview">
                                             </div>
                                             <div class="col-md-6 col-lg-6">
                                                <label>Old Preview Img</label>
                                                <img src="<?php echo base_url('storage_img/img_cover_portfolio/'.$dt_portfolio->Cover_Img); ?>" class="img-responsive" alt="Preview">
                                             </div>
                                         </div>
                                     </div>

                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup Jendela</button>
                                    <button type="submit" class="btn btn-primary">Update Cover</button>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>

                          <?php endforeach ?>


                        </tbody>
                      </table>
                    </div>                    

                  </div>
                </div>
              </div>
            </div>

            

          </div>
        </div>
        <!-- /page content -->

        <?php $this->load->view('adminpages/footer'); ?>
      </div>
    </div>

    <?php require_once(APPPATH .'views/include/admin/admin_script.php'); ?>
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                    $('#blah').css('display', '');
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

       function view_img() 
        {
         $('.img-data').remove();
         var total_file=document.getElementById("img_slide_1").files.length;
         for(var i=0;i<total_file;i++)
         {
          $('#img_slide').append("<img width='200' height='200' class='img-data img-responsive' src='"+URL.createObjectURL(event.target.files[i])+"'>");
         }
        }

        function preview_images() 
        {
         $('.data-img').remove();
         var total_file=document.getElementById("images").files.length;
         for(var i=0;i<total_file;i++)
         {
          $('#slide').append("<img width='200' height='200' class='data-img img-responsive' src='"+URL.createObjectURL(event.target.files[i])+"'>");
         }
        }
    </script>
  </body>
</html>