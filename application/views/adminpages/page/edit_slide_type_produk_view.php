<!DOCTYPE html>
<html lang="en">
  <head>
    <?php require_once(APPPATH .'views/include/admin/admin_style.php'); ?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url('admin/home') ?>" class="site_title"><span>Karya Sehati Utama</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url('asset_admin/images/Logo_KSU.png') ?> " alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>Administrator</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <?php $this->load->view('adminpages/menu_bar'); ?>

          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <?php $this->load->view('adminpages/nav_menu'); ?>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Slide Property <strong><?php echo $data_property->Nama_Property ?></strong> Produk <?php echo $data_property->Nama_Produk ?></h3>
                <ol class="breadcrumb">
                  <li>
                    <a href="<?php echo base_url('admin/new-property'); ?>"><i class="fa fa-book"></i> Produk</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url('admin/data-type-produk'); ?>">Data Type Produk</a>
                  </li>
                  <li class="active">Data Slide Property <?php echo $data_property->Nama_Property ?></li>
                </ol>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Data Slide Property <?php echo $data_property->Nama_Property ?></h2>
                    
                    <div class="clearfix"></div>
                    <?php if (isset($_SESSION['message_data'])): ?>
                      <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <?php echo $_SESSION['message_data'] ?>
                      </div>
                      <?php endif ?>

                      <?php if (isset($_SESSION['error_data'])): ?>
                      <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <?php echo $_SESSION['error_data'] ?>
                      </div>
                    <?php endif ?>
                  </div>
                  <div class="x_content">
                    
                    <div class="row">
                      
                      
                    <?php if (count($data_berkas_slide) > 0): ?>
                        
                      <?php $i = 1; foreach ($data_berkas_slide as $dt_berkas_slide): ?>
                      
                      <div class="col-md-55">
                        <div class="thumbnail">
                          <div class="image view view-first">
                            <img style="width: 100%; display: block;" src="<?php echo base_url("storage_img/img_slide_property/".$dt_berkas_slide->Nama_img); ?>" alt="image" />
                            <div class="mask">
                              <p><?php echo "Edit Slide ".$i++ ?></p>
                              <div class="tools tools-bottom">
                                <a data-toggle="modal" href='#modal-id<?php echo $dt_berkas_slide->Kd_Berkas ?>'><i class="fa fa-pencil"></i></a>
                                <a href='<?php echo base_url('admin/remove_slide_property/'.$dt_berkas_slide->Kd_Berkas.'/'.$data_property->Kd_Property); ?>'><i class="fa fa-times"></i></a>
                              </div>
                            </div>
                          </div>
                          <div class="caption">
                            <p align="Center"><?php echo "Slide Produk ". $data_property->Nama_Property ?></p>
                            <p align="Center"><?php echo $data_property->Nama_Produk ?></p>
                          </div>
                        </div>
                      </div>

                      <div class="modal fade" id="modal-id<?php echo $dt_berkas_slide->Kd_Berkas ?>">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title"><?php echo "Edit Slide " ?></h4>
                            </div>
                            <form action="<?php echo base_url('admin/action_edit_slide_property'); ?>" method="post" enctype="multipart/form-data">
                              <div class="modal-body">
                                <div class="form-group">
                                   <label for="">Upload Slide <sup style="color:red">hanya dapat memakai 1 gambar</sup></label>
                                   <input type="hidden" name="kode_berkas" class="form-control" value="<?php echo $dt_berkas_slide->Kd_Berkas ?>" required="required">
                                   <input type="hidden" name="kode_property" class="form-control" value="<?php echo $dt_berkas_slide->Kd_Property ?>" required="required">
                                   <input type="file" name="img_slide_property" class="form-control" id="" onchange="readURL(this)" placeholder="">
                                   <div class="col-md-6 col-lg-6">
                                      <label>New Preview Img</label>
                                      <img id="blah" src="" style="display: none;" class="img-responsive" alt="Preview">
                                   </div>
                                   <div class="col-md-6 col-lg-6">
                                      <label>Old Preview Img</label>
                                      <img src="<?php echo base_url("storage_img/img_slide_property/".$dt_berkas_slide->Nama_img); ?>" class="img-responsive" alt="Preview">
                                   </div>
                                 </div>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup Jendela</button>
                                <button type="submit" class="btn btn-primary">Ubah Slide</button>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>

                      <?php endforeach ?>

                    <?php else: ?>

                      <div class="alert alert-warning">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <strong>Tidak ada slide saat ini </strong>
                      </div>
                        
                    <?php endif ?>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <?php $this->load->view('adminpages/footer'); ?>
      </div>
    </div>

    

    <?php require_once(APPPATH .'views/include/admin/admin_script.php'); ?>
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                    $('#blah').css('display', '');
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
  </body>
</html>