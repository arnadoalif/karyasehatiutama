<!DOCTYPE html>
<html lang="en">
  <head>
    <?php require_once(APPPATH .'views/include/admin/admin_style.php'); ?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url('admin/home') ?>" class="site_title"><span>Karya Sehati Utama</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url('asset_admin/images/Logo_KSU.png') ?> " alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>Administrator</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <?php $this->load->view('adminpages/menu_bar'); ?>

          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <?php $this->load->view('adminpages/nav_menu'); ?>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Tentang Office</h3>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Isikan Tentang Perusahaan</h2>
                    <div class="clearfix"></div>
                    <?php if (isset($_SESSION['message_data'])): ?>
                      <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <?php echo $_SESSION['message_data'] ?>
                      </div>
                      <?php endif ?>

                      <?php if (isset($_SESSION['error_data'])): ?>
                      <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <?php echo $_SESSION['error_data'] ?>
                      </div>
                    <?php endif ?>
                  </div>
                  <div class="x_content">
                     <form action="<?php echo base_url('admin/action_save_tentang'); ?>" method="post" enctype="multipart/form-data">
                       
                       <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <div class="form-group">
                               <label for="">Nama Perusahaan</label>
                               <input type="text" name="nama_perusahaan" id="inputAlamat" class="form-control" value="<?php echo $data_tentang->nama_perusahaan ?>" required="required" title="">
                            </div>
                           <div class="form-group">
                             <label for="">Upload Cover <sup style="color:red">hanya dapat memakai 1 gambar</sup></label>
                             <input type="hidden" name="kode_tentang" class="form-control" value="<?php echo $data_tentang->Kd_Tentang ?>" required="required" pattern="" title="">
                             <input type="file" name="img_kantor" class="form-control" id="" onchange="readURL(this)" placeholder="">

                             <div class="col-md-6 col-lg-6">
                                <label>New Preview Img</label>
                                <img id="blah" src="" style="display: none;" class="img-responsive" alt="Preview">
                             </div>
                             <div class="col-md-6 col-lg-6">
                                <label>Old Preview Img</label>
                                <img src="<?php echo base_url('storage_img/img_tentang/'.$data_tentang->img_tentang); ?>" style="display: none;" class="img-responsive" alt="Preview">
                             </div>
                           </div>
                       </div>
                       <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group">
                               <label for="">Title <span class="text-danger"><i>(* Title Salam)</i></span></label>
                               <input type="text" name="title_salam" id="inputTitle_salam" class="form-control" value="<?php echo $data_tentang->title_salam ?>" required="required">
                            </div>
                            <div class="form-group">
                               <label for="">Latar Belakang Perusahaan</label>
                               <textarea name="deskripsi" id="ckeditor1" class="form-control" rows="3" required="required"><?php echo $data_tentang->deskripsi ?></textarea>
                            </div>
                            
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                              <input type="text" name="email" class="form-control has-feedback-left" value="<?php echo $data_tentang->email ?>" id="inputSuccess4" placeholder="Email">
                              <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                              <input type="text" name="nomor_telepon" class="form-control" id="inputSuccess5" value="<?php echo $data_tentang->nomor_telepon ?>" placeholder="Phone">
                              <span class="fa fa-phone form-control-feedback right" aria-hidden="true"></span>
                            </div>

                            <div class="form-group">
                               <label for="">Alamat Perusahaan</label>
                               <input type="text" name="alamat" id="inputAlamat" class="form-control" value="<?php echo $data_tentang->alamat ?>" required="required" title="">
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                              <input type="text" name="latitude" class="form-control has-feedback-left" value="<?php echo $data_tentang->latitude ?>" id="inputSuccess4" placeholder="Lat">
                              <span class="fa fa-map-o form-control-feedback left" aria-hidden="true"></span>
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                              <input type="text" name="longitude" class="form-control" id="inputSuccess5" value="<?php echo $data_tentang->longitude ?>" placeholder="Long">
                              <span class="fa fa-map-o form-control-feedback right" aria-hidden="true"></span>
                            </div>
                       </div>
                       <div class="col-lg-12">
                        <div class="form-group pull-right">
                          <button type="reset" class="btn btn-danger">Reset</button>
                          <button type="submit" class="btn btn-success">Save</button>
                        </div>
                       </div>

                     </form>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <?php $this->load->view('adminpages/footer'); ?>
      </div>
    </div>

    

    <?php require_once(APPPATH .'views/include/admin/admin_script.php'); ?>
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                    $('#blah').css('display', '');
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
  </body>
</html>