<!DOCTYPE html>
<html lang="en">
  <head>
    <?php require_once(APPPATH .'views/include/admin/admin_style.php'); ?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url('admin/home') ?>" class="site_title"><span>Karya Sehati Utama</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url('asset_admin/images/Logo_KSU.png') ?> " alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>Administrator</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <?php $this->load->view('adminpages/menu_bar'); ?>

          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <?php $this->load->view('adminpages/nav_menu'); ?>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Tambah Gallery Proyek <strong><?php echo $data_produk->Nama_Produk ?></strong></h3>
              </div>
            </div>
            <span class="pull-right"><img width="100" height="100" src="<?php echo base_url('storage_img/img_logo_produk/'.$data_produk->Logo_Produk); ?>" class="img-responsive" alt="Image"> </span>

            <div class="clearfix"></div>

            <div class="bs-callout bs-callout-info">
                <h4>* Penjelasan Singkat</h4>
                <ul>
                   <li>- <span class="label label-success">[Link]</span> dapat ketuk link tersebut untuk melihat detail deskripsi produk</li>
                   <li>- Hanya dapat upload 1 gambar , gambar berupa dokumentasi proyek</li>
                </ul>
            </div>
            </div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Gallery Proyek <strong><?php echo $data_produk->Nama_Produk ?></strong></h2>
                    <div class="clearfix"></div>
                    <?php if (isset($_SESSION['message_data'])): ?>
                      <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <?php echo $_SESSION['message_data'] ?>
                      </div>
                      <?php endif ?>

                      <?php if (isset($_SESSION['error_data'])): ?>
                      <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <?php echo $_SESSION['error_data'] ?>
                      </div>
                    <?php endif ?>
                  </div>
                  <div class="x_content">
                     <form action="<?php echo base_url('admin/action_save_gallery_proyek'); ?>" method="post" enctype="multipart/form-data">
                       
                       <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <div class="form-group">
                               <label for="">Title</label>
                               <input type="text" name="title" id="inputAlamat" class="form-control" value="" required="required" title="">
                            </div>
                           <div class="form-group">
                             <label for="">Upload Gambar Proyek <sup style="color:red">hanya dapat memakai 1 gambar</sup></label>
                             <input type="hidden" name="kode_produk" class="form-control" value="<?php echo $data_produk->Kd_Produk ?>" required="required" pattern="" title="">
                             <input type="file" name="img_galleri_proyek" class="form-control" id="" onchange="readURL(this)" placeholder="">
                           </div>
                       </div>
                       <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <div class="col-md-6 col-lg-6">
                                <label>New Preview Img</label>
                                <img id="blah" src="" style="display: none;" class="img-responsive" alt="Preview">
                             </div>
                       </div>
                       <div class="col-lg-12">
                        <div class="form-group pull-right">
                          <button type="reset" class="btn btn-danger">Reset</button>
                          <button type="submit" class="btn btn-success"><i class="fa fa-upload"></i> Upload Gambar</button>
                        </div>
                       </div>

                     </form>

                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <?php $i= 1; foreach ($data_gallery_proyek as $dt_g_proyek): ?>
              <div class="col-md-55">
                <div class="thumbnail">
                  <div class="image view view-first">
                    <img style="width: 100%; display: block;" src="<?php echo base_url('storage_img/img_galleri_proyek/'.$dt_g_proyek->Nama_img); ?>" alt="image" />
                    <div class="mask">
                      <p>Gallery  <?php echo $i++ ?> </p>
                      <div class="tools tools-bottom">
                        <a data-toggle="modal" href='#modal-id<?php echo $dt_g_proyek->Kd_Gproyek ?>'><i class="fa fa-pencil"></i></a>
                        <a href="<?php echo base_url('admin/remove_foto_gallery_proyek/'.$dt_g_proyek->Kd_Gproyek.'/'.$data_produk->Kd_Produk); ?>"><i class="fa fa-times"></i></a>
                      </div>
                    </div>
                  </div>
                  <h4 align="center"><?php echo $dt_g_proyek->Title ?></h4>
                </div>
              </div>

              <div class="modal fade" id="modal-id<?php echo $dt_g_proyek->Kd_Gproyek ?>">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      <h4 class="modal-title">Edit Gallery </h4>
                    </div>
                    <form action="<?php echo base_url('admin/action_update_gallery_proyek'); ?>" method="POST" role="form" enctype="multipart/form-data">
                      <div class="modal-body">
                          
                          <div class="form-group">
                              <label for="">Title</label>
                              <input type="text" name="title" id="inputAlamat" class="form-control" value="<?php echo $dt_g_proyek->Title ?>" required="required" title="">
                          </div>

                          <div class="form-group">
                            <label for="">Upload Foto</label>
                            <input type="file" name="img_new_galleri" class="form-control" id="" placeholder="Input field">
                            <input type="hidden" class="form-control" value="<?php echo $dt_g_proyek->Kd_Gproyek ?>" name="kode_gallery" id="">
                            <input type="hidden" class="form-control" value="<?php echo $dt_g_proyek->Kd_Produk ?>" name="kode_produk" id="">
                          </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup Jendela</button>
                        <button type="submit" class="btn btn-primary">Update Image</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>

              <?php endforeach ?>

            </div>

          </div>
        </div>
        <!-- /page content -->

        <?php $this->load->view('adminpages/footer'); ?>
      </div>
    </div>

    

    <?php require_once(APPPATH .'views/include/admin/admin_script.php'); ?>
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                    $('#blah').css('display', '');
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
  </body>
</html>