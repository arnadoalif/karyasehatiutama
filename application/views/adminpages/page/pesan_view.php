<!DOCTYPE html>
<html lang="en">
  <head>
    <?php require_once(APPPATH .'views/include/admin/admin_style.php'); ?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url('admin/home') ?>" class="site_title"><span>Karya Sehati Utama</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url('asset_admin/images/Logo_KSU.png') ?> " alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>Administrator</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <?php $this->load->view('adminpages/menu_bar'); ?>

          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <?php $this->load->view('adminpages/nav_menu'); ?>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Message</h3>
              </div>
            </div>

            <div class="clearfix"></div>
            <div class="bs-callout bs-callout-info">
                <h4>* Penjelasan Singkat</h4>
                <ul>
                    <li>- Hati Hati Ketika Menakan Tombol <strong> <i class="fa fa-trash"></i> Hapus </strong>, Belum ada notifikasi dalam menghapus data</li>
                    <li>- Halaman digunakan untuk melihat pesan yang di tinggalkan pengunjung </li>
                    <li>- <a class="btn btn-xs btn-success disabled" href="#" role="button">[Nomor Telpon]</a> Tombol ini digunakan untuk melakukan chat langsung pada pengunjung, dengan menggunakan fiktur whatsapp</li>
                    <li>- <i class="fa fa-eye"></i> Digunakan untuk melihat detail pesan</li>
                    <li>- Pada detail terdapat symbol <i class="fa fa-reply"></i> Digunakan untuk membalas dengan email, dan <span class="label label-success">Send To [Nomor Telpon] Chat langsung dengan fikur whatsapp</span></li>
                </ul>
            </div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Message Customer</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      
                      <div class="table-responsive">
                        <table class="table table-hover">
                          <thead>
                            <tr>
                              <th>#</th>
                              <th>Nama</th>
                              <th>Email</th>
                              <th>Nomor Telepon</th>
                              <th><i class="fa fa-clock-o"></i></th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php 
                              $this->load->model('Time_lib');
                              $m_lib = new Time_lib();
                             ?>
                            <?php foreach ($data_pesan as $dt_pesan): ?>
                              <tr>
                                <td><a class="btn btn-danger btn-xs" href="<?php echo base_url('admin/remove_pesan/'.$dt_pesan->Kd_Pesan); ?>" title="<?php echo $dt_pesan->Nama ?>"><i class="fa fa-trash"></i> </a></td>
                                <td>
                                  <?php if ($dt_pesan->Status == "0"): ?>
                                     <i class="fa red fa-heart"></i> <?php echo $dt_pesan->Nama ?>
                                   <?php else: ?>
                                     <i class="fa green fa-heart"></i> <?php echo $dt_pesan->Nama ?>
                                   <?php endif ?> 
                                </td>
                                <td><?php echo $dt_pesan->Email ?></td>
                                <td><a class="btn btn-sm btn-success" href="#" role="button"> <i class="fa fa-whatsapp"></i> <?php echo $dt_pesan->No_Tlpn ?></a></td>
                                <td><?php echo $m_lib->getHowLongAgo($dt_pesan->Create_at) ?></td>
                                <td><a class="btn btn-sm btn-primary" data-toggle="modal" href='#modal-id<?php echo $dt_pesan->Kd_Pesan; ?>'><i class="fa fa-eye"></i></a></td>
                              </tr>

                              <div class="modal fade" id="modal-id<?php echo $dt_pesan->Kd_Pesan ?>">
                                <div class="modal-dialog ">
                                  <div class="modal-content ">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                      <h4 class="modal-title">From <?php echo $dt_pesan->Nama ?></h4>
                                      <p><a href="mailto:<?php echo $dt_pesan->Email ?>?subject=Reply Email&amp;body=<?php echo ("Pertanyaan ? %0A%0A".$dt_pesan->Pesan.'%0A%0A Jawaban ? %0A%0A'); ?>" "email me"><i class="fa fa-mail-reply"></i> Reply to <?php echo $dt_pesan->Email ?></a></p>
                                      <p> <?php $num_phone = (int) $dt_pesan->No_Tlpn; ?>
                                          <a href="https://api.whatsapp.com/send?phone=<?php echo ('62'.intval($num_phone, 8)); ?>&text=Menanggapi%20Pesan%20Dari%20<?php echo $dt_pesan->Nama ?>">
                                              <span class="label label-success"><i class="fa fa-whatsapp"></i> Send to <?php echo $dt_pesan->No_Tlpn ?></span>
                                          </a> | <i class="fa fa-map-marker"></i> <?php echo $dt_pesan->No_Tlpn ?> | <i class="fa fa-clock-o"></i> <span class="label label-info"> Date Inbox <?php echo $dt_pesan->Create_at ?></span>  
                                      </p>  
                                    </div>
                                    <form action="<?php echo base_url('admin/action_edit_data_pesan') ?>" method="post">
                                      <div class="modal-body">
                                        <p><?php echo $dt_pesan->Pesan ?></p>
                                        <input type="hidden" name="kode_pesan" id="inputKode_pesan" class="form-control" value="<?php echo $dt_pesan->Kd_Pesan ?>" required="required">
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup Jendela</button>
                                        <?php if ($dt_pesan->Status == "0"): ?>
                                           <button type="submit" class="btn btn-primary">Selesai Baca</button>
                                        <?php endif ?>
                                       
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                            <?php endforeach ?>
                          </tbody>
                        </table>
                        


                      </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <?php $this->load->view('adminpages/footer'); ?>
      </div>
    </div>

    

    <?php require_once(APPPATH .'views/include/admin/admin_script.php'); ?>
  </body>
</html>