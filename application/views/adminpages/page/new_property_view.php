<!DOCTYPE html>
<html lang="en">
  <head>
    <?php require_once(APPPATH .'views/include/admin/admin_style.php'); ?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url('admin/home') ?>" class="site_title"><span>Karya Sehati Utama</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url('asset_admin/images/Logo_KSU.png') ?> " alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>Administrator</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <?php $this->load->view('adminpages/menu_bar'); ?>

          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <?php $this->load->view('adminpages/nav_menu'); ?>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Tambah Produk</h3>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="bs-callout bs-callout-info">
              <h4>* Penjelasan Singkat</h4>
              <ul>
                  Berikut halaman input data produk baru, pada halaman ini digunakan untuk memasukan produk baru, baik nama produk, logo property, cover produk dan penjelasan singkat tentang produk, penjelasan pada halaman ini sebisa mungkin singkat untuk di tampilkan pada bagian halaman utama, untuk deskripsi detailnya dapat di isikan pada link buttom <strong><a class="btn btn-xs btn-warning" href="#" role="button"><i class="fa fa-list"></i> Add Detail Produk</a> tombol akan tampil setelah anda menambahkan produk.</strong>
              </ul>
            </div>

            <div class="bs-callout bs-callout-warning">
              <h4>* Catatan Pengguna</h4>
              <ul>
                  <li>- Hati Hati Ketika Menakan Tombol <strong> <i class="fa fa-trash"></i> Hapus </strong>, Belum ada notifikasi dalam menghapus data</li>
                  <li>- Untuk mengubah logo produk dapat meng-click logo pada list table, maka akan muncul form untuk mengubah logo</li>
                  <li>- Untuk mengubah cover produk dapat meng-click cover pada list table, maka akan muncul form untuk mengubah cover</li>
                  <li>- <a class="btn btn-xs btn-success" href="" role="button"><i class="fa fa-home"></i> Add Type Produk [Nama Produk]</a>, Tombol di gunakan untuk menambahkan produk property , type rumah dalam produk tersebut</li>
                  <li>- Publish / Suspanse, dapat di click, maka form untuk mengubah postingan di publik atau di non publik</li>
                  <li>- Tombol <strong><i class="fa fa-pencil"></i></strong> digunakan untuk mengubah nama produk dan deskripsi produk</li>
              </ul>
            </div>

            <?php if (isset($_SESSION['message_data'])): ?>
              <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
                <?php echo $_SESSION['message_data'] ?>
              </div>
              <?php endif ?>

              <?php if (isset($_SESSION['error_data'])): ?>
              <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
                <?php echo $_SESSION['error_data'] ?>
              </div>
            <?php endif ?>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Produk Property</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                     <form action="<?php echo base_url('admin/action_save_produk'); ?>" method="post" enctype="multipart/form-data">
                       
                       <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <div class="form-group">
                               <label for="">Nama Property</label>
                               <input type="text" name="nama_property" id="inputNama_property" class="form-control" value="" required="required" title="">
                            </div>
                           <div class="form-group">
                             <label for="">Logo Property</label>
                             <input type="file" name="img_logo" class="form-control" id="" onchange="readURL(this)" placeholder="">
                             <label for="">Preview Logo</label>
                             <img id="blah" src="" style="display: none;" class="img-responsive" alt="Preview">
                           </div>

                           <div class="form-group">
                               <label for="">Cover Produk</label>
                               <input type="file" name="img_cover" class="form-control" id="" onchange="readCoverURL(this)" placeholder="">
                               <label for="">Preview Cover</label>
                               <img id="cover" src="" style="display: none;" class="img-responsive" alt="Preview">
                            </div>
                       </div>
                       <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
                            <div class="form-group">
                               <label for="">Cerita Singkat Latar Belakang Produk</label>
                               <textarea name="deskripsi" id="ckeditor1" class="form-control" rows="10" required="required"></textarea>
                            </div>
                          
                            
                       </div>
                       <div class="col-lg-12">
                        <div class="form-group pull-right">
                          <button type="reset" class="btn btn-danger">Reset</button>
                          <button type="submit" class="btn btn-success">Save</button>
                        </div>
                       </div>

                     </form>

                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>List Produk Property</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                     <div class="table-responsive">
                       <table id="datatable" class="table table-hover">
                         <thead>
                           <tr>
                             <th><i class="fa fa-trash"></i> Hapus</th>
                             <th><i class="fa fa-image"></i> Logo Produk</th>
                             <th><i class="fa fa-image"></i> Cover Produk</th>
                             <th>Tambah Type Produk</th>
                             <th>Tambah Detail Produk</th>
                             <th>Tambah Galeri</th>
                             <th>Publish / Suspanse</th>
                             <th>Create At</th>
                             <th>Edit / View</th>
                           </tr>
                         </thead>
                         <tbody>
                          <?php $this->load->model('Time_lib'); $m_time = new Time_lib(); ?>

                          <?php foreach ($data_produk as $dt_produk): ?>
                           <tr>
                             <td><a class="btn btn-xs btn-danger disabled" href="<?php echo base_url('admin/action_delete_produk/'.$dt_produk->Kd_Produk); ?>" role="button"><i class="fa fa-trash"></i></a></td>
                             <td><a data-toggle="modal" href='#logo<?php echo $dt_produk->Kd_Produk ?>'><img width="100" height="50" src="<?php echo base_url('storage_img/img_logo_produk/'.$dt_produk->Logo_Produk); ?>" class="img-responsive" alt="<?php echo $dt_produk->Nama_Produk ?>"></a></td>
                             <td><a data-toggle="modal" href='#cover<?php echo $dt_produk->Kd_Produk ?>'><img width="50" height="50" src="<?php echo base_url('storage_img/img_cover_produk/'.$dt_produk->Cover_Produk); ?>" class="img-responsive" alt="<?php echo $dt_produk->Nama_Produk ?>"></a></td>

                             <td><a class="btn btn-xs btn-success" href="<?php echo base_url('admin/type-produk/'.$dt_produk->Kd_Produk) ?>" role="button"><i class="fa fa-home"></i> Add Type</a></td>
                             
                             <td><a class="btn btn-xs btn-warning" href="<?php echo base_url('admin/add_detail_produk/'.$dt_produk->Kd_Produk) ?>" role="button"><i class="fa fa-list"></i> Add Detail</a></td>
                             <td><a class="btn btn-xs btn-info" href="<?php echo base_url('admin/add_gallery_proyek/'.$dt_produk->Kd_Produk) ?>" role="button"><i class="fa fa-plus"></i> Add Galeri</a></td>
                             <td>
                              <?php if ($dt_produk->Status == 1): ?>
                                <a data-toggle="modal" href="#edit-status<?php echo $dt_produk->Kd_Produk ?>"><span class="label label-info"><i class="fa fa-pencil"></i> Publish</span></a>
                              <?php else: ?>
                                 <a data-toggle="modal" href="#edit-status<?php echo $dt_produk->Kd_Produk ?>"><span class="label label-danger"><i class="fa fa-pencil"></i> Suspanse</span></a>
                              <?php endif ?>
                             </td>
                             <td><?php echo $m_time->getHowLongAgo($dt_produk->Create_At); ?></td>
                             <td>
                               <a class="btn btn-sm btn-info" data-toggle="modal" href='#edit_data<?php echo $dt_produk->Kd_Produk ?>' role="button"><i class="fa fa-pencil"></i></a>
                               <a class="btn btn-sm btn-default" data-toggle="modal" href='#modal-id<?php echo $dt_produk->Kd_Produk ?>' role="button"><i class="fa fa-eye"></i></a>
                             </td>
                           </tr>

                           <div class="modal fade" id="modal-id<?php echo $dt_produk->Kd_Produk ?>">
                             <div class="modal-dialog modal-lg">
                               <div class="modal-content">
                                 <div class="modal-header">
                                   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                   <h4 class="modal-title">View <?php echo $dt_produk->Nama_Produk ?></h4>
                                 </div>
                                 <div class="modal-body">
                                   <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                     <label for="">Logo Produk</label>
                                     <img src="<?php echo base_url('storage_img/img_logo_produk/'.$dt_produk->Logo_Produk); ?>" class="img-responsive" alt="<?php echo $dt_produk->Nama_Produk ?>">
                                     <label for="">Cover Produk</label>
                                     <img src="<?php echo base_url('storage_img/img_cover_produk/'.$dt_produk->Cover_Produk); ?>" class="img-responsive" alt="<?php echo $dt_produk->Nama_Produk ?>">
                                   </div>
                                   <div class="col-xs-12 col-sm-6 col-md-8 col-lg-8">
                                      <label for="">Deskripsi Singkat <?php echo $dt_produk->Nama_Produk ?></label>
                                     <p><?php echo $dt_produk->Deskripsi_1 ?></p>
                                   </div>
                                 </div>
                                 <div class="modal-footer">
                                   <button type="button" class="btn btn-default" data-dismiss="modal">Tutup Jendela</button>
                                 </div>
                               </div>
                             </div>
                           </div>

                           <div class="modal fade" id="edit_data<?php echo $dt_produk->Kd_Produk ?>">
                             <div class="modal-dialog modal-lg">
                               <div class="modal-content">
                                 <div class="modal-header">
                                   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                   <h4 class="modal-title">Edit Produk <?php echo $dt_produk->Nama_Produk ?></h4>
                                 </div>
                                 <form action="<?php echo base_url('admin/action_edit_produk'); ?>" method="post" accept-charset="utf-8">
                                 <div class="modal-body">
                                    <div class="form-group">
                                       <label for="">Nama Property</label>
                                       <input type="text" name="nama_property" id="inputNama_property" class="form-control" value="<?php echo $dt_produk->Nama_Produk ?>" required="required" title="">
                                       <input type="hidden" name="kode_produk" id="inputKodeProduk" class="form-control" value="<?php echo $dt_produk->Kd_Produk ?>" required="required" title="">
                                    </div>
                                    <div class="form-group">
                                       <label for="">Cerita Singkat Latar Belakang Produk</label>
                                       <textarea name="deskripsi" id="ckeditor2" class="form-control" rows="10" required="required"><?php echo $dt_produk->Deskripsi_1 ?></textarea>
                                    </div>
                                 </div>
                                 <div class="modal-footer">
                                   <button type="button" class="btn btn-default" data-dismiss="modal">Tutup Jendela</button>
                                   <button type="submit" class="btn btn-primary">Update Produk</button>
                                 </div>
                                </form>
                               </div>
                             </div>
                           </div>

                           <div class="modal fade" id="logo<?php echo $dt_produk->Kd_Produk ?>">
                             <div class="modal-dialog">
                               <div class="modal-content">
                                 <div class="modal-header">
                                   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                   <h4 class="modal-title">Logo <?php echo $dt_produk->Nama_Produk ?></h4>
                                 </div>
                                 <form action="<?php echo base_url('admin/action_edit_logo_produk'); ?>" method="post" enctype="multipart/form-data">
                                   <div class="modal-body">

                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                      <label for="">Logo Property</label>
                                      <input type="file" name="img_logo" class="form-control" id="" onchange="readUpdateLogoURL(this)" placeholder="">
                                      <input type="hidden" name="kode_produk" id="inputKode_" class="form-control" value="<?php echo $dt_produk->Kd_Produk ?>" required="required">
                                      <label for="">Preview New Logo</label>
                                      <img id="u_logo" src="" style="display: none;" class="img-responsive" alt="Preview">
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                      <label for="">Preview Old Logo</label>
                                      <img src="<?php echo base_url('storage_img/img_logo_produk/'.$dt_produk->Logo_Produk); ?>" class="img-responsive" alt="<?php echo $dt_produk->Nama_Produk ?>">
                                    </div>
                                     
                                   </div>
                                   <div class="modal-footer">
                                     <button type="button" class="btn btn-default" data-dismiss="modal">Tutup Jendela</button>
                                     <button type="submit" class="btn btn-primary">Update Logo</button>
                                   </div>
                                 </form>
                               </div>
                             </div>
                           </div>

                           <div class="modal fade" id="cover<?php echo $dt_produk->Kd_Produk ?>">
                             <div class="modal-dialog">
                               <div class="modal-content">
                                 <div class="modal-header">
                                   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                   <h4 class="modal-title">Cover <?php echo $dt_produk->Nama_Produk ?></h4>
                                 </div>
                                 <form action="<?php echo base_url('admin/action_edit_cover_produk'); ?>" method="post" enctype="multipart/form-data">
                                   <div class="modal-body">

                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                      <label for="">Cover Property</label>
                                      <input type="file" name="img_cover" class="form-control" id="" onchange="readUpdateCoverURL(this)" placeholder="">
                                      <input type="hidden" name="kode_produk" id="inputKode_" class="form-control" value="<?php echo $dt_produk->Kd_Produk ?>" required="required">
                                      <label for="">Preview New Cover</label>
                                      <img id="u_cover" src="" style="display: none;" class="img-responsive" alt="Preview">
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                      <label for="">Preview Old Cover</label>
                                      <img src="<?php echo base_url('storage_img/img_cover_produk/'.$dt_produk->Cover_Produk); ?>" class="img-responsive" alt="<?php echo $dt_produk->Nama_Produk ?>">
                                    </div>
                                     
                                   </div>
                                   <div class="modal-footer">
                                     <button type="button" class="btn btn-default" data-dismiss="modal">Tutup Jendela</button>
                                     <button type="submit" class="btn btn-primary">Update Cover</button>
                                   </div>
                                 </form>
                               </div>
                             </div>
                           </div>

                           <div class="modal fade" id="edit-status<?php echo $dt_produk->Kd_Produk ?>">
                             <div class="modal-dialog">
                               <div class="modal-content">
                                 <div class="modal-header">
                                   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                   <h4 class="modal-title">Ubah Status <?php echo $dt_produk->Nama_Produk ?></h4>
                                 </div>
                                 <div class="modal-body">
                                   <form action="<?php echo base_url('admin/action_edit_data_status_produk'); ?>" method="POST" role="form">
                                   
                                     <div class="form-group">
                                       <label for="">Ubaha Status Produk</label>
                                       <input type="hidden" name="kode_produk" id="inputKode_" class="form-control" value="<?php echo $dt_produk->Kd_Produk ?>" required="required" pattern="" title="">
                                       <select name="status_produk" id="input" class="form-control" required="required">
                                        <option <?php echo $dt_produk->Status == '1' ? 'selected = "selected"': ''; ?> value="1">Publish</option>
                                        <option <?php echo $dt_produk->Status == '0' ? 'selected = "selected"': ''; ?> value="0">Suspanse</option>
                                       </select>
                                     </div>
                                   
                                     <button type="submit" class="btn btn-primary">Ubah Status</button>
                                   </form>
                                 </div>
                                 
                               </div>
                             </div>
                           </div>
                           <?php endforeach ?>
                         </tbody>
                       </table>
                     </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
        <!-- /page content -->

        <?php $this->load->view('adminpages/footer'); ?>
      </div>
    </div>

    

    <?php require_once(APPPATH .'views/include/admin/admin_script.php'); ?>
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                    $('#blah').css('display', '');
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        function readCoverURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#cover').attr('src', e.target.result);
                    $('#cover').css('display', '');
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        function readUpdateLogoURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#u_logo').attr('src', e.target.result);
                    $('#u_logo').css('display', '');
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        function readUpdateCoverURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#u_cover').attr('src', e.target.result);
                    $('#u_cover').css('display', '');
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
  </body>
</html>