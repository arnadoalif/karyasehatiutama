<!DOCTYPE html>
<html lang="en">
  <head>
    <?php require_once(APPPATH .'views/include/admin/admin_style.php'); ?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url('admin/home') ?>" class="site_title"><span>Karya Sehati Utama</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url('asset_admin/images/Logo_KSU.png') ?> " alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>Administrator</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <?php $this->load->view('adminpages/menu_bar'); ?>

          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <?php $this->load->view('adminpages/nav_menu'); ?>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Type Produk</h3>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="bs-callout bs-callout-info">
                <h4>* Penjelasan Singkat</h4>
                <ul>
                    <li>- Halama untuk menambahkan property / type produk dengan kategori sesuai produk yang di pilih</li>
                    <li>- Lengkapi inputan luas tanah / ukuran tanah</li>
                    <li>- Langkapi inputan Kategori Rumah (1 Lantai, 2 Lantai)</li>
                    <li>- Lengkapi Informasi Detail Bagunan</li>
                    <li>- Lengkapi Informasi Tentang property / rumah</li>
                    <li>- Upload Cover sesuai contoh dan hanya dapat meng-upload 1 gambar cover</li>
                </ul>
            </div>

            <?php if (isset($_SESSION['message_data'])): ?>
              <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
                <?php echo $_SESSION['message_data'] ?>
              </div>
              <?php endif ?>

              <?php if (isset($_SESSION['error_data'])): ?>
              <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
                <?php echo $_SESSION['error_data'] ?>
              </div>
            <?php endif ?>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Tambah Type Produk</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                     <form action="<?php echo base_url('admin/action_save_property'); ?>" method="post" enctype="multipart/form-data">
                       
                       <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                         <div class="form-group">
                             <label for="">Pilih Produk</label>
                             <input type="text" name="nama_produk" class="form-control" readonly value="<?php echo $data_produk->Nama_Produk ?>" required="required" title="">
                             <input type="hidden" name="kode_produk" id="" class="form-control" value="<?php echo $data_produk->Kd_Produk ?>" required="required" pattern="" title="">
                          </div>
                          <div class="form-group">
                             <label for="">Nama Property</label>
                             <input type="text" name="nama_property" class="form-control" value="" required="required" title="">
                          </div>
                          <div class="form-group">
                             <label for="">Luas Tanah / Ukuran Rumah</label>
                             <input type="text" name="luas_tanah" class="form-control" value="" required="required" title="" placeholder="Ex: (12,5 x 15,25 | LB : 100 | LT : 190)">
                          </div>
                          <div class="form-group">
                             <label for="">Kategori Rumah</label>
                             <select name="kategori_rumah" id="inputKategori_rumah" class="form-control" required="required">
                               <option value="" selected>-- Pilih Kategori --</option>
                               <option value="1_lantai">1 Lantai</option>
                               <option value="2_lantai">2 Lantai</option>
                             </select>
                          </div>
                          <div class="form-group">
                             <label for="">Informasi Bagunan</label>
                             <textarea name="informasi_bagunan" id="ckeditor1" class="form-control" rows="3" required="required"></textarea>
                          </div>
                          <div class="form-group">
                             <label for="">Tuliskan Sekilas Type Properti Ini</label>
                             <textarea name="deskripsi" id="ckeditor2" class="form-control" rows="3" required="required"></textarea>
                          </div>
                       </div>
                       <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                          
                          <div class="form-group">
                             <label for="">Cover Property</label>
                             <input type="file" name="img_property" class="form-control" id="" onchange="readURL(this)" placeholder="">
                             <label for="">Preview Cover Property</label>
                             <img id="blah" src="" style="display: none;" class="img-responsive" alt="Preview">
                          </div>
                       </div>
                       <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                          <div class="bs-callout bs-callout-info">
                              <h4>* Penjelasan Singkat</h4>
                              <ul>
                                  <li>- Cover jika produk memiliki jumlah property kurang dari 3 property</li>
                                  <li>- Cover di buat dengan detail, dan ukuran <strong><i>(1214x631)</i></strong></li>
                              </ul>
                          </div>
                          <div class="form-group">
                             <label for="">Example Cover > 3 Property</label>
                             <img src="<?php echo base_url('asset_front/images/asset_new/img_tlogoadi_90.png'); ?>" class="img-responsive" alt="Image">
                          </div>
                       </div>
                       <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                          <div class="bs-callout bs-callout-info">
                              <h4>* Penjelasan Singkat</h4>
                              <ul>
                                  <li>- Cover jika produk memiliki jumlah property lebih dari 3 property</li>
                              </ul>
                          </div>
                          <div class="form-group">
                             <label for="">Example Cover > 3 Property</label>
                             <img width="70%" src="<?php echo base_url('asset_front/images/asset_new/img_produk/04.%20VGA_Tulip.jpg'); ?>" class="img-responsive" alt="Image">
                          </div>
                       </div>
                       <div class="col-lg-12">
                        <div class="form-group pull-right">
                          <button type="reset" class="btn btn-danger">Reset</button>
                          <button type="submit" class="btn btn-success">Save</button>
                        </div>
                       </div>

                     </form>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <?php $this->load->view('adminpages/footer'); ?>
      </div>
    </div>

    

    <?php require_once(APPPATH .'views/include/admin/admin_script.php'); ?>
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                    $('#blah').css('display', '');
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
  </body>
</html>