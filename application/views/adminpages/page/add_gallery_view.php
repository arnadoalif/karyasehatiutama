<!DOCTYPE html>
<html lang="en">
  <head>
    <?php require_once(APPPATH .'views/include/admin/admin_style.php'); ?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url('admin/home') ?>" class="site_title"><span>Karya Sehati Utama</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url('asset_admin/images/Logo_KSU.png') ?> " alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>Administrator</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <?php $this->load->view('adminpages/menu_bar'); ?>

          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <?php $this->load->view('adminpages/nav_menu'); ?>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Buat Album</h3>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="bs-callout bs-callout-info">
                <h4>* Penjelasan Singkat</h4>
                <ul>
                    <li>- Hati Hati Ketika Menakan Tombol <strong> <i class="fa fa-trash"></i> Hapus </strong>, Belum ada notifikasi dalam menghapus data</li>
                    <li>- Halaman digunakan untuk membuat album baru </li>
                    <li>- <a class="btn btn-xs btn-warning" href="" role="button"><i class="fa fa-image"></i> Buat & Edit Gallery</a> Button digunakan untuk menambahkan gambar album, dan dapat mengedit gambar</li>
                    <li>- Tekan gambar pada cover untuk mengedit cover album</li>
                    <li>- Tombol Edit di gunakan untuk mengedit data baik nama dan body text album</li>
                </ul>
            </div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Tambah Album Gallery</h2>
                    <div class="clearfix"></div>
                    <?php if (isset($_SESSION['message_data'])): ?>
                      <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <?php echo $_SESSION['message_data'] ?>
                      </div>
                      <?php endif ?>

                      <?php if (isset($_SESSION['error_data'])): ?>
                      <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <?php echo $_SESSION['error_data'] ?>
                      </div>
                    <?php endif ?>
                  </div>
                  <div class="x_content">
                     <form action="<?php echo base_url('admin/action_save_album'); ?>" method="post" enctype="multipart/form-data">
                       
                       <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <div class="form-group">
                               <label for="">Nama Album</label>
                               <input type="text" name="nama_album" id="inputAlamat" class="form-control" value="" required="required" title="">
                            </div>
                            <div class="form-group">
                               <label for="">Body Nama Album</label>
                               <input type="text" name="body_nama_album" id="inputAlamat" class="form-control" value="" required="required" title="">
                            </div>
                            <div class="form-group">
                               <label for="">Kategori Album</label>
                               <select name="kategori" id="inputKategori" class="form-control" required="required">
                                 <option value="">-- Pilih Kategori --</option>
                                 <option value="proyek">Proyek</option>
                                 <option value="office">Office</option>
                                 <option value="realisasi_pembagunan">Realisasi Pembangunan</option>
                                 <option value="kegiatan_karyawan">Kegiatan Karyawan</option>
                                 <option value="workshop_material">Workshop Material</option>
                               </select>
                            </div>
                           <div class="form-group">
                             <label for="">Upload Cover Album <sup style="color:red">hanya dapat memakai 1 gambar</sup></label>
                             <input type="file" name="img_cover_gallery" class="form-control" id="" onchange="readURL(this)" placeholder="">

                             <div class="col-md-6 col-lg-6">
                                <label>New Preview Img</label>
                                <img id="blah" src="" style="display: none;" class="img-responsive" alt="Preview">
                             </div>
                           </div>
                       </div>
                       <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group">
                               <label for="">Example View Album</label>
                               <img src="<?php echo base_url('asset_front/images/asset_new/album_cover.png'); ?>" class="img-responsive" alt="Image">
                            </div>
                       </div>
                       <div class="col-lg-12">
                        <div class="form-group pull-right">
                          <button type="reset" class="btn btn-danger">Reset</button>
                          <button type="submit" class="btn btn-success">Buat Album</button>
                        </div>
                       </div>

                     </form>

                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Data Album Gallery</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                     
                     <div class="table-responsive">
                       <table id="datatable" class="table table-hover">
                         <thead>
                           <tr>
                             <th><i class="fa fa-trash"></i> Hapus</th>
                             <th><i class="fa fa-image"></i> Cover</th>
                             <th>Nama Album</th>
                             <th>Body Text</th>
                             <th>Kategori</th>
                             <th><i class="fa fa-calendar"></i> Create At</th>
                             <th>Edit / Create Gallery</th>
                           </tr>
                         </thead>
                         <tbody>
                          <?php $this->load->model('Time_lib'); $time_lib = new Time_lib(); ?>
                          <?php foreach ($data_gallery as $dt_gallery): ?>
                           <tr>
                             <td><a class="btn btn-danger btn-xs" href="<?php echo base_url('admin/remove_gallery/'.$dt_gallery->Kd_Gallery); ?>" role="button"><i class="fa fa-trash"></i> Hapus</a></td>
                             <td><img width="90" src="<?php echo base_url('storage_img/img_cover_gallery/'.$dt_gallery->Cover_Name); ?>" class="img-responsive" alt="Image"></td>
                             <td><?php echo $dt_gallery->Nama_Gallery ?></td>
                             <td><?php echo $dt_gallery->Body_Nama_Gallery ?></td>
                             <td><?php echo str_replace('_',' ', strtoupper($dt_gallery->Kategori)) ?></td>
                             <td><?php echo $time_lib->getHowLongAgo($dt_gallery->Create_At) ?></td>
                             <td>
                               <a class="btn btn-xs btn-primary" data-toggle="modal" href='#modal-id<?php echo $dt_gallery->Kd_Gallery ?>' role="button"><i class="fa fa-pencil"></i></a>
                               <a class="btn btn-xs btn-warning" href="<?php echo base_url('admin/add_album/'.$dt_gallery->Slug_Url); ?>" role="button"><i class="fa fa-image"></i> Buat & Edit Gallery</a>
                             </td>
                           </tr>

                           <div class="modal fade" id="modal-id<?php echo $dt_gallery->Kd_Gallery ?>">
                             <div class="modal-dialog">
                               <div class="modal-content">
                                 <div class="modal-header">
                                   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                   <h4 class="modal-title">Ubah Album <?php echo $dt_gallery->Nama_Gallery ?></h4>
                                 </div>
                                 <form action="<?php echo base_url('admin/action_update_data_gallery'); ?>" method="post" enctype="multipart/form-data">
                                   <div class="modal-body">
                                      <div class="form-group">
                                         <label for="">Nama Album</label>
                                         <input type="text" name="nama_album" id="inputAlamat" class="form-control" value="<?php echo $dt_gallery->Nama_Gallery ?>" required="required" title="">
                                         <input type="hidden" name="kode_gallery" id="inputAlamat" class="form-control" value="<?php echo $dt_gallery->Kd_Gallery ?>" required="required" title="">
                                      </div>
                                      <div class="form-group">
                                         <label for="">Body Nama Album</label>
                                         <input type="text" name="body_nama_album" id="inputAlamat" class="form-control" value="<?php echo $dt_gallery->Body_Nama_Gallery ?>" required="required" title="">
                                      </div>
                                      <div class="form-group">
                                         <label for="">Kategori Album</label>
                                         <select name="kategori" id="inputKategori" class="form-control" required="required">
                                           <option value="">-- Pilih Kategori --</option>
                                           <option <?php echo $dt_gallery->Kategori == 'proyek' ? 'selected = "selected"': ''; ?> value="proyek">Proyek</option>
                                           <option <?php echo $dt_gallery->Kategori == 'office' ? 'selected = "selected"': ''; ?> value="office">Office</option>
                                           <option <?php echo $dt_gallery->Kategori == 'realisasi_pembagunan' ? 'selected = "selected"': ''; ?> value="realisasi_pembagunan">Realisasi Pembangunan</option>
                                           <option <?php echo $dt_gallery->Kategori == 'kegiatan_karyawan' ? 'selected = "selected"': ''; ?> value="kegiatan_karyawan">Kegiatan Karyawan</option>
                                            <option <?php echo $dt_gallery->Kategori == 'workshop_material' ? 'selected = "selected"': ''; ?> value="workshop_material">Workshop Material</option>
                                         </select>
                                      </div>
                                     <div class="form-group">
                                       <label for="">Upload Cover Album <sup style="color:red">hanya dapat memakai 1 gambar</sup></label>
                                       <input type="file" name="img_cover_gallery" class="form-control" id="" onchange="readURL(this)" placeholder="">

                                       <div class="col-md-6 col-lg-6">
                                          <label>New Preview Img</label>
                                          <img id="edit" src="" style="display: none;" class="img-responsive" alt="Preview">
                                          <input type="hidden" name="old_img_gallery" id="inputOld_img_gallery" class="form-control" value="<?php echo $dt_gallery->Cover_Name ?>" required="required">
                                       </div>
                                       <div class="col-md-6 col-lg-6">
                                          <label>Old Preview Img</label>
                                          <img src="<?php echo base_url('storage_img/img_cover_gallery/'.$dt_gallery->Cover_Name); ?>" class="img-responsive" alt="Preview">
                                       </div>
                                     </div>
                                   </div>
                                   <div class="modal-footer">
                                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                     <button type="submit" class="btn btn-primary">Update</button>
                                   </div>
                                 </form>
                               </div>
                             </div>
                           </div>

                          <?php endforeach ?>
                         </tbody>
                       </table>
                     </div>

                  </div>
                </div>
              </div>
          </div>
        <!-- /page content -->

        <?php $this->load->view('adminpages/footer'); ?>
      </div>
    </div>

    

    <?php require_once(APPPATH .'views/include/admin/admin_script.php'); ?>
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                    $('#blah').css('display', '');
                }

                reader.onload = function (e) {
                    $('#edit').attr('src', e.target.result);
                    $('#edit').css('display', '');
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
  </body>
</html>