<!DOCTYPE html>
<html lang="en">
  <head>
    <?php require_once(APPPATH .'views/include/admin/admin_style.php'); ?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url('admin/home') ?>" class="site_title"><span>Karya Sehati Utama</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url('asset_admin/images/Logo_KSU.png') ?> " alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>Administrator</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <?php $this->load->view('adminpages/menu_bar'); ?>

          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <?php $this->load->view('adminpages/nav_menu'); ?>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Divisi Perusahaan</h3>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Input Divisi</h2>
                    <div class="clearfix"></div>
                    <?php if (isset($_SESSION['message_data'])): ?>
                      <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <?php echo $_SESSION['message_data'] ?>
                      </div>
                      <?php endif ?>

                      <?php if (isset($_SESSION['error_data'])): ?>
                      <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <?php echo $_SESSION['error_data'] ?>
                      </div>
                    <?php endif ?>
                  </div>
                  <div class="x_content">
                     <form action="<?php echo base_url('admin/action_save_divisi'); ?>" method="post" enctype="multipart/form-data">
                       
                       <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <div class="form-group">
                               <label for="">Nama Divisi</label>
                               <input type="text" name="nama_divisi" id="inputDivisi" class="form-control" value="" required="required" title="">
                            </div>
                           <div class="form-group">
                             <label for="">Upload Cover Divisi <sup style="color:red">hanya dapat memakai 1 gambar</sup></label>
                             <input type="file" name="img_divisi" class="form-control" id="" onchange="readURL(this)" placeholder="">

                             <div class="col-md-6 col-lg-6">
                                <label>New Preview Img</label>
                                <img id="blah" src="" style="display: none;" class="img-responsive" alt="Preview">
                             </div>
                             <!-- <div class="col-md-6 col-lg-6">
                                <label>Old Preview Img</label>
                                <img src="" style="display: none;" class="img-responsive" alt="Preview">
                             </div> -->
                           </div>

                       </div>
                       <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

                            <div class="form-group">
                               <label for="">Sekilas tentang divisi</label>
                               <textarea name="deskripsi_divisi" id="ckeditor1" class="form-control" rows="3" required="required"></textarea>
                            </div>
                            
                       </div>
                       <div class="col-lg-12">
                        <div class="form-group pull-right">
                          <button type="reset" class="btn btn-danger">Setel Ulang</button>
                          <button type="submit" class="btn btn-success">Simpan</button>
                        </div>
                       </div>

                     </form>

                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Data Divisi </h2>
                    <div class="clearfix"></div>
                  </div>
                 <div class="bs-callout bs-callout-warning">
                    <h4>* Catatan Penggunaan</h4>
                    <ul>
                        <li>- Hati Hati Ketika Menakan Tombol <strong> <i class="fa fa-trash"></i> Hapus </strong>, Belum ada notifikasi dalam menghapus data</li>
                        <li>- Edit Deskripsi divisi dapat di lakukan langsung pada tombol view <a class="btn btn-xs btn-default" href="#" role="button"><i class="fa fa-eye"></i></a></li>
                        <li>- Edit Nama Divisi dan Gambar Divisi dapat dilakukan pada tombol edit <a class="btn btn-xs btn-primary" href="#" role="button"><i class="fa fa-pencil"></i></a> </li>
                    </ul>
                  </div>
                  <div class="x_content">
                    <div class="table-responsive">
                      <table id="datatable" class="table table-hover">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th><i class="fa fa-trash"></i> Hapus</th>
                            <!-- <th>No Urut</th> -->
                            <th><i class="fa fa-image"></i> Cover</th>
                            <th>Nama Divisi</th>
                            <!-- <th>Status</th> -->
                            <th>Create</th>
                            <th>Update</th>
                            <th>Edit / View</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $this->load->model('Time_lib'); $time_lib = new Time_lib(); ?>
                          <?php $no = 1; foreach ($data_divisi as $dt_divisi): ?>
                          <tr>
                            <td><?php echo $no++ ?></td>
                            <td><a class="btn btn-xs btn-danger" href="<?php echo base_url('admin/remove_divisi/'.$dt_divisi->Kd_Divisi); ?>" role="button"><i class="fa fa-trash"></i> Hapus</a></td>
                            <!-- <td>
                              <?php if ($dt_divisi->No_Urut == 0): ?>
                                <a class="btn btn-xs btn-default" data-toggle="modal" href='#update<?php echo $dt_divisi->Kd_Divisi ?>' role="button">Set No Urut</a>
                              <?php else: ?>
                                <a class="btn btn-xs btn-info" data-toggle="modal" href='#update<?php echo $dt_divisi->Kd_Divisi ?>' role="button"><?php echo "Grid Urut ".$dt_divisi->No_Urut ?> / <i class="fa fa-pencil"></i> Update No Urut</a>
                              <?php endif ?>
                            </td> -->
                            <td><img width="80" height="80" src="<?php echo base_url('storage_img/img_divisi/'.$dt_divisi->Cover_Name); ?>" class="img-responsive" alt="Image"></td>
                            <td><?php echo $dt_divisi->Nama_Divisi ?></td>
                            <!-- <td>
                              <?php if ($dt_divisi->No_Urut == 0): ?>
                                <span class="label label-danger">No Publish</span>
                              <?php else: ?>
                                <span class="label label-primary">Publish</span>
                              <?php endif ?>
                            </td> -->
                            <td><?php echo $time_lib->getHowLongAgo($dt_divisi->Create_at) ?></td>
                            <td>
                              <?php if (!empty($dt_divisi->Update_at)): ?>
                                <?php echo $time_lib->getHowLongAgo($dt_divisi->Update_at) ?>
                              <?php else: ?>
                                No Update
                              <?php endif ?>
                            </td>
                            <td>
                              <a class="btn btn-default btn-xs" data-toggle="modal" href='#modal-id<?php echo $dt_divisi->Kd_Divisi ?>' role="button"><i class="fa fa-pencil"></i></a>
                              <a class="btn btn-primary btn-xs" data-toggle="modal" href='#update-data<?php echo $dt_divisi->Kd_Divisi ?>' role="button"><i class="fa fa-eye"></i></a>
                            </td>
                          </tr>

                          <div class="modal fade" id="update<?php echo $dt_divisi->Kd_Divisi ?>">
                            <div class="modal-dialog">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                  <h4 class="modal-title">Modal title</h4>
                                </div>
                                <div class="modal-body">
                                  <form action="<?php echo base_url('admin/action_update_devisi_no_urut'); ?>" method="post">
                                    
                                  <div class="form-group">
                                     <label>Pilih Nomor Urut Sekarang</label>
                                     <input type="text" readonly name="No_urut_sebelumnya" id="inputKode_div" class="form-control" value="<?php echo $dt_divisi->No_Urut ?>" required="required">
                                  </div>

                                  <div class="form-group">
                                     <label>Pilih Nomor Urut Baru</label>
                                     <select name="No_urut">
                                        <option value="0">Defaultkan Nomor</option>
                                      <?php for ($i = 1; $i <= count($data_divisi); $i++): ?>
                                        <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                      <?php endfor ?>
                                       
                                     </select>
                                     <input type="hidden" name="kode_divisi" id="inputKode_div" class="form-control" value="<?php echo $dt_divisi->Kd_Divisi ?>" required="required">
                                     <input type="hidden" name="No_urut_sebelumnya" id="inputKode_div" class="form-control" value="<?php echo $dt_divisi->No_Urut ?>" required="required">
                                  </div>

                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Tutup Jendela</button>
                                  <button type="submit" class="btn btn-primary">Update</button>
                                </div>

                                </form>

                              </div>
                            </div>
                          </div>

                          <div class="modal fade" id="update-data<?php echo $dt_divisi->Kd_Divisi ?>">
                            <div class="modal-dialog">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                  <h4 class="modal-title">Update Divisi <?php echo $dt_divisi->Nama_Divisi ?></h4>
                                </div>
                                <form action="<?php echo base_url('admin/action_update_deskripsi_divisi'); ?>" method="post">
                                  <div class="modal-body">
                                    
                                    <div class="form-group">
                                      <label for="">Deskripsi Divisi <?php echo $dt_divisi->Nama_Divisi ?></label>
                                      <p>
                                        <?php echo $dt_divisi->Deskripsi_Divisi; ?>
                                      </p>
                                      <hr>
                                    </div>
                                   
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup Jendela</button>
                                    <button type="form" class="btn btn-primary">Update Deskripsi</button>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>

                          <div class="modal fade" id="modal-id<?php echo $dt_divisi->Kd_Divisi ?>">
                            <div class="modal-dialog">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                  <h4 class="modal-title">Update data <?php echo $dt_divisi->Nama_Divisi ?></h4>
                                </div>
                                <form action="<?php echo base_url('admin/action_update_name_cover'); ?>" method="post" enctype="multipart/form-data">
                                  <div class="modal-body">
                                      
                                      <div class="form-group">
                                           <label for="">Nama Divisi</label>
                                           <input type="text" name="nama_divisi" id="inputDivisi" class="form-control" value="<?php echo $dt_divisi->Nama_Divisi ?>" required="required" title="">
                                           <input type="hidden" name="kode_divisi" id="inputKode" class="form-control" value="<?php echo $dt_divisi->Kd_Divisi ?>" required="required" title="">
                                      </div>
                                      <div class="form-group">
                                        <label for="">Sekilas tentang divisi</label>
                                        <textarea name="deskripsi_divisi" id="ckeditor2" class="form-control" rows="3"><?php echo $dt_divisi->Deskripsi_Divisi ?></textarea>
                                      </div>
                                       <div class="form-group">
                                         <label for="">Upload Cover Divisi <sup style="color:red">hanya dapat memakai 1 gambar</sup></label>
                                         <input type="file" name="img_divisi" class="form-control" id="" onchange="readURL(this)" placeholder="">

                                         <div class="col-md-6 col-lg-6">
                                            <label>New Preview Img</label>
                                            <img id="preview" src="" style="display: none;" class="img-responsive" alt="Preview">
                                         </div>
                                         <div class="col-md-6 col-lg-6">
                                            <label>Old Preview Img</label>
                                            <img src="<?php echo base_url('storage_img/img_divisi/'.$dt_divisi->Cover_Name); ?>" class="img-responsive" alt="Preview">
                                         </div>
                                       </div>

                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>

                          <?php endforeach ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <?php $this->load->view('adminpages/footer'); ?>
      </div>
    </div>

    

    <?php require_once(APPPATH .'views/include/admin/admin_script.php'); ?>
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                    $('#blah').css('display', '');
                }

                reader.onload = function (e) {
                    $('#preview').attr('src', e.target.result);
                    $('#preview').css('display', '');
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
  </body>
</html>