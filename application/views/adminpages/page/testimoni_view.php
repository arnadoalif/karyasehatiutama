<!DOCTYPE html>
<html lang="en">
  <head>
    <?php require_once(APPPATH .'views/include/admin/admin_style.php'); ?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url('admin/home') ?>" class="site_title"><span>Karya Sehati Utama</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url('asset_admin/images/Logo_KSU.png') ?> " alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>Administrator</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <?php $this->load->view('adminpages/menu_bar'); ?>

          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <?php $this->load->view('adminpages/nav_menu'); ?>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Testimoni</h2>
                    <div class="clearfix"></div>

                    <?php if (isset($_SESSION['message_data'])): ?>
                      <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <?php echo $_SESSION['message_data'] ?>
                      </div>
                      <?php endif ?>

                      <?php if (isset($_SESSION['error_data'])): ?>
                      <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <?php echo $_SESSION['error_data'] ?>
                      </div>
                    <?php endif ?>

                    <div class="bs-callout bs-callout-default">
                      <h4>Keterangan isi Testimoni</h4>
                      Untuk ukuran foto <strong>121x121</strong> extensi (.png, .jpg, .jpeg)
                    </div>

                    <div class="bs-callout bs-callout-warning">
                      <h4>Warning</h4>
                      Jangan tekan tombol hapus, tomboh hapus masih di bawah mantenance programmer
                    </div>

                  </div>
                  <div class="x_content">
                     <form action="<?php echo base_url('admin/action_save_testimoni'); ?>" method="post" enctype="multipart/form-data">
                       
                       <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <div class="form-group">
                               <label for="">Nama Testimoni</label>
                               <input type="text" name="nama_testimoni" id="inputAlamat" class="form-control" value="" required="required" title="">
                            </div>
                            <div class="form-group">
                               <label for="">Alamat / Alamat Komplek</label>
                               <input type="text" name="alamat_testimoni" id="inputAlamat" class="form-control" value="" required="required" title="">
                            </div>
                            <div class="form-group">
                               <label for="">Avatar Testimoni</label>
                               <input type="file" name="avatar_testimoni" class="form-control" id="" onchange="readURL(this)" placeholder="">
                            </div>
                            <div class="form-group">
                               <label for="">Preview Avatar</label>
                               <img id="blah" src="" style="display: none;" class="img-responsive" alt="Preview">
                            </div>
                       </div>
                       <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group">
                               <label for="">Isi Testimoni</label>
                               <textarea name="isi_testimoni" class="form-control" rows="3" required="required"></textarea>
                            </div>
                       </div>
                       <div class="col-lg-12">
                        <div class="form-group pull-right">
                          <button type="reset" class="btn btn-danger">Reset</button>
                          <button type="submit" class="btn btn-success">Save Testimoni</button>
                        </div>
                       </div>

                     </form>

                  </div>
                </div>
              </div>
          </div>

          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <h2>Data Testimoni</h2>
              <div class="table-responsive">
                <table id="datatable" class="table table-hover">
                  <thead>
                    <tr>
                      <th><i class="fa fa-trash"></i> Hapus</th>
                      <th><i class="fa fa-image"></i></th>
                      <th>Nama Testimoni</th>
                      <th>Alamat</th>
                      <th>Edit / Open</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($data_testimoni as $dt_testimoni): ?>
                      <tr>
                        <td><a class="btn btn-danger btn-sm" href="<?php echo base_url('admin/action_delete_data_testimoni/'.$dt_testimoni->Kd_Testimoni); ?>" title="<?php echo "Hapus Testimoni ".$dt_testimoni->Nama ?>"><i class="fa fa-trash"></i></a></td>
                        <th> <img src="<?php echo base_url('storage_img/img_testimoni/'.$dt_testimoni->Avatar); ?>" alt="<?php echo $dt_testimoni->Nama ?>"><br><span class="label label-info"><a data-toggle="modal" href='#edit-avatar<?php echo $dt_testimoni->Kd_Testimoni ?>' title="Edit Avatar"><i class="fa fa-pencil"></i> Edit Avatar Testimoni<span class="pf pf-payshop"></span></a></span></th>
                        <th><?php echo $dt_testimoni->Nama ?></th>
                        <th><?php echo $dt_testimoni->Alamat ?></th>
                        <th>
                          <a class="btn btn-info btn-sm" data-toggle="modal" href='#edit-data<?php echo $dt_testimoni->Kd_Testimoni ?>' role="button"><i class="fa fa-pencil"></i></a>
                          <a class="btn btn-default btn-sm" data-toggle="modal" href='#modal-id<?php echo $dt_testimoni->Kd_Testimoni ?>' role="button"><i class="fa fa-eye"></i></a>
                        </th>
                      </tr>

                      <div class="modal fade" id="modal-id<?php echo $dt_testimoni->Kd_Testimoni ?>">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal<?php echo $dt_testimoni->Kd_Testimoni ?>" aria-hidden="true">&times;</button>
                              <h4 class="modal-title">Testimoni Dari <?php echo $dt_testimoni->Nama ?></h4>
                            </div>
                            <form action="<?php echo base_url('admin/action_edit_isi_testimoni'); ?>" method="POST" accept-charset="utf-8">
                              
                              <div class="modal-body">
                                <h4>Isi Testimoni</h4>
                                <?php echo $dt_testimoni->Isi_Testimoni ?>
                                <hr>
                                <h4>Edit Isi Testimoni</h4>
                                <div class="form-group">
                                   <label for="">Isi Testimoni</label>
                                   <textarea name="isi_testimoni" class="form-control" rows="5" required="required"><?php echo $dt_testimoni->Isi_Testimoni ?></textarea>
                                   <input type="hidden" name="kode_testimoni" class="form-control" value="<?php echo $dt_testimoni->Kd_Testimoni ?>" required="required">
                                </div>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup Jendela</button>
                                <button type="submit" class="btn btn-primary">Update Testimoni</button>
                              </div>

                            </form>
                          </div>
                        </div>
                      </div>

                      <div class="modal fade" id="edit-avatar<?php echo $dt_testimoni->Kd_Testimoni ?>">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title">Ubah Avatar <?php echo $dt_testimoni->Nama ?></h4>
                            </div>
                             <form action="<?php echo base_url('admin/action_edit_ubah_avatar'); ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                              <div class="modal-body">
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                  <div class="form-group">
                                     <label for="">New Avatar Testimoni</label>
                                     <input type="file" name="avatar_testimoni" class="form-control" id="" onchange="readURL(this)" placeholder="">
                                     <input type="hidden" name="kode_testimoni" class="form-control" value="<?php echo $dt_testimoni->Kd_Testimoni; ?>" required="required">
                                     <img id="old" src="" style="display: none;" class="img-responsive" alt="Preview">
                                  </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                  <div class="form-group">
                                     <label for="">Avatar Lama</label>
                                     <img src="<?php echo base_url('storage_img/img_testimoni/'.$dt_testimoni->Avatar); ?>" class="img-responsive" alt="Image">
                                  </div>
                                </div>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup Jendela</button>
                                <button type="submit" class="btn btn-primary">Ubah Avatar</button>
                              </div>
                             </form>
                          </div>
                        </div>
                      </div>

                      <div class="modal fade" id="edit-data<?php echo $dt_testimoni->Kd_Testimoni ?>">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title">Update Data Testimoni <?php echo $dt_testimoni->Nama ?></h4>
                            </div>
                            <form action="<?php echo base_url('admin/action_edit_data_testimoni'); ?>" method="post" accept-charset="utf-8">
                              <div class="modal-body">
                                
                                <div class="form-group">
                                   <label for="">Nama Testimoni</label>
                                   <input type="text" name="nama_testimoni" class="form-control" value="<?php echo $dt_testimoni->Nama ?>" required="required" title="">
                                   <input type="hidden" name="kode_testimoni" class="form-control" value="<?php echo $dt_testimoni->Kd_Testimoni ?>" required="required" title="">
                                </div>
                                <div class="form-group">
                                   <label for="">Alamat / Alamat Komplek</label>
                                   <input type="text" name="alamat_testimoni" class="form-control" value="<?php echo $dt_testimoni->Alamat ?>" required="required" title="">
                                </div>

                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup Jendela</button>
                                <button type="submit" class="btn btn-primary">Update</button>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>
                    <?php endforeach ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /page content -->

        <?php $this->load->view('adminpages/footer'); ?>
      </div>
    </div>

    <?php require_once(APPPATH .'views/include/admin/admin_script.php'); ?>
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                    $('#blah').css('display', '');
                }

                 reader.onload = function (e) {
                    $('#old').attr('src', e.target.result);
                    $('#old').css('display', '');
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
  </body>
</html>