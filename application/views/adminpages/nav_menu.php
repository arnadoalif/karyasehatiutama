<div class="nav_menu">
  <nav>
    <div class="nav toggle">
      <a id="menu_toggle"><i class="fa fa-bars"></i></a>
    </div>
    <ul class="nav navbar-nav navbar-right">
      <li class="">
        <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
          <img src="<?php echo base_url('asset_front/images/logo_sari_rosa_asih.png'); ?>" alt="">Administrator
          <span class=" fa fa-angle-down"></span>
        </a>
        <ul class="dropdown-menu dropdown-usermenu pull-right">
          <li><a href="<?php echo base_url('out'); ?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
        </ul>
      </li>

      <?php 
        $this->load->model('Pesan_model'); $this->load->model('Time_lib'); $m_pesan = new Pesan_model(); $m_time = new Time_lib();
        $data_pesan = $m_pesan->view_data_pesan_by_status('Tbl_Pesan', '0')->result();
       ?>

      <li role="presentation" class="dropdown">
        <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
          <i class="fa fa-envelope-o"></i>
          <span class="badge bg-green"><?php echo(count($data_pesan)) ?></span>
        </a>
        <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
          <?php foreach ($data_pesan as $dt_pesan): ?>
          <li>
            <a>
              <span class="image"><i class="fa fa-users"></i></span>
              <span>
                <span><?php echo $dt_pesan->Nama ?></span>
                <span class="time"><?php echo $m_time->getHowLongAgo($dt_pesan->Create_at); ?></span>
              </span>
              <span class="message">
                <?php echo $m_time->limitString($dt_pesan->Pesan, 100).'....' ?>
              </span>
            </a>
          </li>
          <?php endforeach ?>
          <li>
            <div class="text-center">
              <a href="<?php echo base_url('admin/pesan'); ?>">
                <strong>See All Alerts</strong>
                <i class="fa fa-angle-right"></i>
              </a>
            </div>
          </li>
        </ul>
      </li>
      
    </ul>
  </nav>
</div>