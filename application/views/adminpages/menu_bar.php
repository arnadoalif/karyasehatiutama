<!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Dashboard <span class="fa fa-chevron-right"></span></a></li>
                  <li><a><i class="fa fa-home"></i> Home Page <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url('admin/testimoni'); ?>">Testimoni</a></li>
                      <li><a href="<?php echo base_url('admin/slide'); ?>" >Slider </a></li>
                      <li><a href="" >Ads <span class="badge"><i class="fa fa-cogs"></i></span></a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-book"></i> Property & Portfolio <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a>New Property<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="<?php echo base_url('admin/new-property'); ?>">Produk</a></li>
                          <li><a href="<?php echo base_url('admin/data-type-produk'); ?>">Data Type Produk</a></li>
                        </ul>
                      </li>
                      <li><a href="<?php echo base_url('admin/portofolio-property'); ?>">Portfolio Property</a></li>
                    </ul>
                  </li>
                  <li><a href="<?php echo base_url('admin/pesan'); ?>"><i class="fa fa-comments-o"></i> Message <span class="fa fa-chevron-right"></span></a></li>
                  <li><a href="<?php echo base_url('admin/gallery'); ?>"><i class="fa fa-image"></i> Gallery <span class="fa fa-chevron-right"></span></a></li>
                  
                </ul>
              </div>

              <div class="menu_section">
                <h3>Configuration</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-bug"></i> Setting & Divisi <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url('admin/tentang') ?>">Tentang Perusahaan</a></li>
                      <li><a href="<?php echo base_url('admin/divisi'); ?>">Divisi Perusahaan</a></li>
                    </ul>
                  </li>
                      
                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->