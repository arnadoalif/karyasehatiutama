<meta charset="utf-8">
<!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- For Resposive Device -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title> Karya Sehati Utama | Property Yogyakarta </title>
<meta name="keywords" content=""/>
<meta name="description" content="">
<meta name="author" content="">
<link rel="canonical" href="http://www.karyasehati.com/" />
<meta name="robots" content="follow,index" />
<link rel="icon" href="<?php echo base_url('asset_admin/images/Logo_KSU.png'); ?>">
<meta property="og:url"           content="http://www.karyasehati.com/" />
<meta property="og:type"          content="website" />
<meta property="og:title"         content="" />
<meta property="og:description"   content="" />
<meta property="og:image"         content="<?php echo base_url(''); ?>" />

<meta name="google-site-verification" content="p5vWs1PBf3LzHvuCnM9ubQ8RmP181PzHobQR7ON6KX0" />

<!-- Favicon -->
<!-- <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url('asset_front/images/fav-icon/apple-icon-57x57.png'); ?> ">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url('asset_front/images/fav-icon/apple-icon-60x60.png'); ?> ">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url('asset_front/images/fav-icon/apple-icon-72x72.png'); ?> ">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('asset_front/images/fav-icon/apple-icon-76x76.png'); ?> ">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url('asset_front/images/fav-icon/apple-icon-114x114.png'); ?> ">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url('asset_front/images/fav-icon/apple-icon-120x120.png'); ?> ">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url('asset_front/images/fav-icon/apple-icon-144x144.png'); ?> ">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url('asset_front/images/fav-icon/apple-icon-152x152.png'); ?> ">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url('asset_front/images/fav-icon/apple-icon-180x180.png'); ?> ">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url('asset_front/images/fav-icon/android-icon-192x192.png'); ?> ">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url('asset_front/images/fav-icon/favicon-32x32.png'); ?> ">
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url('asset_front/images/fav-icon/favicon-96x96.png'); ?> ">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('asset_front/images/fav-icon/favicon-16x16.pn'); ?> g"> -->
<!-- Custom Css -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset_front/css/custom/style.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset_front/css/responsive/responsive.css'); ?> ">
<!-- Fixing Internet Explorer ______________________________________-->
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<script src="js/html5shiv.js"></script>
<![endif]-->

<style type="text/css">
	.testimonial_subtitle{
	    color: #0aaa7a;
	    font-size: 12px;
	}
	  .testimonial_btn{
	    background-color: #373d4b !important;
	    color: #fff !important;
	 }
	 .seprator {
	    height: 2px;
	    width: 56px;
	    background-color: #0aaa7a;
	    margin: 7px 0 10px 0;
	}
</style>

<style type="text/css">
	
	#custom_carousel .item  .top {
	    overflow:hidden;
	    max-height:300px;
	    margin-bottom:15px;
	}
	#custom_carousel .item {

	    color:#000;
	    background-color:#fff;
	    padding:20px 0;
	    overflow:hidden
	}
	#custom_carousel .item img{
	width:100%;
	height:auto
	}

	#custom_carousel .izq 
	{
	    position:absolute;
	  left: -25px;
	  top:40%;
	  background-image: none;
	  background: none repeat scroll 0 0 #222222;
	  border: 4px solid #FFFFFF;
	  border-radius: 23px;
	  height: 40px;
	  width : 40px;
	  margin-top: 30px;
	}
	/* Next button  */
	#custom_carousel .der 
	{
	       position:absolute;
	  right: -25px !important;
	  top:40%;
	  left:inherit;
	  background-image: none;
	  background: none repeat scroll 0 0 #222222;
	  border: 4px solid #FFFFFF;
	  border-radius: 23px;
	  height: 40px;
	  width : 40px;
	  margin-top: 30px;
	}
	#custom_carousel .controls{

	    overflow:hidden;
	    padding:0;
	    margin:0;
	    white-space: nowrap;
	    text-align: center;
	    position: relative;
	    background:#fff;
	    border:0;
	}
	#custom_carousel .controls .nav{

	    padding:0;
	    margin:0;
	    white-space: nowrap;
	    text-align: center;
	    position: relative;
	    background:#fff;
	    width: auto;
	    border: 0;
	}
	#custom_carousel .controls li {
	    transition: all .5s ease;
	    display: inline-block;
	    max-width: 100px;
	    height: 90px;
	    opacity:.5;
	}
	#custom_carousel .controls li a{
	    padding:0;
	}
	#custom_carousel .controls li img{
	width:100%;
	height:auto
	}

	#custom_carousel .controls li.active {
	    background-color:#fff;
	    opacity:1;
	}
	#custom_carousel .controls a small {
	    overflow:hidden;
	    display:block;
	    font-size:10px;
	    margin-top:5px;
	    font-weight:bold
	}
</style>