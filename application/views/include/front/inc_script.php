<!-- Js File_________________________________ -->
<!-- j Query -->
<script type="text/javascript" src="<?php echo base_url('asset_front/js/jquery-2.1.4.js'); ?> "></script>
<!-- Bootstrap JS -->
<script type="text/javascript" src="<?php echo base_url('asset_front/js/bootstrap/bootstrap.min.js'); ?> "></script>
<!-- Vendor js _________ -->
<!-- Google map js -->
<!-- <script src="https://maps.google.com/maps/api/js"></script> -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD9W8kzf0p7qMy4S7ulgyaFTaodr5vsIYQ"></script> 
<script src="<?php echo base_url('asset_front/js/gmap.js'); ?> "></script>
<!-- Time picker -->
<script type="text/javascript" src="<?php echo base_url('asset_front/vendor/time-picker/jquery.timepicker.min.js'); ?> "></script>

<!-- ui js -->
<script type="text/javascript" src="<?php echo base_url('asset_front/vendor/jquery-ui/jquery-ui.min.js'); ?>"></script>
<!-- Fancybox js -->
<script type="text/javascript" src="<?php echo base_url('asset_front/vendor/fancy-box/jquery.fancybox.pack.js'); ?>"></script>
<!-- Mixitup -->
<script type="text/javascript" src="<?php echo base_url('asset_front/vendor/jquery.mixitup.min.js'); ?>"></script>
<!-- owl.carousel -->
<script type="text/javascript" src="<?php echo base_url('asset_front/vendor/owl-carousel/owl.carousel.min.js'); ?>"></script>
<!-- Vegas Slider -->
<script type="text/javascript" src="<?php echo base_url('asset_front/vendor/vegas/vegas.min.js'); ?>"></script>
<!-- isotop -->
<script type="text/javascript" src="<?php echo base_url('asset_front/vendor/isotope.pkgd.min.js'); ?>"></script>
<!-- Language Stitcher -->
<script type="text/javascript" src="<?php echo base_url('asset_front/vendor/language-switcher/jquery.polyglot.language.switcher.js'); ?>"></script>
<!-- revolution -->
<script src="<?php echo base_url('asset_front/vendor/revolution/jquery.themepunch.tools.min.js'); ?> "></script>
<script src="<?php echo base_url('asset_front/vendor/revolution/jquery.themepunch.revolution.min.js'); ?> "></script>
<script type="text/javascript" src="<?php echo base_url('asset_front/vendor/revolution/revolution.extension.slideanims.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('asset_front/vendor/revolution/revolution.extension.layeranimation.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('asset_front/vendor/revolution/revolution.extension.navigation.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('asset_front/vendor/revolution/revolution.extension.kenburn.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('asset_front/vendor/revolution/revolution.extension.actions.min.js'); ?>"></script>
<!-- appear js -->
<script src="<?php echo base_url('asset_front/vendor/jquery.appear.js'); ?> "></script>
<!-- count to -->
<script src="<?php echo base_url('asset_front/vendor/jquery.countTo.js'); ?> "></script>
<!-- Custom-scrollbar -->
<script src="<?php echo base_url('asset_front/vendor/custom-scrollbar/jquery.mCustomScrollbar.concat.min.js'); ?> "></script>

<!-- Main js -->
<script type="text/javascript" src="<?php echo base_url('asset_front/js/map-script.js'); ?>"></script>

<script type="text/javascript" src="<?php echo base_url('asset_front/js/custom.js'); ?>"></script>
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/56ff8eee3a48b09e318df1c1/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>

<script type="text/javascript">
	$(document).ready(function(ev){
    var items = $(".nav li").length;
    var leftRight=0;
    if(items>5){
        leftRight=(items-5)*50*-1;
    }
    $('#custom_carousel').on('slide.bs.carousel', function (evt) {
        
 
      $('#custom_carousel .controls li.active').removeClass('active');
      $('#custom_carousel .controls li:eq('+$(evt.relatedTarget).index()+')').addClass('active');
    })
    $('.nav').draggable({ 
        axis: "x",
         stop: function() {
            var ml = parseInt($(this).css('left'));
            if(ml>0)
            $(this).animate({left:"0px"});
                if(ml<leftRight)
                    $(this).animate({left:leftRight+"px"});
                    
        }
      
    });
});
</script>