<footer>

	<div class="main_footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
					<div class="about_estate">
						<a href="index.html"><img src="<?php echo base_url('asset_front/images/asset_new/logo_name_ksu.png'); ?> " alt="logo"></a>
						<p>
							Berdiri sejak 2005 Anggota Aktif Real Indonesia (REI) NPA REI : 10.00124, dengan kurang lebih 13 tahun dengan menyaksikan perubahan di kota yogyakarta dan pasar property Yogyakarta.
							
						</p>
						
						<!-- <ul class="icon_footer">
							<li class="border_round tran3s" title="Facebook"><a href="index.html#"><i class="fa fa-facebook"></i></a></li>
							<li class="border_round tran3s" title="Twitter"><a href="index.html#"><i class="fa fa-twitter"></i></a></li>
							<li class="border_round tran3s" title="Google Plus"><a href="index.html#"><i class="fa fa-google-plus"></i></a></li>
							<li class="border_round tran3s" title="Linkedin"><a href="index.html#"><i class="fa fa-linkedin"></i></a></li>
							<li class="border_round tran3s" title="Vimeo"><a href="index.html#"><i class="fa fa-vimeo"></i></a></li>
						</ul> --> <!-- End .icon_header -->

					</div> <!-- End .about_estate -->
				</div>

				<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
					<div class="useful_link">
						<div class="footer_title">
							<h3>Menu</h3>
						</div>
						<ul>
							<li><a href="<?php echo base_url(); ?>" class="tran3s"><i class="fa fa-angle-right"></i> Home</a></li>
							<li><a href="<?php echo base_url('page/portfolio'); ?>" class="tran3s"><i class="fa fa-angle-right"></i> Portofolio</a></li>
							<li><a href="<?php echo base_url('page/kontak'); ?>" class="tran3s"><i class="fa fa-angle-right"></i> Kontak</a></li>
							<li><a href="<?php echo base_url('page/ksu'); ?>" class="tran3s"><i class="fa fa-angle-right"></i> Profil KSU</a></li>
						</ul>

						<ul>
							<?php $this->load->model('Produk_model'); $m_produk = new Produk_model(); $data_produk = $m_produk->view_data_produk_by_status('Tbl_Produk')->result(); ?>
			       			<?php foreach ($data_produk as $dt_produk): ?>
			       				<li><a href="<?php echo base_url('page/detail/'.$dt_produk->Slug_name); ?>" class="tran3s"><i class="fa fa-angle-right"></i> <?php echo $dt_produk->Nama_Produk ?></a></li>
			       			<?php endforeach ?>
						</ul>
					</div> <!-- End .useful_link -->
				</div>

				<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 pull-right">
					<div class="contact_info">
						<div class="footer_title">
							<h3>Kontak Informasi</h3>
						</div>

						<ul>
							<li> Gedung Karya Sehati, komplek Grand Cupuwatu, Jl. Solo km. 11,8 Kalasan, Yogyakarta</li>
							<li><a href="tel:0274496777" class="tran3s">Phone: 0274-496-777</a></li>
							<li><a href="mailto:karyasehatiutama@gmail.com" class="tran3s">Email: karyasehatiutama@gmail.com</a></li>
							<li><a href="www.karyasehati.com" target="_blank" class="tran3s">Web: www.karyasehati.com</a></li>
							<li>Senin - Sabtu 08.30am - 16.30pm <br>
										  Minggu Libur</li>
						</ul>

					</div> <!-- End .contact_info -->
				</div>
			</div> <!-- End .row -->
		</div> <!-- End .container -->
	</div> <!-- End .main_footer -->

	<div class="bottom_footer">
		<div class="container">
			<!-- <div class="text">
				<p>Copyright &copy; 2015 Real Estate Theme Designed by | <a href="http://themeforest.net/user/steelthemes/portfolio" class="tran3s" target="_blank">SteelThemes</a> Developed by | <a href="http://themeforest.net/user/template_path/portfolio" class="tran3s" target="_blank">Template Path</a> </p>
			</div> --> <!-- End .text -->

			<div class="footer_nav">
				<ul>
					<li><a href="" class="tran3s">Home</a></li>
					<li><a href="" class="tran3s">Portofolio</a></li>
					<li><a href="" class="tran3s">Profil KSU</a></li>
				</ul>
			</div> <!-- End .footer_nav -->
		</div>
	</div> <!-- End .bottom_footer -->
</footer>