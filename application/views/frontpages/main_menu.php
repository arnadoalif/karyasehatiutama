<nav class="navbar navbar-default">
	   <!-- Brand and toggle get grouped for better mobile display -->
	   <div class="navbar-header">
	     <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
	       <span class="sr-only">Toggle navigation</span>
	       <span class="icon-bar"></span>
	       <span class="icon-bar"></span>
	       <span class="icon-bar"></span>
	     </button>
	   </div>
	   <!-- Collect the nav links, forms, and other content for toggling -->
	   <div class="collapse navbar-collapse mCustomScrollbar" id="navbar-collapse-1" data-mcs-theme="dark-thin">
		     <ul class="nav navbar-nav">
		       <li><a href="<?php echo base_url('/'); ?>">Home</a></li>
		       <li><a href="<?php echo base_url('page/ksu'); ?>">Profil Ksu</a></li>
		       <li class="dropdown_holder"><a href="">Produk <i class="fa fa-angle-down" aria-hidden="true"></i></a>
		       		<ul class="sub-menu">
		       			<?php $this->load->model('Produk_model'); $m_produk = new Produk_model(); $data_produk = $m_produk->view_data_produk_by_status('Tbl_Produk')->result(); ?>
		       			<?php foreach ($data_produk as $dt_produk): ?>
		       				<li><a href="<?php echo base_url('page/detail/'.$dt_produk->Slug_name); ?>"><?php echo $dt_produk->Nama_Produk ?></a></li>
		       			<?php endforeach ?>
		       			
		       			<!-- <li><a href="<?php echo base_url('page/detail/villagardenia'); ?>">Villa Gardenia</a></li> -->
		       		</ul>
		       </li>
		       <li><a href="<?php echo base_url('page/shop'); ?>">Shop</a></li>
		       <li><a href="<?php echo base_url('page/portofolio'); ?>">Portofolio</a></li>
		       <li><a href="<?php echo base_url('page/galeri'); ?>">Galeri</a></li>
		       <li><a href="<?php echo base_url('page/kontak'); ?>">Kontak</a></li>
		     </ul>
	   </div><!-- /.navbar-collapse -->
	   <a href="<?php echo base_url('page/brosur'); ?>" class="amenities tran3s">Brosur <i class="fa fa-file-pdf-o"></i></a>
	</nav>