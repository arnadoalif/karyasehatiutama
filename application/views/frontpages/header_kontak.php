<div class="bottom_header">
			<div class="container">
				
				<!-- ______LOGO_____ -->
				<div class="logo_holder float_left">
					<a href="<?php echo base_url(); ?>"><img src="<?php echo base_url('asset_front/images/asset_new/logo_ksu_new_1.png'); ?>" alt="LOGO"></a>
				</div> <!-- End of .logo_holder -->

				<!-- _______Contact info _______ -->
				<div class="contact_info float_right">
					
					<div class="info float_left border_fix space_fix1">
						<span class="ficon flaticon-house-1"></span>
						<h6>PT Karya Sehati Utama</h6>
						<p>Gedung Karya Sehati,<br> komplek Grand Cupuwatu,<br> Jl. Solo km.11,8 Kalasan<br> Yogyakarta</p>
					</div> <!-- End of .info -->

					<!-- <div class="info float_left border_fix space_fix2">
						
					</div>  -->

					<div class="info float_left">
						<!-- <span class="ficon flaticon-message3"></span> -->
						<h6>Email Sekarang </h6>
						<p><a href="mailto:karyasehatiutama@gmail.com" "email me" title="Kirim Email">karyasehatiutama@gmail.com</a> </p>

						<span class="ficon flaticon-phone"></span>
						<h6>Kontak Kantor & Hotline</h6>
						<a href="tel:0274496777" title="Hubungi Kami">0274-496-777</a> |
						<a target="_blank" href="https://api.whatsapp.com/send?phone=6282134444360" title="Hubungi Hotline">0821 3444 4360</a>
					</div> <!-- End of .info -->
					<div class="clear_fix"></div>
				</div> <!-- End of .contact_info -->
				<div class="clear_fix"></div>
			</div> <!-- End of .container -->
</div> <!-- End of .bottom_header -->