<!DOCTYPE html>
<html lang="en">
	<head>
		<?php require_once(APPPATH .'views/include/front/inc_style.php'); ?>
	</head>
	<body class="about-us">

		<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

	 	<!-- pre loader  -->

	 	<div id="loader-wrapper">
			<div id="loader"></div>
		</div>

		<!-- Scroll To Top -->
		<button class="scroll-top tran3s">
			<span class="hvr-icon-bob"></span>
		</button>


        <!-- Top Header ______________________________________-->

		<?php $this->load->view('frontpages/top_header'); ?>

        <!-- End Of Top Header _______________________________-->


        <!-- Bottom header  _________________________________ -->

		<?php $this->load->view('frontpages/header_kontak'); ?>


        <!-- End of Bottom header ___________________________ -->


        <!-- Main Menu ______________________________________ -->

        <div class="main_menu container">

			<?php $this->load->view('frontpages/main_menu'); ?>

		</div>  <!-- End of #main_menu -->


        <!-- End of Main Menu _______________________________ -->



		<!-- Inner Banner ________________________________ -->
		
		<section id="inner_banner">
			<div class="overlay">
				<div class="container">
					<div class="title">
						<h2>Profil <?php echo $data_tentang->nama_perusahaan ?></h2>
					</div> <!-- End .title -->
				</div> <!-- End .container -->
			</div> <!-- End .overlay -->
		</section> <!-- End .inner_banner -->

		<!-- End Inner Banner ____________________________ -->



		<!-- About Real Estate ___________________________ -->

		<div class="about_real_estate container">
			<div class="main-title">
				<h2>Tentang <span><?php echo $data_tentang->nama_perusahaan ?></span></h2>
			</div> <!-- End of .main-title -->


			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 pull-right">
					<div class="img_wrapper">
						<img src="<?php echo base_url('storage_img/img_tentang/'.$data_tentang->img_tentang); ?>" class="img-responsive" alt="Image">
					</div>
				</div>

				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 pull-left author_speech">
					<!-- <img src="<?php echo base_url('asset_front/images/about-us/1.png'); ?>" alt="author" class="img-responsive float_left"> -->
					<div class="text float_left">
					 	<h5><i><?php echo $data_tentang->title_salam ?></i></h5>
					 	<br>
					 	<p>
					 		<?php echo $data_tentang->deskripsi ?>
					 	</p>
					 	<!-- <p class="clear_fix" style="margin-bottom:21px;">It's now easier than ever to search for properties and get all the details you need to find that perfect new home or condominium in California and the surrounding areas, here you can know about that all facilities.</p> -->

						<!-- <img src="<?php echo base_url('asset_front/images/about-us/sign.png'); ?>" alt="sign" class="float_left">
						<div class="name float_left">
							<h6>Ngatijan ST.</h6>
							<span>Director Karya Sehati Utama</span>
						</div>  -->
						<!-- End of .name -->
					</div> <!-- End of .text -->
					<div class="clear_fix"></div>
				</div> <!-- End of .author_speech -->
			</div> <!-- End of .row -->
			
		</div> <!-- End of .about_real_estate -->


		<!-- End of About Real Estate ______________________ -->

		<div class="why_choose_us container">
			<div class="main-title">
				<!-- <h2>Kenapa <span>Memilih Kami?</span></h2> -->
			</div>
		    <div id="custom_carousel" class="carousel slide" data-ride="carousel" data-interval="4000">
		        
		        <div class="controls draggable ui-widget-content col-md-12 col-xs-12">
		            <ul class="nav ui-widget-header">
		            	<?php foreach ($data_divisi as $key => $dt_divisi): ?>
		            		<?php if ($key == 0): ?>
		            			<li data-target="#custom_carousel" data-slide-to="<?php echo $key ?>" class="active"><a href="#"><img src="<?php echo base_url('storage_img/img_divisi/'.$dt_divisi->Cover_Name); ?>"><small><?php echo $dt_divisi->Nama_Divisi ?></small></a></li>
		            		<?php else: ?>
		            			<li data-target="#custom_carousel" data-slide-to="<?php echo $key ?>" ><a href="#"><img src="<?php echo base_url('storage_img/img_divisi/'.$dt_divisi->Cover_Name); ?>"><small><?php echo $dt_divisi->Nama_Divisi ?></small></a></li>
		            		<?php endif ?>
		                <?php endforeach ?>
		            </ul>
		        </div>

		        <!-- Wrapper for slides -->

		        <div class="carousel-inner">
		            
		            <?php foreach ($data_divisi as $key => $dta_divisi): ?>
						
						<?php if ($key == 0): ?>
							<div class="item active">
						<?php else: ?>
							<div class="item">
						<?php endif ?>
		            
		                <div class="container-fluid">
		                    <div class="row">
		                        <div class="top col-md-2 col-xs-12"><img src="<?php echo base_url('storage_img/img_divisi/'.$dta_divisi->Cover_Name); ?>" class="img-responsive"></div>
		                        <div class="content col-md-10 col-xs-12">
		                            <h2><?php echo $dta_divisi->Nama_Divisi ?></h2>
		                            <p><?php echo $dta_divisi->Deskripsi_Divisi ?></p>
		                        </div>
		                    </div>
		                </div>            
		            </div> 

		            <?php endforeach ?>
		            
		        <!-- End Item -->
		        </div>
		        <!-- <a data-slide="prev" href="#custom_carousel" class="izq carousel-control">‹</a>
		        <a data-slide="next" href="#custom_carousel" class="der carousel-control">›</a> -->
		        <!-- End Carousel Inner -->
		        
		    </div>
		    <!-- End Carousel -->
		</div>

		<!-- Footer __________________________________________ -->
		<?php echo $this->load->view('frontpages/footer'); ?>

		<!-- End of Footer ______________________________________ -->


		<?php require_once(APPPATH .'views/include/front/inc_script.php'); ?>

	</body>
</html>