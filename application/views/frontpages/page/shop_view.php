<!DOCTYPE html>
<html lang="en">
	<head>
		<?php require_once(APPPATH .'views/include/front/inc_style.php'); ?>
	</head>
	<body class="gallery-page">

		<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

	 	<!-- pre loader  -->

	 	<div id="loader-wrapper">
			<div id="loader"></div>
		</div>

		<!-- Scroll To Top -->
		<button class="scroll-top tran3s">
			<span class="hvr-icon-bob"></span>
		</button>


        <!-- Top Header ______________________________________-->

		<?php $this->load->view('frontpages/top_header'); ?>

        <!-- End Of Top Header _______________________________-->


        <!-- Bottom header  _________________________________ -->

		<?php $this->load->view('frontpages/header_kontak'); ?>

        <!-- End of Bottom header ___________________________ -->


        <!-- Main Menu ______________________________________ -->

        <div class="main_menu container">

			<?php $this->load->view('frontpages/main_menu'); ?>

		</div>  <!-- End of #main_menu -->


        <!-- End of Main Menu _______________________________ -->



		<!-- Inner Banner ________________________________ -->
		
		<section id="inner_banner">
			<div class="overlay">
				<div class="container">
					<div class="title">
						<h2>SHOP PROPERTI</h2>
						<!-- <ul>
							<li><a href="index.html">Home</a></li>
							<li><i class="fa fa-angle-double-right"></i></li>
							<li><a href="gallery-3columns-with-text.html">Gallery</a></li>
							<li><i class="fa fa-angle-double-right"></i></li>
							<li>4 Columns With Text</li>
						</ul> -->
					</div> <!-- End .title -->
				</div> <!-- End .container -->
			</div> <!-- End .overlay -->
		</section> <!-- End .inner_banner -->

		<!-- End Inner Banner ____________________________ -->



		<!-- Gallery page content _______________________ -->
		<?php $this->load->model('Lib_model'); $m_lib = new Lib_model(); ?>
		<div class="gallery_wrapper">
			<div class="container menu_wrapper">
				<ul class="gallery_menu">
					<li class="filter active tran3s" data-filter="all">Semua Produk</li>
					<?php foreach ($group_name as $gr_name): ?>
						<li class="filter tran3s" data-filter="<?php echo ".".str_replace(' ','_', $gr_name->Nama_Produk); ?>"><?php echo $gr_name->Nama_Produk ?></li>
					<?php endforeach ?>
					<li class="filter tran3s" data-filter=".1_lantai">1 Lantai</li>
					<li class="filter tran3s" data-filter=".2_lantai">2 Lantai</li>
				</ul>
			</div> <!-- End .menu_wrapper -->

			<div class="gallery_item_container gallery_style_nine gallery_text container" id="mixitup_list">

				<div class="row">
					<?php foreach ($data_property as $dt_property): ?>
						
					<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 mix <?php echo str_replace(' ','_', $dt_property->Nama_Produk); ?> <?php echo $dt_property->Kategori ?>">
						<div class="single_item">
							<div class="img_holder">
								<img src="<?php echo base_url('storage_img/img_cover_property/'.$dt_property->Cover_Name); ?>" alt="<?php echo $dt_property->Nama_Property ?>" class="img-responsive">
								<div class="overlay tran3s">
									<div class="link">
										<a href="<?php echo base_url('storage_img/img_cover_property/'.$dt_property->Cover_Name); ?>" class="fancybox"><i class="fa fa-search-plus"></i></a>
										<a href="<?php echo base_url('page/property/'.$dt_property->Slug_Property); ?>"><i class="fa fa-link"></i></a>
									</div>
								</div> <!-- End .overlay -->
							</div> <!-- End .img_holder -->
							<div class="title">
								<a href="<?php echo base_url('page/property/'.$dt_property->Slug_Property); ?>">
									<h6><?php echo $dt_property->Nama_Property ?></h6>
									<i><?php echo $dt_property->Nama_Produk ?> <br> <?php echo $dt_property->Luas_tanah ?></i>
								</a>
							</div> <!-- End .title -->
						</div> <!-- End .single_item -->
					</div>

					<?php endforeach ?>

				</div>
			</div> <!-- End #mixitup_list -->

			<!-- <div class="container page_indicator">
				<a href="gallery-4columns-with-text.html#" class="prev tran3s">Prev Page</a>
				<ul>
					<li><a href="gallery-4columns-with-text.html#"><i class="fa fa-angle-left"></i></a></li>
					<li class="active"><a href="gallery-4columns-with-text.html#">1</a></li>
					<li><a href="gallery-4columns-with-text.html#">2</a></li>
					<li><a href="gallery-4columns-with-text.html#">3</a></li>
					<li><a href="gallery-4columns-with-text.html#"><i class="fa fa-angle-right"></i></a></li>
				</ul>
				<a href="gallery-4columns-with-text.html#" class="next tran3s">Next Page</a>
			</div> -->
		</div> <!-- End .gallery_wrapper -->


		<!-- End Gallery page content ___________________ -->


		<!-- Footer __________________________________________ -->
		
		<?php $this->load->view('frontpages/footer.php'); ?>

		<!-- End of Footer ______________________________________ -->

		<?php require_once(APPPATH .'views/include/front/inc_script.php'); ?>

	</body>
</html>