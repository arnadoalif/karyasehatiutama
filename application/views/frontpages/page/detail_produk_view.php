<!DOCTYPE html>
<html lang="en">
	<head>
		<?php require_once(APPPATH .'views/include/front/inc_style.php'); ?>
		<style type="text/css">
			.img-cover {
				margin-left: 100px;
			    position: relative;
			    padding-top: 60px;
			    padding-bottom: 30px;
			    width: 75%;
			}
		</style>
	</head>
	<body class="gallery-page">

		<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

	 	<!-- pre loader  -->

	 	<div id="loader-wrapper">
			<div id="loader"></div>
		</div>

		<!-- Scroll To Top -->
		<button class="scroll-top tran3s">
			<span class="hvr-icon-bob"></span>
		</button>


       <!-- Top Header ______________________________________-->

		<?php $this->load->view('frontpages/top_header'); ?>

        <!-- End Of Top Header _______________________________-->


        <!-- Bottom header  _________________________________ -->

		<?php $this->load->view('frontpages/header_kontak'); ?>

        <!-- End of Bottom header ___________________________ -->


        <!-- Main Menu ______________________________________ -->

        <div class="main_menu container">

			<?php $this->load->view('frontpages/main_menu'); ?>

		</div>  <!-- End of #main_menu -->



		<!-- Inner Banner ________________________________ -->
		
		<section id="inner_banner">
			<div class="overlay">
				<div class="container">
					<div class="title">
						<h2><?php echo $data_produk->Nama_Produk ?></h2>
					</div> <!-- End .title -->
				</div> <!-- End .container -->
			</div> <!-- End .overlay -->
		</section> <!-- End .inner_banner -->

		<!-- End Inner Banner ____________________________ -->



		<!-- Gallery page content _______________________ -->

		<section class="single_gallery_details container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 gallery_img_holder">
					
					<?php if (count($data_berkas_slide) > 0): ?>
						
					<div class="img_wrapper">
						<div id="myCarousel" class="carousel slide" data-ride="carousel">
						  <div class="carousel-inner" role="listbox">
						    
						    <?php foreach ($data_berkas_slide as $key => $dt_berkas_slide): ?>
						    	<?php if ($key == 0): ?>
						    		<div class="item active">
								<?php else: ?>
									<div class="item">
						    	<?php endif ?>
								    	<a href="<?php echo base_url('storage_img/img_slide_produk/'.$dt_berkas_slide->Nama_img); ?>" class="fancybox"><img src="<?php echo base_url('storage_img/img_slide_produk/'.$dt_berkas_slide->Nama_img); ?>" alt="images" class="img-responsive"></a>
									</div>
						    <?php endforeach ?>

						  <a class="left carousel-control tran3s" href="gallery-single.html#myCarousel" role="button" data-slide="prev">
						    <i class="fa fa-angle-left" aria-hidden="true"></i>
						    <span class="sr-only">Previous</span>
						  </a>
						  <a class="right carousel-control tran3s" href="gallery-single.html#myCarousel" role="button" data-slide="next">
						   <i class="fa fa-angle-right" aria-hidden="true"></i>
						    <span class="sr-only">Next</span>
						  </a>
						</div>
					</div>

					<?php endif ?>
				</div>
			</div>

			<div class="post_wrapper container">
				
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<div class="blog_post_meta">
						
						<div class="post">
							<img src="<?php echo base_url('storage_img/img_cover_produk/'.$data_produk->Cover_Produk); ?>" class="img-responsive img-cover" alt="Image">
						</div> <!-- End .post -->
					</div> <!-- End .blog_post_meta -->
				</div>

				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<div class="blog_post_meta">
						<div class="post">
							<!-- inner_title__________ -->
							<div class="post">
								<img style="margin: 0 auto;" width="50%" src="<?php echo base_url('storage_img/img_logo_produk/'.$data_produk->Logo_Produk); ?>" class="img-responsive" alt="<?php echo $data_produk->Nama_Produk ?>">
							</div> <!-- End .post -->

							<div class="inner_title" style="top: 14px; text-align: center;">
								<h5>Sekilas Latar Belakang Tentang <?php echo $data_produk->Nama_Produk ?></h5>
							</div>
							<!-- End inner_title______ -->
							<p>
								<?php echo $data_produk->Deskripsi_2 ?>
							</p>
						</div> <!-- End .post -->
					</div> <!-- End .blog_post_meta -->
				</div>
			</div> <!-- End .post_wrapper -->

			<div class="related_project">
				<div class="main-title2">
					<h2>Tipe Rumah <?php echo $data_produk->Nama_Produk ?></h2>
				</div>

				<?php if ($total_property <= 3): ?>
					<div class="row">
						
						<?php foreach ($data_property as $dt_property): ?>
							<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 mix">
								<div class="single_item">
									<div class="img_holder">
										<img src="<?php echo base_url('storage_img/img_cover_property/'.$dt_property->Cover_Name); ?>" class="img-responsive" alt="<?php echo $dt_property->Nama_Property ?>" alt="<?php echo $dt_property->Nama_Property ?>" class="img-responsive">
										<div class="overlay tran3s">
											<div class="link">
												<a href="<?php echo base_url('storage_img/img_cover_property/'.$dt_property->Cover_Name); ?>" class="img-responsive" alt="<?php echo $dt_property->Nama_Property ?>" class="fancybox"><i class="fa fa-search-plus"></i></a>
												<a href="<?php echo base_url('page/property/'.$dt_property->Slug_Property); ?>"><i class="fa fa-link"></i></a>
											</div>
										</div>
									</div>
									<div class="title">
										<a href="<?php echo base_url('page/property/'.$dt_property->Slug_Property); ?>">
											<h5><?php echo $dt_property->Nama_Property ?></h5>
											<i><?php echo $dt_property->Luas_tanah ?></i>
										</a>
									</div> <!-- End .title -->
								</div> <!-- End .single_item -->
							</div>
						<?php endforeach ?>

					</div>
				<?php else: ?>
					
					<div class="gallery_wrapper">
						<div class="container menu_wrapper">
							<ul class="gallery_menu">
								<li class="filter active tran3s" data-filter="all">Show All</li>
								<li class="filter tran3s" data-filter=".2_lantai">2 Lantai</li>
								<li class="filter tran3s" data-filter=".1_lantai">1 Lantai</li>
							</ul>
						</div> <!-- End .menu_wrapper -->

						<div class="gallery_item_container gallery_style_eight gallery_text container" id="mixitup_list">	
							<div class="row">
								<?php foreach ($data_property as $dta_property): ?>
								<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 mix <?php echo $dta_property->Kategori ?>">
									<div class="single_item">
										<div class="img_holder">
											<img src="<?php echo base_url('storage_img/img_cover_property/'.$dta_property->Cover_Name); ?>" alt="" class="img-responsive">
											<div class="overlay tran3s">
												<div class="link">
													<a href="<?php echo base_url('storage_img/img_cover_property/'.$dta_property->Cover_Name); ?>" class="fancybox"><i class="fa fa-search-plus"></i></a>
													<a href="<?php echo base_url('page/property/'.$dta_property->Slug_Property); ?>"><i class="fa fa-link"></i></a>
												</div>
											</div> <!-- End .overlay -->
										</div> <!-- End .img_holder -->
										<div class="title">
											<a href="">
												<h5><?php echo $dta_property->Nama_Property ?></h5>
												<i><?php echo $dta_property->Luas_tanah ?></i>
											</a>
										</div> <!-- End .title -->
									</div> <!-- End .single_item -->
								</div>
								<?php endforeach ?>
							</div>
						</div> <!-- End #mixitup_list -->

					</div> <!-- End .gallery_wrapper -->

				<?php endif ?>

			</div>

			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<div class="post">
						<!-- inner_title__________ -->
						<div class="inner_title">
							<h5>Spesifikasi</h5>
						</div>
						<!-- End inner_title______ -->
						<p><?php echo $data_produk->Grid_1 ?></p>
					</div> <!-- End .post -->
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<div class="post">
						<!-- inner_title__________ -->
						<div class="inner_title">
							<!-- <h5></h5> -->
						</div>
						<!-- End inner_title______ -->
						<p>
							<?php echo $data_produk->Grid_2 ?>
					    </p>
					</div> <!-- End .post -->
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<div class="post">
						<!-- inner_title__________ -->
						<div class="inner_title">
							
						</div>
						<!-- End inner_title______ -->
						<p>
							<?php echo $data_produk->Grid_3 ?>
						</p>

					</div> <!-- End .post -->
				</div>
			</div>

			<div class="related_project">
				<div class="main-title2">
					<h2><?php echo $data_produk->Nama_Produk ?></h2>
				</div>

				<div class="slider_wrapper">
					<div class="related_project_slider">

						<?php foreach ($data_fasilitas as $dt_fasilitas): ?>

							<?php if (empty($dt_fasilitas->Link_Url)): ?>
								
								<!-- Single Project -->
								<div class="item">
									<div class="single_item">
										<div class="img_holder">
											<img src="<?php echo base_url('storage_img/img_fasilitas/'.$dt_fasilitas->Nama_img); ?>" alt="<?php echo $dt_fasilitas->Title ?>" class="img-responsive">
											<div class="overlay tran3s">
												<div class="link">
													<a href="<?php echo base_url('storage_img/img_fasilitas/'.$dt_fasilitas->Nama_img); ?>" class="fancybox"><i class="fa fa-search-plus"></i></a>
													<!-- <a href=""><i class="fa fa-link"></i></a> -->
												</div>
											</div> <!-- End .overlay -->
										</div> <!-- End .img_holder -->
										<div class="title">
											<a href="">
												<h6><?php echo $dt_fasilitas->Title ?></h6>
												<i><?php echo $dt_fasilitas->Body_text ?></i>
											</a>
										</div> <!-- End .title -->
									</div> <!-- End .single_item -->
								</div> <!-- End of .item -->

							<?php else: ?>

								<!-- Single Project -->
								<div class="item">
									<div class="single_item">
										<div class="img_holder">
											<iframe class="media-video" width="100%" src="https://www.youtube.com/embed/<?php echo $dt_fasilitas->Link_Url ?>" frameborder="0" allowfullscreen style="height: 200px;"></iframe>
											<div class="overlay tran3s">
												<div class="link">
													<a href="https://www.youtube.com/embed/<?php echo $dt_fasilitas->Link_Url ?>" class="fancybox fancy_video fancybox.iframe"><i class="fa fa-play"></i></a>
												</div>
											</div> <!-- End .overlay -->
										</div> <!-- End .img_holder -->
										<div class="title">
											<a href="">
												<h6><?php echo $dt_fasilitas->Title ?></h6>
												<i><?php echo $dt_fasilitas->Body_text ?></i>
											</a>
										</div> <!-- End .title -->
									</div> <!-- End .single_item -->
								</div> <!-- End of .item -->
								
							<?php endif ?>
							
						

						<?php endforeach ?>

					</div> <!-- End of .related_project_slider -->
				</div> <!-- End of .slider_wrapper -->
			</div>

			<div class="related_project">
				<div class="main-title2">
					<h2>Gallery Proyek <?php echo $data_produk->Nama_Produk ?></h2>
				</div>

				<div class="slider_wrapper">
					<div class="related_project_slider">

						<?php foreach ($data_gallery_proyek as $dt_g_proyek): ?>

							<!-- Single Project -->
							<div class="item">
								<div class="single_item">
									<div class="img_holder">
										<img src="<?php echo base_url('storage_img/img_galleri_proyek/'.$dt_g_proyek->Nama_img); ?>" alt="<?php echo $dt_g_proyek->Title ?>" class="img-responsive">
										<div class="overlay tran3s">
											<div class="link">
												<a href="<?php echo base_url('storage_img/img_galleri_proyek/'.$dt_g_proyek->Nama_img); ?>" class="fancybox"><i class="fa fa-search-plus"></i></a>
												<!-- <a href=""><i class="fa fa-link"></i></a> -->
											</div>
										</div> <!-- End .overlay -->
									</div> <!-- End .img_holder -->
									<div class="title">
										<a href="">
											<h6><?php echo $dt_g_proyek->Title ?></h6>
											<i><?php echo $data_produk->Nama_Produk ?></i>
										</a>
									</div> <!-- End .title -->
								</div> <!-- End .single_item -->
							</div> <!-- End of .item -->

						<?php endforeach ?>

					</div> <!-- End of .related_project_slider -->
				</div> <!-- End of .slider_wrapper -->
			</div>
		</section> <!-- End of .single_gallery_details -->

		<!-- End Gallery page content ___________________ -->




		<!-- Footer __________________________________________ -->
		
		<?php $this->load->view('frontpages/footer'); ?>

		<!-- End of Footer ______________________________________ -->

		<?php require_once(APPPATH .'views/include/front/inc_script.php'); ?>

	</body>
</html>