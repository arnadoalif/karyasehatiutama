<!DOCTYPE html>
<html lang="en">
	<head>
		<?php require_once(APPPATH .'views/include/front/inc_style.php'); ?>
	</head>
	<body class="home">

		<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

	 	<!-- pre loader  -->

	 	<!-- <div id="loader-wrapper">
			<div id="loader"></div>
		</div> -->

		<!-- Scroll To Top -->
		<button class="scroll-top tran3s">
			<span class="hvr-icon-bob"></span>
		</button>


        <!-- Top Header ______________________________________-->

		<?php $this->load->view('frontpages/top_header'); ?>

        <!-- End Of Top Header _______________________________-->


        <!-- Bottom header  _________________________________ -->

		<?php $this->load->view('frontpages/header_kontak'); ?>

        <!-- End of Bottom header ___________________________ -->


        <!-- Main Menu ______________________________________ -->

        <div class="main_menu container">

			<?php $this->load->view('frontpages/main_menu'); ?>

		</div>  <!-- End of #main_menu -->


        <!-- End of Main Menu _______________________________ -->

		<!-- Banner _________________________________________ -->

		<section class="banner">
			<div id="main_slider">
				<div class="container">
					<div class="clear_fix"></div>
				</div>
				<!-- <button id="next"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
				<button id="previous"><i class="fa fa-angle-left" aria-hidden="true"></i></button> -->
			</div> <!-- End of #main_slider -->

		</section> 

		<!-- End of Banner ___________________________________ -->


		<!-- Property Details Tab ____________________________ -->
		
		
		<?php foreach ($data_produk as $key => $dt_produk): ?>

			<section class="know_agent container">
				<div class="main-title">
					<h2>Produk <span><?php echo $dt_produk->Nama_Produk ?></span></h2>
				</div> <!-- End of .main-title -->

				<div class="row agent_info_wrapper">
					
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<div class="img_holder">
							<a target="_blank" href="<?php echo base_url('page/detail/'.$dt_produk->Slug_name); ?>" title="<?php echo $dt_produk->Nama_Produk ?>">
							<img src="<?php echo base_url('storage_img/img_cover_produk/'.$dt_produk->Cover_Produk); ?>" alt="<?php echo $dt_produk->Nama_Produk ?>" class="img-responsive" style="left: 100px; position: relative; ">
							</a>
						</div> <!-- End of .img_holder -->

					</div> <!-- End of .col -->

					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 agent_summary">
						<div class="name">
							<!-- <h4>Grand Tlogoadi</h4>
							<i>Real Estate Agent</i> -->
							<img style="margin: 0 auto;" width="50%" src="<?php echo base_url('storage_img/img_logo_produk/'.$dt_produk->Logo_Produk); ?>" class="img-responsive" alt="<?php echo $dt_produk->Nama_Produk ?>">
						</div> <!-- End of .name -->
			
						<p style="margin-top: 30px; position: relative;"><?php echo $dt_produk->Deskripsi_1 ?></p>
					</div> <!-- End of .agent_summary -->
				</div> <!-- End of .agent_info_wrapper -->
			</section> <!-- End of .know_agent -->

		<?php endforeach ?>

		<!-- End of Property Details Tab ______________________ -->

		<!-- End of Home Gallery ______________________________ -->

		<div class="related_project container">
			<div class="main-title2">
				<h2>SHOP <sub>Properti Yang Tersedia</sub></h2>
			</div>

			<div class="slider_wrapper">
				<div class="related_project_slider">

					<?php foreach ($data_property as $dt_property): ?>
						
					<!-- Single Project -->
					<div class="item">
						<div class="single_item">
							<div class="img_holder">
								<img src="<?php echo base_url('storage_img/img_cover_property/'.$dt_property->Cover_Name); ?>" alt="<?php echo $dt_property->Nama_Produk ?>" class="img-responsive">
								<div class="overlay tran3s">
									<div class="link">
										<a href="<?php echo base_url('storage_img/img_cover_property/'.$dt_property->Cover_Name); ?>" class="fancybox"><i class="fa fa-search-plus"></i></a>
										<a href="<?php echo base_url('page/property/'.$dt_property->Slug_Property); ?>"><i class="fa fa-link"></i></a>
									</div>
								</div> <!-- End .overlay -->
							</div> <!-- End .img_holder -->
							<div class="title">
								<a href="<?php echo base_url('page/property/'.$dt_property->Slug_Property); ?>">
									<h6><?php echo $dt_property->Nama_Property ?></h6>
									<i><?php echo $dt_property->Nama_Produk ?> <br> <?php echo $dt_property->Luas_tanah ?></i>
								</a>
							</div> <!-- End .title -->
						</div> <!-- End .single_item -->
					</div> <!-- End of .item -->

					<?php endforeach ?>
					
				</div> <!-- End of .related_project_slider -->
			</div> <!-- End of .slider_wrapper -->
		</div>



		<!-- Testimonial Two _______________________________ -->
		
		<div class="testimonial_two">
			<div class="container">
				<div class="main-title">
					<h2>Testimoni Customer</h2>
				</div> <!-- End of .main-title -->
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

			            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
			              <!-- Wrapper for slides -->
			              <div class="carousel-inner">
							
							<?php foreach ($data_testimoni as $key => $dt_testimoni): ?>
			                	
		                		<?php if ($key == 0): ?>
		                			<div class="item active">
		                		<?php else: ?>
		                			<div class="item">
		                		<?php endif ?>
			                	
				                  	<div class="row" style="padding: 20px">
					                    <button style="border: none;"><i class="fa fa-quote-left testimonial_fa" aria-hidden="true"></i></button>
					                    <p class="testimonial_para"><?php echo $dt_testimoni->Isi_Testimoni ?></p><br>
					                    <div class="row">
					                    <div class="col-sm-2">
					                        <img alt="client <?php echo $dt_testimoni->Nama ?>" src="<?php echo base_url('storage_img/img_testimoni/'.$dt_testimoni->Avatar); ?>" class="img-responsive" style="width: 100px; margin: 0 auto;">
					                        </div>
					                        <div class="col-sm-10">
					                        <h4><strong><?php echo $dt_testimoni->Nama ?></strong></h4>
					                        <!-- <p class="testimonial_subtitle"><span>Chlinical Chemistry Technologist</span><br> -->
					                        <span><?php echo $dt_testimoni->Alamat ?></span>
					                        </p>
					                    </div>
					                    </div>
					                  </div>
			                	</div>
			                <?php endforeach ?>

			              </div>
			            </div>
			            <div class="controls testimonial_control pull-right">
			                <a class="left fa fa-chevron-left btn btn-default testimonial_btn" href="#carousel-example-generic"
			                  data-slide="prev"></a>

			                <a class="right fa fa-chevron-right btn btn-default testimonial_btn" href="#carousel-example-generic"
			                  data-slide="next"></a>
			              </div>
			        </div>

				</div>
			</div> <!-- End .container -->
		</div> <!-- End .testimonial -->

		<!-- End Testimonial Two____________________________ -->



		<!-- Footer __________________________________________ -->
		
		<?php $this->load->view('frontpages/footer.php'); ?>

		<!-- End of Footer ______________________________________ -->

		<?php require_once(APPPATH .'views/include/front/inc_script.php'); ?>

	</body>
</html>