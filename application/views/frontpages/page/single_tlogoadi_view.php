<!DOCTYPE html>
<html lang="en">
	<head>
		<?php require_once(APPPATH .'views/include/front/inc_style.php'); ?>
	</head>
	<body class="gallery-page">

		<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

	 	<!-- pre loader  -->

	 	<div id="loader-wrapper">
			<div id="loader"></div>
		</div>

		<!-- Scroll To Top -->
		<button class="scroll-top tran3s">
			<span class="hvr-icon-bob"></span>
		</button>


       <!-- Top Header ______________________________________-->

		<?php $this->load->view('frontpages/top_header'); ?>

        <!-- End Of Top Header _______________________________-->


        <!-- Bottom header  _________________________________ -->

		<?php $this->load->view('frontpages/header_kontak'); ?>

        <!-- End of Bottom header ___________________________ -->


        <!-- Main Menu ______________________________________ -->

        <div class="main_menu container">

			<?php $this->load->view('frontpages/main_menu'); ?>

		</div>  <!-- End of #main_menu -->



		<!-- Inner Banner ________________________________ -->
		
		<section id="inner_banner">
			<div class="overlay">
				<div class="container">
					<div class="title">
						<h2>Grand Tlogoadi</h2>
						<!-- <ul>
							<li><a href="index.html">Home</a></li>
							<li><i class="fa fa-angle-double-right"></i></li>
							<li><a href="gallery-3columns-with-text.html">Gallery</a></li>
							<li><i class="fa fa-angle-double-right"></i></li>
							<li>Loyal Beach house</li>
						</ul> -->
					</div> <!-- End .title -->
				</div> <!-- End .container -->
			</div> <!-- End .overlay -->
		</section> <!-- End .inner_banner -->

		<!-- End Inner Banner ____________________________ -->



		<!-- Gallery page content _______________________ -->

		<section class="single_gallery_details container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 gallery_img_holder">
					<div class="img_wrapper">
						<div id="myCarousel" class="carousel slide" data-ride="carousel">
						  <!-- Wrapper for slides -->
						  <div class="carousel-inner" role="listbox">
						    <div class="item active">
						      <a href="<?php echo base_url('asset_front/images/asset_new/slide_tlogoadi.png'); ?>" class="fancybox"><img src="<?php echo base_url('asset_front/images/asset_new/slide_tlogoadi.png'); ?>" alt="images" class="img-responsive"></a>
						    </div>

						    <div class="item">
						      <a href="<?php echo base_url('asset_front/images/asset_new/slide_tlogoadi.png'); ?>" class="fancybox"><img src="<?php echo base_url('asset_front/images/asset_new/slide_tlogoadi.png'); ?>" alt="images" class="img-responsive"></a>
						    </div>

						    <div class="item">
						     <a href="<?php echo base_url('asset_front/images/asset_new/slide_tlogoadi.png'); ?>" class="fancybox"><img src="<?php echo base_url('asset_front/images/asset_new/slide_tlogoadi.png'); ?>" alt="images" class="img-responsive"></a>
						    </div>

						  </div>

						  <!-- Left and right controls -->
						  <a class="left carousel-control tran3s" href="gallery-single.html#myCarousel" role="button" data-slide="prev">
						    <i class="fa fa-angle-left" aria-hidden="true"></i>
						    <span class="sr-only">Previous</span>
						  </a>
						  <a class="right carousel-control tran3s" href="gallery-single.html#myCarousel" role="button" data-slide="next">
						   <i class="fa fa-angle-right" aria-hidden="true"></i>
						    <span class="sr-only">Next</span>
						  </a>
						</div>
					</div> <!-- End of .img_wrapper -->
				</div> <!-- End of .gallery_img_holder -->
			</div> <!-- End of .row -->

			<div class="post_wrapper container">
				
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<div class="blog_post_meta">
						<div class="post">
							<img src="<?php echo base_url('asset_front/images/asset_new/log_tlogoadi.png'); ?>" class="img-responsive" alt="Image">
						</div> <!-- End .post -->
					</div> <!-- End .blog_post_meta -->
				</div>

				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<div class="blog_post_meta">
						<div class="post">
							<!-- inner_title__________ -->
							<div class="inner_title">
								<h5>Sekilas Latar Belakang Tentang Grand Tlogoadi</h5>
							</div>
							<!-- End inner_title______ -->
							<p>
								Setelah sukses dengan Grand Tlogoadi, dan didukung oleh tingginya permintaan akan keterbatasan unit rumah di kawasan tersebut, sebagai developer pengembangan PT. Karya Sehati Utama membuka pengembangan kawasan, khususnya Romantic CLuster (Rumah 2 Lantai) dengan penambahan fasilitas umum berupa Pendopo Balai Warga.
							</p>
							<p>
								Grand Tlogoadi bedara di daerah yang strategis & bernilai investasi tinggi, kani tepi jalan utama Jl. Kebon agung, Tlogoadi, Mlati, SLeman yang merupakan daerah pengembangan kota Sleman. sebuah lokasi yang berdekatan dengan Pusat Pemerintahaan Kota Sleman, Fly Over Jombor, RS. UGM, Jogja City Mall, Terminal Jombor, Restoran, Hotel berbintang, Kampus ternama, dsb.
							</p>
							<p>
								Grand Tlogoadi dibagun dengan menerapkan konsep <strong>HARMONY</strong> (Harmonisasi), <strong>ROMANTIC</strong> (Romantisme), dan <strong>LOVE</strong> (Cinta Kasih) dengan filosofi Hasta Brata yang merupakan simbol alam semesta. Hasta berarti delapan, Brata berati lau atau watak, yang pada hakekatnya menyiratkan keharmonisasian alam semesta.
							</p>
						</div> <!-- End .post -->
					</div> <!-- End .blog_post_meta -->
				</div>
			</div> <!-- End .post_wrapper -->

			<div class="related_project">
				<div class="main-title2">
					<h2>Tipe Rumah Grand Tlogoadi</h2>
				</div>

				<div class="slider_wrapper">
					<img src="<?php echo base_url('asset_front/images/asset_new/img_tlogoadi_100.png'); ?>" class="img-responsive" alt="Image">
					<img src="<?php echo base_url('asset_front/images/asset_new/img_tlogoadi_90.png'); ?>" class="img-responsive" alt="Image">
				</div> <!-- End of .slider_wrapper -->
			</div>

			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<div class="post">
						<!-- inner_title__________ -->
						<div class="inner_title">
							<h5>Spesifikasi</h5>
						</div>
						<!-- End inner_title______ -->
						<p><strong>Pondasi : </strong> Pasang Batu Kali</p>
						<p><strong>Dinding : </strong> Pasang bata merah, dispenser aci, finishing cat</p>
						<p><strong>Lantai : </strong>Granit 60x60 Ex. Indogres</p>
						<p><strong>Atap : </strong>Rangkap atap Baja ringan, Penutup atap genteng beton + finishing cat</p>
					</div> <!-- End .post -->
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<div class="post">
						<!-- inner_title__________ -->
						<div class="inner_title">
							<!-- <h5></h5> -->
						</div>
						<!-- End inner_title______ -->
						<p><strong>Kusen, Pintu & Jendela :</strong></p>
						<p>
							<ul>
							    <li>- Kusen kayu jati lokal</li>
							    <li>- Daun pintu : panil jati lokal</li>
							    <li>- Daun jendela ram kayu jati lokal
	                                <ul>
	                                    <li>+ kacabening 5mm</li>
	                                </ul>
							    </li>
							    <li>Finishing melamine</li>
							</ul>
						</p>
						<p><strong>Plafond : </strong></p>
						<p>
							<ul>
						        <li>- Rangka Hollow Galvanis 2/4</li>
						        <li>- Gypsum Mini Jaya Board/Elephant
                                    <ul>
                                        <li>+ cornice</li>
                                    </ul>
						        </li>
						    </ul>
					    </p>
					</div> <!-- End .post -->
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<div class="post">
						<!-- inner_title__________ -->
						<div class="inner_title">
							
						</div>
						<!-- End inner_title______ -->
						<p><strong>Kamar Mandi :</strong></p>
						<ul>
						    <li>- Keramik lantai 25x25 ex. Milan / setara</li>
						    <li>- Keramik dinding 25x25 ex. Roman / setara</li>
						    <li>List keramik dinding Listello 3,5x25</li>
						</ul>
						<p><strong>Sanitair : </strong>Closet duduk ex. Toto</p>
						<p><strong>Airbersih : </strong></p>
						<ul>
						    <li>Sumur suntik</li>
						    <li>Pompa air & tandon 500 lt</li>
						</ul>
						<p><strong>Elektrikal : </strong></p>
						<ul>
						    <li>- Daya listrik PLN 2200 Watt</li>
						    <li>- Stop kontak & saklar ex. Broco</li>
						</ul>

					</div> <!-- End .post -->
				</div>
			</div>

			<div class="related_project">
				<div class="main-title2">
					<h2>Fasilitas Lainnya</h2>
				</div>

				<div class="slider_wrapper">
					<div class="related_project_slider">

						<!-- Single Project -->
						<div class="item">
							<div class="single_item">
								<div class="img_holder">
									<img src="<?php echo base_url('asset_front/images/asset_new/site_plan_gta.png'); ?>" alt="images" class="img-responsive">
									<div class="overlay tran3s">
										<div class="link">
											<a href="<?php echo base_url('asset_front/images/asset_new/site_plan_gta.png'); ?>" class="fancybox"><i class="fa fa-search-plus"></i></a>
											<!-- <a href=""><i class="fa fa-link"></i></a> -->
										</div>
									</div> <!-- End .overlay -->
								</div> <!-- End .img_holder -->
								<div class="title">
									<a href="">
										<h6>Site Plan</h6>
										<i>Grand Tlogoadi</i>
									</a>
								</div> <!-- End .title -->
							</div> <!-- End .single_item -->
						</div> <!-- End of .item -->

						<!-- Single Project -->
						<div class="item">
							<div class="single_item">
								<div class="img_holder">
									<img src="<?php echo base_url('asset_front/images/asset_new/fasilitas_umum_gta.png'); ?>" alt="images" class="img-responsive">
									<div class="overlay tran3s">
										<div class="link">
											<a href="<?php echo base_url('asset_front/images/asset_new/fasilitas_umum_gta.png'); ?>" class="fancybox"><i class="fa fa-search-plus"></i></a>
											<!-- <a href=""><i class="fa fa-link"></i></a> -->
										</div>
									</div> <!-- End .overlay -->
								</div> <!-- End .img_holder -->
								<div class="title">
									<a href="">
										<h6>Fasilitas Umum</h6>
										<i>Kolam Renang</i>
									</a>
								</div> <!-- End .title -->
							</div> <!-- End .single_item -->
						</div> <!-- End of .item -->

						<!-- Single Project -->
						<div class="item">
							<div class="single_item">
								<div class="img_holder">
									<img src="<?php echo base_url('asset_front/images/asset_new/denah_lokasi_gta.png'); ?>" alt="images" class="img-responsive">
									<div class="overlay tran3s">
										<div class="link">
											<a href="<?php echo base_url('asset_front/images/asset_new/denah_lokasi_gta.png'); ?>" class="fancybox"><i class="fa fa-search-plus"></i></a>
											<!-- <a href=""><i class="fa fa-link"></i></a> -->
										</div>
									</div> <!-- End .overlay -->
								</div> <!-- End .img_holder -->
								<div class="title">
									<a href="">
										<h6>Denah Lokasi</h6>
										<i>Grand Tlogoadi</i>
									</a>
								</div> <!-- End .title -->
							</div> <!-- End .single_item -->
						</div> <!-- End of .item -->

					</div> <!-- End of .related_project_slider -->
				</div> <!-- End of .slider_wrapper -->
			</div>
		</section> <!-- End of .single_gallery_details -->

		<!-- End Gallery page content ___________________ -->




		<!-- Footer __________________________________________ -->
		
		<?php $this->load->view('frontpages/footer'); ?>

		<!-- End of Footer ______________________________________ -->

		<?php require_once(APPPATH .'views/include/front/inc_script.php'); ?>

	</body>
</html>