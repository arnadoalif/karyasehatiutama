<!DOCTYPE html>
<html lang="en">
	<head>
		<?php require_once(APPPATH .'views/include/front/inc_style.php'); ?>

	</head>
	<body class="contact-us">

		<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

	 	<!-- pre loader  -->

	 	<div id="loader-wrapper">
			<div id="loader"></div>
		</div>

		<!-- Scroll To Top -->
		<button class="scroll-top tran3s">
			<span class="hvr-icon-bob"></span>
		</button>

 		<!-- Top Header ______________________________________-->

		<?php $this->load->view('frontpages/top_header'); ?>

        <!-- End Of Top Header _______________________________-->


        <!-- Bottom header  _________________________________ -->

		<?php $this->load->view('frontpages/header_kontak'); ?>

        <!-- End of Bottom header ___________________________ -->


        <!-- Main Menu ______________________________________ -->

        <div class="main_menu container">

			<?php $this->load->view('frontpages/main_menu'); ?>

		</div>  <!-- End of #main_menu -->

        <!-- End of Main Menu _______________________________ -->



		<!-- Inner Banner ________________________________ -->
		
		<section id="inner_banner">
			<div class="overlay">
				<div class="container">
					<div class="title">
						<h2>Kontak Perusahaan</h2>
						<!-- <ul>
							<li><a href="index.html">Home</a></li>
							<li><i class="fa fa-angle-double-right"></i></li>
							<li>Contact</li>
						</ul> -->
					</div> <!-- End .title -->
				</div> <!-- End .container -->
			</div> <!-- End .overlay -->
		</section> <!-- End .inner_banner -->

		<!-- End Inner Banner ____________________________ -->


		<!-- Contact Us Map ______________________________ -->

		<div class="container contact_up_map_wrapper">
			<div class="inner_title">
				<h2>Lokasi Kantor</h2>
			</div>
			<!-- <div class="contact-us-map" id="google_map" data-map-lat="-7.7784907" data-map-lng="110.4535683" data-icon-path="<?php echo base_url('asset_front/images/map/3.png'); ?>" data-map-zoom="16" style="height:100%;"></div> -->
			<br>
				<div id="map-canvas" style="width: 50%; height: 100%;float:left"></div>
	            <div id="pano" style="width: 50%; height: 100%;float:left"></div>
		</div> <!-- End of .contact_up_map_wrapper -->


		<!-- End of  Contact Us Map ______________________ -->



		<!-- Contact Information _________________________ -->

		<div class="contact_information container">
			<!-- inner_title__________ -->
			<div class="inner_title">
				<h2>Kontak Informasi</h2>
			</div>
			<!-- End inner_title______ -->
			<div class="row">
				<div class="col-lg-9 col-md-12 col-sm-12 col-xs-12">
					<p style="margin: 33px 0 41px 0;"></p>
				</div>
				<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12"></div>
			</div> <!-- End of .row -->

			<div class="row address_area">
				<div class="single_address col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<h5>Alamat Perusahaan</h5>
					<p>Gedung Karya Sehati, komplek Grand Cupuwatu,<br> Jl. Solo km.11,8 Kalasan, Yogyakarta</p>
				</div> <!-- End of .single_address -->

				<div class="single_address col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<h5>Alamat Email</h5>
					<p><i class="fa fa-envelope-o"></i> <a href="mailto:karyasehatiutama@gmail.com" "email me">karyasehatiutama@gmail.com</a></p>
				</div> <!-- End of .single_address -->

				<div class="single_address col-lg-3 col-md-2 col-sm-6 col-xs-12">
					<h5>Kontak & Hotline</h5>
					<a class="btn btn-danger" href="tel:0274496777" role="button"><i class="fa fa-phone"></i> Hubungi Kantor</a>
					<a class="btn btn-success" target="_blank" href="https://api.whatsapp.com/send?phone=6282134444360" role="button"><i class="fa fa-whatsapp"></i> Whatsapp </a>
					<!-- <p><a href="tel:0274496777" title=""></a><i class="fa fa-phone"></i> 0274 496 777</p>
					<p><a href="tel:082134444360" title=""></a><i class="fa fa-phone-square"></i> 0821 3444 4360</p> -->
				</div> <!-- End of .single_address -->

				<div class="single_address col-lg-3 col-md-4 col-sm-6 col-xs-12">
					<h5>Jam Kerja Kantor</h5>
					<p><b>Senin - Sabtu:</b> 08.30am to 16.30pm</p>
					<p><b>Minggu:</b> Tutup</p>
				</div> <!-- End of .single_address -->
			</div> <!-- End of .address-area -->


			
		</div> <!-- End of .contact_information -->

		<!-- End of Contact Information __________________ -->

		<!-- Footer __________________________________________ -->
		
		<?php $this->load->view('frontpages/footer.php'); ?>

		<!-- End of Footer ______________________________________ -->

		<?php require_once(APPPATH .'views/include/front/inc_script.php'); ?>
		<script type="text/javascript">
	      var lat = "-7.7787364";
	      var lang = "110.4557445";

	      function initialize() {

	        var baltimore = new google.maps.LatLng(lat, lang);
	        var baltimore1 = new google.maps.LatLng(lat, lang);

	        var mapOptions = {
	          center: baltimore,
	          zoom: 18
	        };
	        var map = new google.maps.Map(
	            document.getElementById('map-canvas'), mapOptions);
	            var cafeMarker = new google.maps.Marker({
	            position: baltimore1,
	            map: map,
	            icon: '<?php echo base_url('asset_front/images/asset_new/pin_marker.png'); ?>',
	            title: 'PT Karya Sehati Utama'
	        });
	        
	        var contentString = "<b>PT Karya Sehati Utama</b> <br> Gedung Karya Sehati, <br> komplek Grand Cupuwatu, Jl. Solo km.11,8 Kalasan Yogyakarta <br> Email : <a href='mailto:karyasehatiutama@gmail.co?subject=feedback'><b>karyasehatiutama@gmail.com</b></a><br> Kontak Kantor & Hotline : <b>0274-496-777 | <a href='https://api.whatsapp.com/send?phone=6282134444360' title='Whatsapp' target='_blank'>0821 3444 4360</a> </b>";

	        var infowindow = new google.maps.InfoWindow({
	          content: contentString
	        });

	        cafeMarker.addListener('click', function() {
	          infowindow.open(map, cafeMarker);
	        });

	        var panoramaOptions = {
	          position: baltimore,
	          pov: {
	            heading: 34,
	            pitch: 10
	          }
	        };
	        var panorama = new google.maps.StreetViewPanorama(document.getElementById('pano'), panoramaOptions);
	        map.setStreetView(panorama);
	      }

	      google.maps.event.addDomListener(window, 'load', initialize);
	    </script>

	</body>
</html>