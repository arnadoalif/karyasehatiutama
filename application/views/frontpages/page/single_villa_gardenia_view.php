<!DOCTYPE html>
<html lang="en">
	<head>
		<?php require_once(APPPATH .'views/include/front/inc_style.php'); ?>
	</head>
	<body class="gallery-page">

		<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

	 	<!-- pre loader  -->

	 	<div id="loader-wrapper">
			<div id="loader"></div>
		</div>

		<!-- Scroll To Top -->
		<button class="scroll-top tran3s">
			<span class="hvr-icon-bob"></span>
		</button>


       <!-- Top Header ______________________________________-->

		<?php $this->load->view('frontpages/top_header'); ?>

        <!-- End Of Top Header _______________________________-->


        <!-- Bottom header  _________________________________ -->

		<?php $this->load->view('frontpages/header_kontak'); ?>

        <!-- End of Bottom header ___________________________ -->


        <!-- Main Menu ______________________________________ -->

        <div class="main_menu container">

			<?php $this->load->view('frontpages/main_menu'); ?>

		</div>  <!-- End of #main_menu -->



		<!-- Inner Banner ________________________________ -->
		
		<section id="inner_banner">
			<div class="overlay">
				<div class="container">
					<div class="title">
						<h2>Villa Gardenia</h2>
						<!-- <ul>
							<li><a href="index.html">Home</a></li>
							<li><i class="fa fa-angle-double-right"></i></li>
							<li><a href="gallery-3columns-with-text.html">Gallery</a></li>
							<li><i class="fa fa-angle-double-right"></i></li>
							<li>Loyal Beach house</li>
						</ul> -->
					</div> <!-- End .title -->
				</div> <!-- End .container -->
			</div> <!-- End .overlay -->
		</section> <!-- End .inner_banner -->

		<!-- End Inner Banner ____________________________ -->



		<!-- Gallery page content _______________________ -->

		<section class="single_gallery_details container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 gallery_img_holder">
					<div class="img_wrapper">
						<div id="myCarousel" class="carousel slide" data-ride="carousel">
						  <!-- Wrapper for slides -->
						  <div class="carousel-inner" role="listbox">
						    <div class="item active">
						      <a href="<?php echo base_url('asset_front/images/asset_new/slide_tlogoadi.png'); ?>" class="fancybox"><img src="<?php echo base_url('asset_front/images/asset_new/slide_tlogoadi.png'); ?>" alt="images" class="img-responsive"></a>
						    </div>

						    <div class="item">
						      <a href="<?php echo base_url('asset_front/images/asset_new/slide_tlogoadi.png'); ?>" class="fancybox"><img src="<?php echo base_url('asset_front/images/asset_new/slide_tlogoadi.png'); ?>" alt="images" class="img-responsive"></a>
						    </div>

						    <div class="item">
						     <a href="<?php echo base_url('asset_front/images/asset_new/slide_tlogoadi.png'); ?>" class="fancybox"><img src="<?php echo base_url('asset_front/images/asset_new/slide_tlogoadi.png'); ?>" alt="images" class="img-responsive"></a>
						    </div>

						  </div>

						  <!-- Left and right controls -->
						  <a class="left carousel-control tran3s" href="gallery-single.html#myCarousel" role="button" data-slide="prev">
						    <i class="fa fa-angle-left" aria-hidden="true"></i>
						    <span class="sr-only">Previous</span>
						  </a>
						  <a class="right carousel-control tran3s" href="gallery-single.html#myCarousel" role="button" data-slide="next">
						   <i class="fa fa-angle-right" aria-hidden="true"></i>
						    <span class="sr-only">Next</span>
						  </a>
						</div>
					</div> <!-- End of .img_wrapper -->
				</div> <!-- End of .gallery_img_holder -->
			</div> <!-- End of .row -->

			<div class="post_wrapper container">
				
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<div class="blog_post_meta">
						<div class="post">
							<img src="<?php echo base_url('asset_front/images/asset_new/logo_villagardenia.png'); ?>" class="img-responsive" alt="Image">
						</div> <!-- End .post -->
					</div> <!-- End .blog_post_meta -->
				</div>

				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<div class="blog_post_meta">
						<div class="post">
							<!-- inner_title__________ -->
							<div class="inner_title">
								<h5>Sekilas Latar Belakang Tentang Villa Gardenia</h5>
							</div>
							<!-- End inner_title______ -->
							<p>
							Villa Gardenia merupakan sebuah kawasan perumahan persembahan Karya Sehati Utama yang dikemas tidak hanya sebagai kawasan hunian saja, namun juga sebagai sebuah Villa. Seperti halnya villa pada umumnya, Villa Gardenia memiliki berbagai unsur yang mendukung dalam pengemasannya, mulai dari kondisi alamnya maupun konsep desain arsitekturnya. </p>
							<p>Villa Gardenia menempati lokasi yang sangat unik, yakni sebuah area perbukitan yang menawan dengan kondisi udara yang segar, serta kemudahan akses dan jarak tempuh dalam pencapaian menuju fasilitas-fasilitas kota, dimana kondisi tersebut merupakan sebuah kondisi lokasi yang sangat pas untuk sebuah kawasan hunian ideal.</p>
							<p>Villa Gardenia dibalut dengan gaya desain modern tropis, dimana menampilkan karakter desain elegant, kasual, dan juga responable terhadap cuaca tropis, yang mampu memaksimalkan sirkulasi udara dan pencahayaan, juga karakter bentuk bangunannya tinggi, sehingga responsive terhadap suhu panas.</p>
							<p>Seperti halnya karya-karya Karya Sehati Utama yang lainnya, yang selalu menorehkan muatan filosofi-filosofi kehidupan pada setiap karyanya, penerapan filosofi Candra Jiwa yang mendasari desain & elemen-elemen pendukung, memberikan nilai tersendiri pada kawasan Villa Gardenia, sehingga menjadikannya sebagai sebuah mahakarya yang berkarakter, adiluhung dan memiliki ruh.</p>

							</p>
						</div> <!-- End .post -->
					</div> <!-- End .blog_post_meta -->
				</div>
			</div> <!-- End .post_wrapper -->

			<div class="related_project">
				<div class="main-title2">
					<h2>Tipe Rumah Villa Gardernia</h2>
				</div>

				<div class="gallery_wrapper">
					<div class="container menu_wrapper">
						<ul class="gallery_menu">
							<li class="filter active tran3s" data-filter="all">Show All</li>
							<li class="filter tran3s" data-filter=".2lantai">2 Lantai</li>
							<li class="filter tran3s" data-filter=".1lantai">1 Lantai</li>
						</ul>
					</div> <!-- End .menu_wrapper -->

					<div class="gallery_item_container gallery_style_eight gallery_text container" id="mixitup_list">

						<div class="row">
							<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 mix 2lantai">
								<div class="single_item">
									<div class="img_holder">
										<img src="<?php echo base_url('asset_front/images/asset_new/img_produk/01.%20VGA_%20Kamboja.jpg'); ?>" alt="images" class="img-responsive">
										<div class="overlay tran3s">
											<div class="link">
												<a href="<?php echo base_url('asset_front/images/asset_new/img_produk/01.%20VGA_%20Kamboja.jpg'); ?>" class="fancybox"><i class="fa fa-search-plus"></i></a>
												<a href=""><i class="fa fa-link"></i></a>
											</div>
										</div> <!-- End .overlay -->
									</div> <!-- End .img_holder -->
									<div class="title">
										<a href="">
											<h5>Type Kamboja</h5>
											<i>(9x15 | LB : 85 | LT : 35)</i>
										</a>
									</div> <!-- End .title -->
								</div> <!-- End .single_item -->
							</div>

							<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 mix 1lantai">
								<div class="single_item">
									<div class="img_holder">
										<img src="<?php echo base_url('asset_front/images/asset_new/img_produk/02.%20VGA_Eidelweis.jpg'); ?>" alt="images" class="img-responsive">
										<div class="overlay tran3s">
											<div class="link">
												<a href="<?php echo base_url('asset_front/images/asset_new/img_produk/02.%20VGA_Eidelweis.jpg'); ?>" class="fancybox"><i class="fa fa-search-plus"></i></a>
												<a href=""><i class="fa fa-link"></i></a>
											</div>
										</div> <!-- End .overlay -->
									</div> <!-- End .img_holder -->
									<div class="title">
										<a href="">
											<h5>Type Eidelweis</h5>
											<i>(10.5x15 | LB : 70 | LT : 156)</i>
										</a>
									</div> <!-- End .title -->
								</div> <!-- End .single_item -->
							</div>

							<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 mix 1lantai">
								<div class="single_item">
									<div class="img_holder">
										<img src="<?php echo base_url('asset_front/images/asset_new/img_produk/03.%20VGA_Iris.jpg'); ?>" alt="images" class="img-responsive">
										<div class="overlay tran3s">
											<div class="link">
												<a href="<?php echo base_url('asset_front/images/asset_new/img_produk/03.%20VGA_Iris.jpg'); ?>" class="fancybox"><i class="fa fa-search-plus"></i></a>
												<a href=""><i class="fa fa-link"></i></a>
											</div>
										</div> <!-- End .overlay -->
									</div> <!-- End .img_holder -->
									<div class="title">
										<a href="">
											<h5>Type Iris</h5>
											<i>(8.5x15 | LB : 63 | LT : 127)</i>
										</a>
									</div> <!-- End .title -->
								</div> <!-- End .single_item -->
							</div>

							<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 mix 1lantai">
								<div class="single_item">
									<div class="img_holder">
										<img src="<?php echo base_url('asset_front/images/asset_new/img_produk/04.%20VGA_Tulip.jpg'); ?>" alt="images" class="img-responsive">
										<div class="overlay tran3s">
											<div class="link">
												<a href="<?php echo base_url('asset_front/images/asset_new/img_produk/04.%20VGA_Tulip.jpg'); ?>" class="fancybox"><i class="fa fa-search-plus"></i></a>
												<a href=""><i class="fa fa-link"></i></a>
											</div>
										</div> <!-- End .overlay -->
									</div> <!-- End .img_holder -->
									<div class="title">
										<a href="">
											<h5>Type Tulip</h5>
											<i>(8x13,5 | LB : 54 | LT : 108)</i>
										</a>
									</div> <!-- End .title -->
								</div> <!-- End .single_item -->
							</div>

							<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 mix 1lantai">
								<div class="single_item">
									<div class="img_holder">
										<img src="<?php echo base_url('asset_front/images/asset_new/img_produk/05.%20VGA_Camelia.jpg'); ?>" alt="images" class="img-responsive">
										<div class="overlay tran3s">
											<div class="link">
												<a href="<?php echo base_url('asset_front/images/asset_new/img_produk/05.%20VGA_Camelia.jpg'); ?>" class="fancybox"><i class="fa fa-search-plus"></i></a>
												<a href=""><i class="fa fa-link"></i></a>
											</div>
										</div> <!-- End .overlay -->
									</div> <!-- End .img_holder -->
									<div class="title">
										<a href="">
											<h5>Type Camelia</h5>
											<i>(7,25x12,75 | LB : 45 | LT : 92)</i>
										</a>
									</div> <!-- End .title -->
								</div> <!-- End .single_item -->
							</div>

							<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 mix 1lantai">
								<div class="single_item">
									<div class="img_holder">
										<img src="<?php echo base_url('asset_front/images/asset_new/img_produk/06.%20VGA_Anyelir.jpg'); ?>" alt="images" class="img-responsive">
										<div class="overlay tran3s">
											<div class="link">
												<a href="<?php echo base_url('asset_front/images/asset_new/img_produk/06.%20VGA_Anyelir.jpg'); ?>" class="fancybox"><i class="fa fa-search-plus"></i></a>
												<a href=""><i class="fa fa-link"></i></a>
											</div>
										</div> <!-- End .overlay -->
									</div> <!-- End .img_holder -->
									<div class="title">
										<a href="">
											<h5>Anyelir</h5>
											<i>(6x12 | LB : 36 | LT : 76)</i>
										</a>
									</div> <!-- End .title -->
								</div> <!-- End .single_item -->
							</div>

						</div>
					</div> <!-- End #mixitup_list -->

				</div> <!-- End .gallery_wrapper -->
			</div>

			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<div class="post">
						<!-- inner_title__________ -->
						<div class="inner_title">
							<h5>Spesifikasi</h5>
						</div>
						<!-- End inner_title______ -->
						<p><strong>Pondasi : </strong> Pasang Batu Kali</p>
						<p><strong>Dinding : </strong> Pasang bata merah, dispenser aci, finishing cat</p>
						<p><ul>
						    <li>- Rumah 2 Lt : Pasangan bata merah </li>
						    <li>- Rumah 1 Lt : Pasangan bata merah</li>
						    <li>- Plester aci, finishing cat  </li>
						</ul></p>
						<p><strong>Lantai : </strong>Keramik 40X40 Ex. Milan</p>
						<p><strong>Atap : </strong>Rangkap atap Baja ringan, Penutup atap genteng beton flat + finishing cat</p>
					</div> <!-- End .post -->
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<div class="post">
						<!-- inner_title__________ -->
						<div class="inner_title">
							<!-- <h5></h5> -->
						</div>
						<!-- End inner_title______ -->
						<p><strong>Kusen, Pintu & Jendela :</strong></p>
						<p>
							<ul>
								<li>- Kusen pintu : tanpa kusen</li>
                                <li>- Kusen jendela : kusen aluminium </li>
                                <li>- Daun pintu utama & samping :
                                	<ul>
                                	    <li>Type 85, 70, 63, 54 : panil jati lokal</li>
                                	    <li>Type 45, 36 : Pintu pabrikan HDF</li>
                                	</ul>
                                </li>
                                <li>- Daun pintu kamar : Pintu pabrikan HDF</li>
                                <li>- Daun jendela Aluminium putih + kaca bening 5mm </li>
                                <li>- Finishing daun pintu : Semi duco></li>
							</ul>
						</p>
						<p><strong>Plafond : </strong></p>
						<p>
							<ul>
								<li>- Rangka metal furring 2/4 </li>
								<li>- Gypsum Star /setara + cornice</li>
						    </ul>
					    </p>
					</div> <!-- End .post -->
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<div class="post">
						<!-- inner_title__________ -->
						<div class="inner_title">
							
						</div>
						<!-- End inner_title______ -->
						<p><strong>Kamar Mandi :</strong></p>
						<ul>
						    <li>- Keramik lantai 30x30 ex. KIA / setara</li>
						    <li>- Keramik dinding 25x40 ex. KIA / setara</li>
						</ul>
						<p><strong>Sanitair : </strong>
							<ul>
							    <li>Type 85, 70, 63, 54 : Closet duduk ex. INA</li>
							    <li>Type 45, 36 : Closet jongkok ex. INA</li>
							</ul>
						</p>
						<p><strong>Airbersih : </strong></p>
						<ul>
						    <li>- PDAM</li>
						    <li>- Water Toren 500 lt ex. Penguin / setara</li>
						</ul>
						<p><strong>Jaringan Airbersih & Kotor : </strong></p>
						<ul>
						    <li>- - Pipa PVC ex : Wavin / setara</li>
						</ul>
						<p><strong>Elektrikal : </strong></p>
						<ul>
						    <li>- Daya listrik PLN 
								<ul>
								    <li>Type 2 Lt : 2200 Watt </li>
								    <li>Type 1 Lt : 1300 Watt</li>
								</ul>
						    </li>
						    <li>- Stop kontak & saklar ex. Broco</li>
						</ul>

					</div> <!-- End .post -->
				</div>
			</div>

			<div class="related_project">
				<div class="main-title2">
					<h2>TYPE VILLA GARDENIA</h2>
				</div>

				<div class="slider_wrapper">
					<div class="related_project_slider">

						<!-- Single Project -->
						<div class="item">
							<div class="single_item">
								<div class="img_holder">
									<img src="<?php echo base_url('asset_front/images/gallery/18.jpg'); ?>" alt="images" class="img-responsive">
									<div class="overlay tran3s">
										<div class="link">
											<a href="<?php echo base_url('asset_front/images/gallery/18.jpg'); ?>" class="fancybox"><i class="fa fa-search-plus"></i></a>
											<a href="gallery-single.html"><i class="fa fa-link"></i></a>
										</div>
									</div> <!-- End .overlay -->
								</div> <!-- End .img_holder -->
								<div class="title">
									<a href="gallery-single.html">
										<h6>Agriculture Lab For Sale</h6>
										<i>Apartment, Sale</i>
									</a>
								</div> <!-- End .title -->
							</div> <!-- End .single_item -->
						</div> <!-- End of .item -->

						<!-- Single Project -->
						<div class="item">
							<div class="single_item">
								<div class="img_holder">
									<img src="<?php echo base_url('asset_front/images/gallery/19.jpg'); ?>" alt="images" class="img-responsive">
									<div class="overlay tran3s">
										<div class="link">
											<a href="<?php echo base_url('asset_front/images/gallery/19.jpg'); ?>" class="fancybox"><i class="fa fa-search-plus"></i></a>
											<a href="gallery-single.html"><i class="fa fa-link"></i></a>
										</div>
									</div> <!-- End .overlay -->
								</div> <!-- End .img_holder -->
								<div class="title">
									<a href="gallery-single.html">
										<h6>U.S South Padre Beach House</h6>
										<i>House, Rent</i>
									</a>
								</div> <!-- End .title -->
							</div> <!-- End .single_item -->
						</div> <!-- End of .item -->

						<!-- Single Project -->
						<div class="item">
							<div class="single_item">
								<div class="img_holder">
									<img src="<?php echo base_url('asset_front/images/gallery/20.jpg'); ?>" alt="images" class="img-responsive">
									<div class="overlay tran3s">
										<div class="link">
											<a href="<?php echo base_url('asset_front/images/gallery/20.jpg'); ?>" class="fancybox"><i class="fa fa-search-plus"></i></a>
											<a href="gallery-single.html"><i class="fa fa-link"></i></a>
										</div>
									</div> <!-- End .overlay -->
								</div> <!-- End .img_holder -->
								<div class="title">
									<a href="gallery-single.html">
										<h6>St.Fransisco Apartment</h6>
										<i>Apartment, Rent</i>
									</a>
								</div> <!-- End .title -->
							</div> <!-- End .single_item -->
						</div> <!-- End of .item -->

						<!-- Single Project -->
						<div class="item">
							<div class="single_item">
								<div class="img_holder">
									<img src="<?php echo base_url('asset_front/images/gallery/21.jpg'); ?>" alt="images" class="img-responsive">
									<div class="overlay tran3s">
										<div class="link">
											<a href="<?php echo base_url('asset_front/images/gallery/21.jpg'); ?>" class="fancybox"><i class="fa fa-search-plus"></i></a>
											<a href="gallery-single.html"><i class="fa fa-link"></i></a>
										</div>
									</div> <!-- End .overlay -->
								</div> <!-- End .img_holder -->
								<div class="title">
									<a href="gallery-single.html">
										<h6>Nicole Bunya Farm House</h6>
										<i>House, Rent</i>
									</a>
								</div> <!-- End .title -->
							</div> <!-- End .single_item -->
						</div> <!-- End of .item -->
					</div> <!-- End of .related_project_slider -->
				</div> <!-- End of .slider_wrapper -->
			</div>
		</section> <!-- End of .single_gallery_details -->

		<!-- End Gallery page content ___________________ -->




		<!-- Footer __________________________________________ -->
		
		<?php $this->load->view('frontpages/footer'); ?>

		<!-- End of Footer ______________________________________ -->

		<?php require_once(APPPATH .'views/include/front/inc_script.php'); ?>

	</body>
</html>