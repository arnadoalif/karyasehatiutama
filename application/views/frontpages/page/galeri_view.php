<!DOCTYPE html>
<html lang="en">
	<head>
		<?php require_once(APPPATH .'views/include/front/inc_style.php'); ?>
	</head>
	<body class="gallery-page">

		<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

	 	<!-- pre loader  -->

	 	<div id="loader-wrapper">
			<div id="loader"></div>
		</div>

		<!-- Scroll To Top -->
		<button class="scroll-top tran3s">
			<span class="hvr-icon-bob"></span>
		</button>


        <!-- Top Header ______________________________________-->

		<?php $this->load->view('frontpages/top_header'); ?>

        <!-- End Of Top Header _______________________________-->


        <!-- Bottom header  _________________________________ -->

		<?php $this->load->view('frontpages/header_kontak'); ?>

        <!-- End of Bottom header ___________________________ -->


        <!-- Main Menu ______________________________________ -->

        <div class="main_menu container">

			<?php $this->load->view('frontpages/main_menu'); ?>

		</div>  <!-- End of #main_menu -->


        <!-- End of Main Menu _______________________________ -->

		<!-- Inner Banner ________________________________ -->
		
		<section id="inner_banner">
			<div class="overlay">
				<div class="container">
					<div class="title">
						<h2>Galeri Kami</h2>
						<!-- <ul>
							<li><a href="index.html">Home</a></li>
							<li><i class="fa fa-angle-double-right"></i></li>
							<li><a href="gallery-3columns-with-text.html">Gallery</a></li>
							<li><i class="fa fa-angle-double-right"></i></li>
							<li>Manasory Style</li>
						</ul> -->
					</div> <!-- End .title -->
				</div> <!-- End .container -->
			</div> <!-- End .overlay -->
		</section> <!-- End .inner_banner -->

		<!-- End Inner Banner ____________________________ -->



		<!-- Gallery page content _______________________ -->

		<div class="gallery_wrapper">
			<div class="container menu_wrapper">
				<ul class="gallery_menu">
					<li class="is-checked tran3s" data-filter="*">Show All</li>
					<?php foreach ($group_name as $gr_name): ?>
						<li class="tran3s" data-filter=".<?php echo $gr_name->Kategori ?>"><?php echo str_replace('_',' ', strtoupper($gr_name->Kategori)) ?></li>
					<?php endforeach ?>
				</ul>
			</div> <!-- End .menu_wrapper -->

			<div class="gallery_item_container gallery_masonary container" id="isotop_wrapper">

				<div class="row">
					
					<?php foreach ($data_gallery as $dt_gallery): ?>
						
						<div class="isotop_item tran3s <?php echo $dt_gallery->Kategori ?>">
							<div class="single_item">
								<img src="<?php echo base_url('storage_img/img_cover_gallery/'.$dt_gallery->Cover_Name); ?> " alt="images" class="img-responsive">
								<div class="overlay tran3s">
									<div class="link">
										<a href="<?php echo base_url('storage_img/img_cover_gallery/'.$dt_gallery->Cover_Name); ?>" class="fancybox"><i class="fa fa-search-plus"></i></a>
										<a href="<?php echo base_url('page/album/'.$dt_gallery->Slug_Url); ?>"><i class="fa fa-link"></i></a>
									</div>
									<div class="text">
										<h4><?php echo $dt_gallery->Nama_Gallery ?></h4>
										<i><?php echo $dt_gallery->Body_Nama_Gallery ?></i>
									</div>
								</div> <!-- End .overlay -->
							</div> <!-- End .single_item -->
						</div>

					<?php endforeach ?>

				</div>

				<div class="clear_fix"></div>

			</div> <!-- End #mixitup_list -->

			<!-- <div class="container">
				<a href="gallery-fullwidth-4columns.html#" class="load_more tran3s">Load More</a>
			</div> -->
		</div> <!-- End .gallery_wrapper -->

		<!-- Footer __________________________________________ -->
		
		<?php $this->load->view('frontpages/footer.php'); ?>

		<!-- End of Footer ______________________________________ -->

		<?php require_once(APPPATH .'views/include/front/inc_script.php'); ?>

	</body>
</html>