<!DOCTYPE html>
<html lang="en">
	<head>
		<?php require_once(APPPATH .'views/include/front/inc_style.php'); ?>
	</head>
	<body class="gallery-page">

		<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

	 	<!-- pre loader  -->

	 	<div id="loader-wrapper">
			<div id="loader"></div>
		</div>

		<!-- Scroll To Top -->
		<button class="scroll-top tran3s">
			<span class="hvr-icon-bob"></span>
		</button>


        <!-- Top Header ______________________________________-->

		<?php $this->load->view('frontpages/top_header'); ?>

        <!-- End Of Top Header _______________________________-->


        <!-- Bottom header  _________________________________ -->

		<?php $this->load->view('frontpages/header_kontak'); ?>

        <!-- End of Bottom header ___________________________ -->


        <!-- Main Menu ______________________________________ -->

        <div class="main_menu container">

			<?php $this->load->view('frontpages/main_menu'); ?>

		</div>  <!-- End of #main_menu -->

        <!-- End of Main Menu _______________________________ -->



		<!-- Inner Banner ________________________________ -->
		
		<section id="inner_banner">
			<div class="overlay">
				<div class="container">
					<div class="title">
						<h2>PORTFOLIO</h2>
					</div> <!-- End .title -->
				</div> <!-- End .container -->
			</div> <!-- End .overlay -->
		</section> <!-- End .inner_banner -->

		<!-- End Inner Banner ____________________________ -->



		<!-- Gallery page content _______________________ -->

		<div class="gallery_wrapper">
			<div class="container menu_wrapper">

			</div> <!-- End .menu_wrapper -->

			<div class="gallery_item_container gallery_style_eight gallery_text container" id="mixitup_list">

				<div class="row">
					<?php foreach ($data_portfolio as $dt_portfolio): ?>
						
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 mix">
							<div class="single_item">
								<div class="img_holder">
									<img src="<?php echo base_url('storage_img/img_cover_portfolio/'.$dt_portfolio->Cover_Img); ?>" alt="<?php echo $dt_portfolio->Nama_Portfolio ?>" class="img-responsive">
									<div class="overlay tran3s">
										<div class="link">
											<a href="<?php echo base_url('storage_img/img_cover_portfolio/'.$dt_portfolio->Cover_Img); ?>" class="fancybox"><i class="fa fa-search-plus"></i></a>
											<a href="<?php echo base_url('page/portofolio/'.$dt_portfolio->Slug_Url); ?>"><i class="fa fa-link"></i></a>
										</div>
									</div>
								</div>
								<div class="title">
									<a href="<?php echo base_url('page/portofolio/'.$dt_portfolio->Slug_Url); ?>">
										<h5><?php echo $dt_portfolio->Nama_Portfolio ?></h5>
										<i><?php echo $dt_portfolio->Alamat_Portfolio ?></i>
									</a>
								</div> <!-- End .title -->
							</div> <!-- End .single_item -->
						</div>

					<?php endforeach ?>

				</div>
			</div> <!-- End #mixitup_list -->
		</div> <!-- End .gallery_wrapper -->


		<!-- End Gallery page content ___________________ -->



		<!-- Footer __________________________________________ -->
		
		<?php $this->load->view('frontpages/footer.php'); ?>

		<!-- End of Footer ______________________________________ -->

		<?php require_once(APPPATH .'views/include/front/inc_script.php'); ?>

	</body>
</html>