<!DOCTYPE html>
<html lang="en">
	<head>
		<?php require_once(APPPATH .'views/include/front/inc_style.php'); ?>
	</head>
	<body class="error-page">

		<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

	 	<!-- pre loader  -->

	 	<div id="loader-wrapper">
			<div id="loader"></div>
		</div>


		<!-- Scroll To Top -->
		<button class="scroll-top tran3s">
			<span class="hvr-icon-bob"></span>
		</button>


        <!-- Top Header ______________________________________-->

		<?php $this->load->view('frontpages/top_header'); ?>

        <!-- End Of Top Header _______________________________-->


        <!-- Bottom header  _________________________________ -->

		<?php $this->load->view('frontpages/header_kontak'); ?>

        <!-- End of Bottom header ___________________________ -->


        <!-- Main Menu ______________________________________ -->

        <div class="main_menu container">

			<?php $this->load->view('frontpages/main_menu'); ?>

		</div>  <!-- End of #main_menu -->


        <!-- End of Main Menu _______________________________ -->



		<!-- Error page content _____________________________ -->

		<div class="error_page_wrapper container">
			<h1>4<img src="images/error/1.png" alt="error">4</h1>
			<h2>Sorry! but we couldnt find this page</h2>
			<h6>This page you are looking for does not exsist <a href="404.html#">Report this?</a></h6>

			<div class="input_search">
				<form action="404.html#">
					<input type="text" placeholder="Search..." class="tran3s">
					<button><i class="fa fa-search" aria-hidden="true"></i></button>
				</form>
			</div>
		</div> <!-- End of .error_page_wrapper -->


		<!-- End of Error page content ______________________ -->


		<!-- Footer __________________________________________ -->
		
		<?php $this->load->view('frontpages/footer.php'); ?>

		<!-- End of Footer ______________________________________ -->

		<?php require_once(APPPATH .'views/include/front/inc_script.php'); ?>

	</body>
</html>