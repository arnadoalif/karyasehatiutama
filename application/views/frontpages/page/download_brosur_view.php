<!DOCTYPE html>
<html lang="en">
	<head>
		<?php require_once(APPPATH .'views/include/front/inc_style.php'); ?>
	</head>
	<body class="home">

		<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

	 	<!-- pre loader  -->

	 	<!-- <div id="loader-wrapper">
			<div id="loader"></div>
		</div> -->

		<!-- Scroll To Top -->
		<button class="scroll-top tran3s">
			<span class="hvr-icon-bob"></span>
		</button>


        <!-- Top Header ______________________________________-->

		<?php $this->load->view('frontpages/top_header'); ?>

        <!-- End Of Top Header _______________________________-->


        <!-- Bottom header  _________________________________ -->

		<?php $this->load->view('frontpages/header_kontak'); ?>

        <!-- End of Bottom header ___________________________ -->


        <!-- Main Menu ______________________________________ -->

        <div class="main_menu container">

			<?php $this->load->view('frontpages/main_menu'); ?>

		</div>  <!-- End of #main_menu -->


        <!-- End of Main Menu _______________________________ -->

		<!-- Inner Banner ________________________________ -->
		
		<section id="inner_banner">
			<div class="overlay">
				<div class="container">
					<div class="title">
						<h2>Download Brosur KSU</h2>
					</div> <!-- End .title -->
				</div> <!-- End .container -->
			</div> <!-- End .overlay -->
		</section> <!-- End .inner_banner -->

		<!-- End Inner Banner ____________________________ -->



		<!-- Pricing plans Table _________________________ -->

		<div class="pricing_plan_table container">
			
				<div class="row">
					<?php foreach ($data_produk as $dt_produk): ?>
						
					<div class="col-lg-6 col-md-3 col-sm-6 col-xs-12">
						<div class="single_price_table hvr-float-shadow">
							<h3 class="tran3s"><?php echo $dt_produk->Nama_Produk; ?></h3>
							<span class="tran3s"><i class="fa fa-file-pdf-o"></i></span>
							<img style="margin: 0 auto;" src="<?php echo base_url('storage_img/img_cover_produk/'.$dt_produk->Cover_Produk); ?>" class="img-responsive" alt="Image">
							<a target="_blank" href="<?php echo $dt_produk->Brosur_name ?>" class="tran3s">Download Sekarang</a>
						</div> <!-- End .single_price_table -->
					</div>

					<?php endforeach ?>
				</div> <!-- End .row -->
		</div> <!-- End .pricing_plan_table -->


		<!-- End Pricing plans Table ____________________ -->



		<!-- Footer __________________________________________ -->
		
		<?php $this->load->view('frontpages/footer.php'); ?>

		<!-- End of Footer ______________________________________ -->

		<?php require_once(APPPATH .'views/include/front/inc_script.php'); ?>

	</body>
</html>