<!DOCTYPE html>
<html lang="en">
	<head>
		<?php require_once(APPPATH .'views/include/front/inc_style.php'); ?>
	</head>
	<body class="gallery-page">

		<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

	 	<!-- pre loader  -->

	 	<div id="loader-wrapper">
			<div id="loader"></div>
		</div>

		<!-- Scroll To Top -->
		<button class="scroll-top tran3s">
			<span class="hvr-icon-bob"></span>
		</button>


        <!-- Top Header ______________________________________-->

		<?php $this->load->view('frontpages/top_header'); ?>

        <!-- End Of Top Header _______________________________-->


        <!-- Bottom header  _________________________________ -->

		<?php $this->load->view('frontpages/header_kontak'); ?>

        <!-- End of Bottom header ___________________________ -->


        <!-- Main Menu ______________________________________ -->

        <div class="main_menu container">

			<?php $this->load->view('frontpages/main_menu'); ?>

		</div>  <!-- End of #main_menu -->


        <!-- End of Main Menu _______________________________ -->



		<!-- Inner Banner ________________________________ -->
		
		<section id="inner_banner">
			<div class="overlay">
				<div class="container">
					<div class="title">
						<h2><?php echo $data_portfolio->Nama_Portfolio ?></h2>
						<ul>
							<li></li>
						</ul>
					</div> <!-- End .title -->
				</div> <!-- End .container -->
			</div> <!-- End .overlay -->
		</section> <!-- End .inner_banner -->

		<!-- End Inner Banner ____________________________ -->



		<!-- Gallery page content _______________________ -->

		<section class="single_gallery_details container">
			<div class="row">
				<div class="col-lg-12 col-md-8 col-sm-12 col-xs-12 gallery_img_holder">
					<div class="img_wrapper">
						<div id="myCarousel" class="carousel slide" data-ride="carousel">
						  <!-- Wrapper for slides -->
						  <div class="carousel-inner" role="listbox">
						    
						    <div class="item active">
						      <a href="<?php echo base_url('storage_img/img_cover_portfolio/'.$data_portfolio->Cover_Img); ?>" class="fancybox"><img src="<?php echo base_url('storage_img/img_cover_portfolio/'.$data_portfolio->Cover_Img); ?>" alt="images" class="img-responsive"></a>
						    </div>

						    <?php foreach ($data_berkas_portfolio as $key => $dt_berkas): ?>
						    	
						    	<div class="item">
							      <a href="<?php echo base_url('storage_img/img_slide_portfolio/'.$dt_berkas->Nama_img); ?>" class="fancybox"><img src="<?php echo base_url('storage_img/img_slide_portfolio/'.$dt_berkas->Nama_img); ?>" alt="images" class="img-responsive"></a>
							    </div>

						    <?php endforeach ?>

						  </div>

						  <!-- Left and right controls -->
						  <a class="left carousel-control tran3s" href="gallery-single.html#myCarousel" role="button" data-slide="prev">
						    <i class="fa fa-angle-left" aria-hidden="true"></i>
						    <span class="sr-only">Previous</span>
						  </a>
						  <a class="right carousel-control tran3s" href="gallery-single.html#myCarousel" role="button" data-slide="next">
						   <i class="fa fa-angle-right" aria-hidden="true"></i>
						    <span class="sr-only">Next</span>
						  </a>
						</div>
					</div> <!-- End of .img_wrapper -->
				</div> <!-- End of .gallery_img_holder -->
			</div> <!-- End of .row -->

			<div class="related_project">
				<div class="main-title2">
					<h2>Portfolio Lainnya</h2>
				</div>

				<div class="slider_wrapper">
					<div class="related_project_slider">

						<?php foreach ($data_portfolio_lainnya as $dt_portfolio_lainnya): ?>
							
						<!-- Single Project -->
						<div class="item">
							<div class="single_item">
								<div class="img_holder">
									<img src="<?php echo base_url('storage_img/img_cover_portfolio/'.$dt_portfolio_lainnya->Cover_Img); ?>" alt="<?php echo $dt_portfolio_lainnya->Nama_Portfolio ?>" class="img-responsive">
									<div class="overlay tran3s">
										<div class="link">
											<a href="<?php echo base_url('storage_img/img_cover_portfolio/'.$dt_portfolio_lainnya->Cover_Img); ?>" class="fancybox"><i class="fa fa-search-plus"></i></a>
											<a href="<?php echo base_url('page/portofolio/'.$dt_portfolio_lainnya->Slug_Url); ?>"><i class="fa fa-link"></i></a>
										</div>
									</div> <!-- End .overlay -->
								</div> <!-- End .img_holder -->
								<div class="title">
									<a href="<?php echo base_url('page/property/'.$dt_portfolio_lainnya->Slug_Url); ?>">
										<h6><?php echo $dt_portfolio_lainnya->Nama_Portfolio ?></h6>
										<i><?php echo $dt_portfolio_lainnya->Alamat_Portfolio ?> </i>
									</a>
								</div> <!-- End .title -->
							</div> <!-- End .single_item -->
						</div> <!-- End of .item -->

						<?php endforeach ?>



					</div> <!-- End of .related_project_slider -->
				</div> <!-- End of .slider_wrapper -->
			</div>


			
		</section> <!-- End of .single_gallery_details -->


		<!-- End Gallery page content ___________________ -->




		<!-- Footer __________________________________________ -->
		
		<?php $this->load->view('frontpages/footer.php'); ?>

		<!-- End of Footer ______________________________________ -->

		<?php require_once(APPPATH .'views/include/front/inc_script.php'); ?>

	</body>
</html>