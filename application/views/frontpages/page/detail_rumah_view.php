<!DOCTYPE html>
<html lang="en">
	<head>
		<?php require_once(APPPATH .'views/include/front/inc_style.php'); ?>
	</head>
	<body class="gallery-page">

		<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

	 	<!-- pre loader  -->

	 	<div id="loader-wrapper">
			<div id="loader"></div>
		</div>

		<!-- Scroll To Top -->
		<button class="scroll-top tran3s">
			<span class="hvr-icon-bob"></span>
		</button>


        <!-- Top Header ______________________________________-->

		<?php $this->load->view('frontpages/top_header'); ?>

        <!-- End Of Top Header _______________________________-->


        <!-- Bottom header  _________________________________ -->

		<?php $this->load->view('frontpages/header_kontak'); ?>

        <!-- End of Bottom header ___________________________ -->


        <!-- Main Menu ______________________________________ -->

        <div class="main_menu container">

			<?php $this->load->view('frontpages/main_menu'); ?>

		</div>  <!-- End of #main_menu -->


        <!-- End of Main Menu _______________________________ -->



		<!-- Inner Banner ________________________________ -->
		
		<section id="inner_banner">
			<div class="overlay">
				<div class="container">
					<div class="title">
						<h2><?php echo $data_property->Nama_Property ?></h2>
						<ul>
							<li>Produk Dari <?php echo $data_property->Nama_Produk ?></li>
						</ul>
					</div> <!-- End .title -->
				</div> <!-- End .container -->
			</div> <!-- End .overlay -->
		</section> <!-- End .inner_banner -->

		<!-- End Inner Banner ____________________________ -->



		<!-- Gallery page content _______________________ -->

		<section class="single_gallery_details container">
			<div class="row">
				<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12 gallery_img_holder">
					<div class="img_wrapper">
						<div id="myCarousel" class="carousel slide" data-ride="carousel">
						  <!-- Wrapper for slides -->
						  <div class="carousel-inner" role="listbox">
						    
						    <div class="item active">
						      <a href="<?php echo base_url('storage_img/img_cover_property/'.$data_property->Cover_Name); ?>" class="fancybox"><img src="<?php echo base_url('storage_img/img_cover_property/'.$data_property->Cover_Name); ?>" alt="images" class="img-responsive"></a>
						    </div>

						    <?php foreach ($data_berkas as $key => $dt_berkas): ?>
						    	
						    	<div class="item">
							      <a href="<?php echo base_url('storage_img/img_slide_property/'.$dt_berkas->Nama_img); ?>" class="fancybox"><img src="<?php echo base_url('storage_img/img_slide_property/'.$dt_berkas->Nama_img); ?>" alt="images" class="img-responsive"></a>
							    </div>

						    <?php endforeach ?>

						    <!-- <div class="item">
						      <a href="<?php echo base_url('asset_front/images/asset_new/shop/GTA/GTA_100_lt%201.jpg'); ?>" class="fancybox"><img src="<?php echo base_url('asset_front/images/asset_new/shop/GTA/GTA_100_lt%201.jpg'); ?>" alt="images" class="img-responsive"></a>
						    </div>

						    <div class="item">
						     <a href="<?php echo base_url('asset_front/images/asset_new/shop/GTA/GTA_100_lt%202.jpg'); ?>" class="fancybox"><img src="<?php echo base_url('asset_front/images/asset_new/shop/GTA/GTA_100_lt%202.jpg'); ?>" alt="images" class="img-responsive"></a>
						    </div> -->

						  </div>

						  <!-- Left and right controls -->
						  <a class="left carousel-control tran3s" href="gallery-single.html#myCarousel" role="button" data-slide="prev">
						    <i class="fa fa-angle-left" aria-hidden="true"></i>
						    <span class="sr-only">Previous</span>
						  </a>
						  <a class="right carousel-control tran3s" href="gallery-single.html#myCarousel" role="button" data-slide="next">
						   <i class="fa fa-angle-right" aria-hidden="true"></i>
						    <span class="sr-only">Next</span>
						  </a>
						</div>
					</div> <!-- End of .img_wrapper -->
				</div> <!-- End of .gallery_img_holder -->

				<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 single_gallery_propery_details">
					<div class="property_name">
						<h4>Detail <?php echo $data_property->Nama_Produk ?> <?php echo $data_property->Nama_Property ?></h4>
						<p>
							<?php echo $data_property->Deskripsi ?>
						</p>
					</div> <!-- End of .property_name -->

					<div class="info">
						<h4>Informasi Bangunan</h4>
						<?php echo $data_property->Info_Bangunan ?>
					</div> <!-- End of .info -->

					<div class="info">
						<a class="btn btn-success" target="_blank" href="https://api.whatsapp.com/send?phone=6282134444360" role="button"><i class="fa fa-whatsapp"></i> Hubungi Sekarang</a>
						<br><br>
						<a class="btn btn-danger" href="tel:0274496777" role="button"><i class="fa fa-phone"></i> Hubungi Kantor Kami</a>
					</div>
				</div> <!-- End of .single_gallery_propery_details -->
			</div> <!-- End of .row -->


			<div class="related_project">
				<div class="main-title2">
					<h2>Type Lainnya <?php echo $data_property->Nama_Produk ?></h2>
				</div>

				<div class="slider_wrapper">
					<div class="related_project_slider">

						<?php foreach ($data_property_lainnya as $dt_property_lainnya): ?>
							
						<!-- Single Project -->
						<div class="item">
							<div class="single_item">
								<div class="img_holder">
									<img src="<?php echo base_url('storage_img/img_cover_property/'.$dt_property_lainnya->Cover_Name); ?>" alt="<?php echo $dt_property_lainnya->Nama_Property ?>" class="img-responsive">
									<div class="overlay tran3s">
										<div class="link">
											<a href="<?php echo base_url('storage_img/img_cover_property/'.$dt_property_lainnya->Cover_Name); ?>" class="fancybox"><i class="fa fa-search-plus"></i></a>
											<a href="<?php echo base_url('page/property/'.$dt_property_lainnya->Slug_Property); ?>"><i class="fa fa-link"></i></a>
										</div>
									</div> <!-- End .overlay -->
								</div> <!-- End .img_holder -->
								<div class="title">
									<a href="<?php echo base_url('page/property/'.$dt_property_lainnya->Slug_Property); ?>">
										<h6><?php echo $dt_property_lainnya->Nama_Property ?></h6>
										<i><?php echo $dt_property_lainnya->Nama_Produk ?> <br> <?php echo $dt_property_lainnya->Luas_tanah ?> </i>
									</a>
								</div> <!-- End .title -->
							</div> <!-- End .single_item -->
						</div> <!-- End of .item -->

						<?php endforeach ?>



					</div> <!-- End of .related_project_slider -->
				</div> <!-- End of .slider_wrapper -->
			</div>
		</section> <!-- End of .single_gallery_details -->


		<!-- End Gallery page content ___________________ -->




		<!-- Footer __________________________________________ -->
		
		<?php $this->load->view('frontpages/footer.php'); ?>

		<!-- End of Footer ______________________________________ -->

		<?php require_once(APPPATH .'views/include/front/inc_script.php'); ?>

	</body>
</html>