<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| c
 *ontroller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'front';
$route['404_override'] = 'front';

/**
 *
 * Route Login
 *
 */

$route['login'] = 'login';
$route['out'] = 'login/action_logout';

/**
 *
 * Route Administrator
 */

/**
 * Menu Front 
 */
$route['admin'] = 'admin';
$route['admin/slide']   = 'admin/slider_view';
$route['admin/ads']     = 'admin/ads_view';
$route['admin/testimoni'] = 'admin/testimoni_view';
$route['admin/pesan'] = 'admin/pesan_view';
$route['admin/new-property'] = 'admin/new_property_view';
$route['admin/portofolio-property'] = 'admin/portofolio_property_view';
$route['admin/type-produk/(:any)'] = 'admin/type_produk_view/$1';
$route['admin/data-type-produk'] = 'admin/data_type_property_view';
$route['admin/divisi'] = 'admin/divisi_perusahaan_view';
$route['admin/gallery'] = 'admin/gallery_view';

// $route['admin/slide/json'] = 'admin/view_data_slide_json';

$route['admin/add_gallery_proyek/(:any)'] = 'admin/tambah_galleri_proyek_view/$1';
$route['admin/add_album/(:any)'] = 'admin/tambah_album_view/$1';
$route['admin/add_detail_produk/(:any)'] = 'admin/add_detail_produk/$1';

$route['admin/edit_slide_produk/(:any)'] = 'admin/edit_slide_type_produk/$1';
$route['admin/edit_slide_portfolio/(:any)'] = 'admin/edit_slide_portfolio/$1';

$route['admin/remove_detail_slide/(:any)/(:any)'] = 'admin/action_delete_detail_produk_slide/$1/$2';
$route['admin/remove_fasilitas/(:any)/(:any)'] = 'admin/action_delete_fasilitas/$1/$2';
$route['admin/remove_property/(:any)/(:any)'] = 'admin/action_delete_property/$1/$2';
$route['admin/remove_pesan/(:any)'] = 'admin/action_delete_pesan/$1';
$route['admin/remove_divisi/(:any)'] = 'admin/action_delete_divisi/$1';
$route['admin/remove_gallery/(:any)'] = 'admin/action_delete_gallery/$1';
$route['admin/remove_slide_property/(:any)/(:any)'] = 'admin/action_delete_slide_property/$1/$2';
$route['admin/remove_slide_portfolio/(:any)/(:any)'] = 'admin/action_delete_slide_portfolio/$1/$2';
$route['admin/remove_foto_gallery/(:any)/(:any)'] = 'admin/action_delete_foto_gallery/$1/$2';
$route['admin/remove_foto_gallery_proyek/(:any)/(:any)'] = 'admin/action_delete_gallery_proyek/$1/$2';
$route['admin/remove_slide/(:any)'] = 'admin/action_delete_slide/$1';



/**
 * Setting Menu
 */
$route['admin/tentang'] = 'admin/tentang_view';



/**
 *
 * Route Front Page
 *
 */
$route['page'] = 'front';
$route['page/ksu'] 			= 'front/about_view';
$route['page/kontak'] 		= 'front/kontak_view';
$route['page/portofolio'] 	= 'front/portfolio_view';
$route['page/galeri'] 	= 'front/galeri_view';
$route['page/shop'] = 'front/shop_view';
$route['page/brosur'] = 'front/download_brosur';

$route['page/property/(:any)'] = 'front/detail_rumah_view/$1';
 
$route['page/detail/tlogoadi'] = 'front/detail_tlogoadi_view';
$route['page/detail/villagardenia'] = 'front/detail_villa_gardenia_view';

$route['page/detail/(:any)'] = 'front/detail_produk_view/$1';
$route['page/album/(:any)'] = 'front/detail_album_view/$1';
$route['page/portofolio/(:any)'] = 'front/view_detail_portfolio/$1';

$route['page/slide/json'] = 'front/view_data_slide_json';





$route['translate_uri_dashes'] = FALSE;
