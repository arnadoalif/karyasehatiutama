<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		if($this->session->userdata('Session_log') != 1){
			redirect('login');
		}
		
		$this->load->library('user_agent');
		date_default_timezone_set('Asia/Jakarta');

		//Load Model
		$this->load->model('Tentang_model');
		$this->load->model('Pesan_model');
		$this->load->model('Slide_model');
		$this->load->model('Testimoni_model');
		$this->load->model('Portfolio_model');
		$this->load->model('Berkas_model');
		$this->load->model('Produk_model');
		$this->load->model('Lib_model');
		$this->load->model('Fasilitas_model');
		$this->load->model('Property_model');
		$this->load->model('Divisi_model');
		$this->load->model('Gallery_model');
		$this->load->model('Gallery_proyek_model');
	}

	public function index()
	{
		$m_pesan = new Pesan_model();
		$m_produk = new Produk_model();
		$m_property = new Property_model();
		$m_portfolio = new Portfolio_model();
		$m_testimoni = new Testimoni_model();

		$data['count_pesan'] = $m_pesan->view_data_pesan_by_status('Tbl_Pesan', '0')->num_rows();
		$data['count_produk'] = $m_produk->view_data_produk('Tbl_Produk')->num_rows();
		$data['count_property'] = $m_property->view_data_property('Tbl_Property')->num_rows();
		$data['count_portfolio'] = $m_portfolio->view_data_portfolio('Tbl_Portfolio')->num_rows();
		$data['count_testimoni'] = $m_testimoni->view_data_testimoni('Tbl_Testimoni')->num_rows();
 
		$data['data_produk'] = $m_produk->view_data_produk('Tbl_Produk')->result();

		$this->load->view('adminpages/page/home_view', $data);
	}

	public function tentang_view() {
		$m_tentang = new Tentang_model();
		$data['data_tentang'] = $m_tentang->view_data_tentang('Tbl_Tentang');
		$this->load->view('adminpages/page/tentang_view', $data);
	}

	public function slider_view() {
		$m_slide = new Slide_model();
		$data['data_slide'] = $m_slide->view_data_slide('Tbl_Slide')->result();
		$this->load->view('adminpages/page/slide_view', $data);
	}

	public function ads_view() {
		$this->load->view('adminpages/page/ads_view');
	}

	public function pesan_view() {
		$m_pesan = new Pesan_model();

		$data['data_pesan'] = $m_pesan->view_data_pesan('Tbl_Pesan')->result();
		$this->load->view('adminpages/page/pesan_view', $data);
	}

	public function testimoni_view() {
		$m_testimoni = new Testimoni_model();

		$data['data_testimoni'] = $m_testimoni->view_data_testimoni('Tbl_Testimoni')->result();
		$this->load->view('adminpages/page/testimoni_view', $data);
	}

	public function new_property_view() {
		$m_produk = new Produk_model();
		$data['data_produk'] = $m_produk->view_data_produk('Tbl_Produk')->result();
		$this->load->view('adminpages/page/new_property_view', $data);
	}

	public function add_detail_produk($kode_produk) {
		$m_berkas = new Berkas_model();
		$m_produk = new Produk_model();
		$m_fasilitas = new Fasilitas_model();

		$data['kode_produk'] = $kode_produk;
		$data['data_berkas'] = $m_berkas->view_data_berkas_by_col('Tbl_Berkas', 'Kd_Produk', $kode_produk)->result();
		$data['data_produk'] = $m_produk->view_data_produk_just_one('Tbl_Produk', $kode_produk);
		$data['data_fasilitas'] = $m_fasilitas->view_fasilitas_by_kode_produk('Tbl_Fasilitas', $kode_produk)->result();
		$this->load->view('adminpages/page/add_detail_produk_view', $data);
	}

	public function type_produk_view($kode_produk) {
		$m_produk = new Produk_model();

		$data['data_produk'] = $m_produk->view_data_produk_just_one('Tbl_Produk', $kode_produk);
		$this->load->view('adminpages/page/type_produk_view', $data);
	}

	public function portofolio_property_view() {
		$m_portfolio = new Portfolio_model();
		$data['data_portfolio'] = $m_portfolio->view_data_portfolio('Tbl_Portfolio')->result();
		$this->load->view('adminpages/page/portofolio_property_view', $data);
	}

	public function data_type_property_view() {
		$m_property = new Property_model();
		$data['data_property'] = $m_property->view_data_property('Tbl_Property')->result();
		$this->load->view('adminpages/page/data_type_produk_view', $data);
	}

	public function divisi_perusahaan_view() {
		$m_divisi = new Divisi_model();
		$data['data_divisi'] = $m_divisi->view_data_divisi('Tbl_Divisi')->result();
		$this->load->view('adminpages/page/add_divisi_perusahaan_view', $data);
	}

	public function gallery_view() {
		$m_gallery = new Gallery_model();

		$data['data_gallery'] = $m_gallery->view_data_gallery('Tbl_Gallery')->result();
		$this->load->view('adminpages/page/add_gallery_view', $data);
	}

	public function tambah_album_view($kode_slug) {
		$m_gallery = new Gallery_model();
		$m_berkas = new Berkas_model();

		$data['data_gallery'] = $m_gallery->view_data_gallery_by_slug('Tbl_Gallery', $kode_slug)->row();
        $data['data_berkas'] = $m_berkas->view_data_berkas('Tbl_Berkas', 'Kd_Gallery', $data['data_gallery']->Kd_Gallery)->result(); 

		$this->load->view('adminpages/page/add_full_gallery_view', $data);
	}

	public function edit_slide_type_produk($kode_property) {
		$m_berkas = new Berkas_model();
		$m_property = new Property_model();

		$data['data_property'] = $m_property->view_data_property_by_kode_property('Tbl_Property', $kode_property)->row();
		$data['data_berkas_slide'] = $m_berkas->view_data_berkas('Tbl_Berkas', 'Kd_Property', $kode_property)->result();
		$this->load->view('adminpages/page/edit_slide_type_produk_view', $data);
	} 

	public function edit_slide_portfolio($kode_portfolio) {
		$m_berkas = new Berkas_model();
		$m_portfolio = new Portfolio_model();

		$data['data_portofolio'] = $m_portfolio->view_data_portfolio('Tbl_Portfolio')->row();
		$data['data_berkas_slide'] = $m_berkas->view_data_berkas('Tbl_Berkas', 'Kd_Portfolio', $kode_portfolio)->result();
		$this->load->view('adminpages/page/edit_slide_portfolio_view', $data);
	}

	public function tambah_galleri_proyek_view($kode_produk) {
		$m_produk = new Produk_model();
		$m_g_proyek = new Gallery_proyek_model();

		$data['data_produk'] = $m_produk->view_data_produk_just_one('Tbl_Produk',$kode_produk);
		$data['data_gallery_proyek'] = $m_g_proyek->view_data_g_proyek_by_kode_proyek('Tbl_Gproyek', $kode_produk)->result();

		$this->load->view('adminpages/page/add_gallery_proyek_view', $data);
	}


	/**
	 *
	 * backend engine pesan
	 *
	 */
	
	public function action_edit_data_pesan() {
		$m_pesan = new Pesan_model();

		$kode_pesan = $this->input->post('kode_pesan');
		$data = array('Status' => '1');
		$result = $m_pesan->update_data_pesan('Tbl_Pesan', $kode_pesan, $data);

		if ($result > 0) {
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat menutup pesan.');
			redirect($this->agent->referrer());
		}

	}
	
	public function action_delete_pesan($kode_pesan) {
		$m_pesan = new Pesan_model();

		$data = array('Status' => '1');
		$result = $m_pesan->delete_data_pesan('Tbl_Pesan', $kode_pesan);

		if ($result > 0) {
			$this->session->set_flashdata('message_data', '<strong>Success </strong> Pesan berhasil di hapus.');
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat menghapus pesan.');
			redirect($this->agent->referrer());
		}
	}

	/**
	 *
	 * backend engine upload galeri proyek
	 *
	 */
	
	public function action_save_gallery_proyek() {
		$m_produk = new Produk_model();
		$m_lib = new Lib_model();
		$m_g_proyek = new Gallery_proyek_model();

		$kode_produk = $this->input->post('kode_produk');
		$title = $this->input->post('title');

		if ($_FILES['img_galleri_proyek']['error'] > 0) {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Pastikan gambar tidak boleh kosong.');
			redirect($this->agent->referrer());

		} else {

			$get_data = $m_produk->view_data_produk_just_one('Tbl_Produk', $kode_produk);
			$img_name = $m_lib->slug($title);

			$file_name = $_FILES['img_galleri_proyek']['name'];
			$file_size = $_FILES['img_galleri_proyek']['size'];
			$file_type = pathinfo($file_name, PATHINFO_EXTENSION);
			$new_file_name = date('Y').'_'.str_replace('-','_', $m_lib->slug($get_data->Nama_Produk)).'_'.str_replace('-','_',$img_name).'_'.implode('-', str_split(substr(strtolower(md5(microtime().rand(1000, 9999))), 0, 3), 4)).'.'.$file_type;

			$config['upload_path'] = './storage_img/img_galleri_proyek/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size']  = '*';
			$config['file_name'] = $new_file_name;
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('img_galleri_proyek')){
				$error = array('error' => $this->upload->display_errors());
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat upload gambar.');
				redirect($this->agent->referrer());
			}
			else{
				$data = array('upload_data' => $this->upload->data());
			}

			$data = array(
				'Kd_Produk' => $kode_produk,
				'Title' => $title,
				'Slug_Url' => $m_lib->slug($title),
				'Nama_img' => $new_file_name
			);

			$m_g_proyek->insert_data_g_proyek('Tbl_Gproyek', $data);
			$this->session->set_flashdata('message_data', '<strong>Success </strong> Gallery Proyek berhasil di tambah.');
			redirect($this->agent->referrer());
		}
	}

	public function action_update_gallery_proyek() {
		$m_produk = new Produk_model();
		$m_lib = new Lib_model();
		$m_g_proyek = new Gallery_proyek_model();

		$kode_produk = $this->input->post('kode_produk');
		$title = $this->input->post('title');
		$kode_gallery = $this->input->post('kode_gallery');

		if ($_FILES['img_new_galleri']['error'] > 0 ) {

			$data = array(
				'Title' => $title,
				'Slug_Url' => $m_lib->slug($title)
			);

			$result = $m_g_proyek->edit_data_g_proyek('Tbl_Gproyek', $kode_produk, $kode_gallery, $data);

			if ($result > 0) {
				$this->session->set_flashdata('message_data', '<strong>Success </strong> Gallery Proyek berhasil di ubah.');
				redirect($this->agent->referrer());
			} else {
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Data ini tidak tersedia.');
				redirect($this->agent->referrer());
			}
			

		} else {
            $get_data_gallery = $m_g_proyek->view_data_g_proyek_by_kode('Tbl_Gproyek', $kode_produk, $kode_gallery);
			$get_data = $m_produk->view_data_produk_just_one('Tbl_Produk', $kode_produk);
			$img_name = $m_lib->slug($title);

			$file_name = $_FILES['img_new_galleri']['name'];
			$file_size = $_FILES['img_new_galleri']['size'];
			$file_type = pathinfo($file_name, PATHINFO_EXTENSION);
			$new_file_name = date('Y').'_'.str_replace('-','_', $m_lib->slug($get_data->Nama_Produk)).'_'.str_replace('-','_',$img_name).'_'.implode('-', str_split(substr(strtolower(md5(microtime().rand(1000, 9999))), 0, 3), 4)).'.'.$file_type;

			$dir = './storage_img/img_galleri_proyek/'.$get_data_gallery->Nama_img;

			if (file_exists($dir)) {
				unlink($dir);	
			} 

			$config['upload_path'] = './storage_img/img_galleri_proyek/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size']  = '*';
			$config['file_name'] = $new_file_name;
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('img_new_galleri')){
				$error = array('error' => $this->upload->display_errors());
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat upload gambar.');
				redirect($this->agent->referrer());
			}
			else{
				$data = array('upload_data' => $this->upload->data());
			}

			$data = array(
				'Title' => $title,
				'Slug_Url' => $m_lib->slug($title),
				'Nama_img' => $new_file_name
			);
			$result = $m_g_proyek->edit_data_g_proyek('Tbl_Gproyek', $kode_produk, $kode_gallery, $data);
			if ($result > 0) {
				$this->session->set_flashdata('message_data', '<strong>Success </strong> Gallery Proyek berhasil di ubah.');
				redirect($this->agent->referrer());
			} else {
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Data ini tidak tersedia.');
				redirect($this->agent->referrer());
			}
			
		}
	}

	public function action_delete_gallery_proyek($kode_g_proyek, $kode_produk) {
		$m_g_proyek = new Gallery_proyek_model();

		$result = $m_g_proyek->delete_data_g_proyek('Tbl_Gproyek', $kode_produk, $kode_g_proyek);
		if ($result > 0) {
			$this->session->set_flashdata('message_data', '<strong>Success </strong> Gallery Proyek berhasil di ubah.');
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Data ini tidak tersedia.');
			redirect($this->agent->referrer());
		}
	}

	/**
	 *
	 * backend engine slide
	 *
	 */
	
	public function action_save_slide() {
		$m_slide = new Slide_model();
		$m_lib = new Lib_model();

		$judul_slide = $this->input->post('judul_slide');

		if ($_FILES['cover_slide']['error'] > 0) {			
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Gambar tidak boleh kosong.');
			redirect($this->agent->referrer());

		} else {

			$img_name = $m_lib->slug($judul_slide);
			$file_name = $_FILES['cover_slide']['name'];
			$file_size = $_FILES['cover_slide']['size'];
			$file_type = pathinfo($file_name, PATHINFO_EXTENSION);
			$new_file_name = date('Y').'_'.str_replace('-','_',$img_name).'_'.implode('-', str_split(substr(strtolower(md5(microtime().rand(1000, 9999))), 0, 3), 4)).'.'.$file_type;

			$config['upload_path'] = './storage_img/img_slide/';
			$config['allowed_types'] = 'jpg|png|jpeg';
			$config['max_size']  = '*';
			$config['file_name'] = $new_file_name;

			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('cover_slide')){
				$error = array('error' => $this->upload->display_errors());
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat upload gambar pastikan ukuran < 1 MB dan format jpg|png|jpeg.');
				redirect($this->agent->referrer());
			}
			else{
				$data = array('upload_data' => $this->upload->data());
			}

			$data = array(
				'Judul_Slide' => $judul_slide,
				'Cover_name' => $new_file_name
			);

			$m_slide->insert_data_slide('Tbl_Slide', $data);
			$this->session->set_flashdata('message_data', '<strong>Success </strong> Slide show berhasil di tambah.');
			redirect($this->agent->referrer());
		}
	}

	public function action_delete_slide($kode_slide) {
		$m_slide = new Slide_model();

		$result = $m_slide->delete_data_slide('Tbl_Slide', $kode_slide);
		if ($result > 0) {
			$this->session->set_flashdata('message_data', '<strong>Success </strong> Slide show berhasil di hapus.');
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat hapus slide.');
			redirect($this->agent->referrer());
		}
	}

	public function view_data_slide_json() {
		$m_slide = new Slide_model();
		$data_slide = $m_slide->view_data_slide('Tbl_Slide')->result();

		$data = array();
		foreach ($data_slide as $key => $dt_slide) {
			$data[] = array('src' => base_url('storage_img/img_slide/'.$dt_slide->Cover_name));
		}
		$dt = json_encode($data);
		print_r($dt);
		return $dt;
	}
	
	/**
	 *
	 * backend engine tentang
	 *
	 */
	

	public function action_save_tentang() {
		$m_tentang = new Tentang_model();
		$m_lib = new Lib_model();

		$kode_tentang   = $this->input->post('kode_tentang');
		$nama_perusahaan = $this->input->post('nama_perusahaan');
		$deskripsi 		= $this->input->post('deskripsi');
		$email 			= $this->input->post('email');
		$nomor_telepon  = $this->input->post('nomor_telepon');
		$title_salam    = $this->input->post('title_salam');
		$alamat 		= $this->input->post('alamat');
		$latitude 		= $this->input->post('latitude');
		$longitude 		= $this->input->post('longitude');

		if ($_FILES['img_kantor']['error'] > 0) {
			$new_file_name = 'logo_ksu_new.png';
			$data = array(
				'deskripsi' => $deskripsi,
				'email' => $email,
				'nomor_telepon' => $nomor_telepon,
				'title_salam' => $title_salam,
				'alamat' => $alamat,
				'latitude' => $latitude,
				'longitude' => $longitude,
				'img_tentang' => $new_file_name,
				'nama_perusahaan' => $nama_perusahaan,
				'last_update' => date("Y-m-d H:i:s")
			);

			$result = $m_tentang->update_data_tentang('Tbl_Tentang', $kode_tentang, $data);
			if ($result > 0) {
				$this->session->set_flashdata('message_data', '<strong>Success </strong> Tentang berhasil di ubah.');
				redirect($this->agent->referrer());
			} else {
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat mengubah data.');
				redirect($this->agent->referrer());
			}
		} else {

			$get_data = $this->db->where('Kd_Tentang', $kode_tentang);
			$get_data = $this->db->get('Tbl_Tentang', 1)->row();

			$dir = './storage_img/img_tentang/'.$get_data->img_tentang;

			if ($get_data->img_tentang != "logo_ksu_new.png") {
				unlink($dir);
			}

			$img_name = $m_lib->slug($_FILES['img_kantor']['name']);

			$file_name = $_FILES['img_kantor']['name'];
			$file_size = $_FILES['img_kantor']['size'];
			$file_type = pathinfo($file_name, PATHINFO_EXTENSION);
			$new_file_name = $img_name.'.'.$file_type;

			$config['upload_path'] = './storage_img/img_tentang/';
			$config['allowed_types'] = '*';
			$config['max_size']  = '*';
			$config['file_name'] = $new_file_name;
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('img_kantor')){
				$error = array('error' => $this->upload->display_errors());
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat upload gambar.');
				redirect($this->agent->referrer());
			}
			else{
				$data = array('upload_data' => $this->upload->data());
			}

			$data = array(
				'deskripsi' => $deskripsi,
				'nama_perusahaan' => $nama_perusahaan,
				'email' => $email,
				'nomor_telepon' => $nomor_telepon,
				'title_salam' => $title_salam,
				'alamat' => $alamat,
				'latitude' => $latitude,
				'longitude' => $longitude,
				'img_tentang' => $new_file_name,
				'nama_perusahaan' => $nama_perusahaan,
				'last_update' => date("Y-m-d H:i:s")
			);

			$result = $m_tentang->update_data_tentang('Tbl_Tentang', $kode_tentang, $data);
			if ($result > 0) {
				$this->session->set_flashdata('message_data', '<strong>Success </strong> Tentang berhasil di ubah.');
				redirect($this->agent->referrer());
			} else {
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat mengubah data.');
				redirect($this->agent->referrer());
			}

		}
	}

	/**
	 *
	 * backend engine devisi
	 *
	 */
	
	public function action_save_divisi() {
		$m_divisi = new Divisi_model();
		$m_lib = new Lib_model();

		$nama_divisi = $this->input->post('nama_divisi');
		$deskripsi_divisi = $this->input->post('deskripsi_divisi');

		if ($_FILES['img_divisi']['error'] > 0) {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Gambar tidak boleh kosong.');
				redirect($this->agent->referrer());
		} else {
			$img_name = $m_lib->slug($_FILES['img_divisi']['name']);

			$file_name = $_FILES['img_divisi']['name'];
			$file_size = $_FILES['img_divisi']['size'];
			$file_type = pathinfo($file_name, PATHINFO_EXTENSION);
			$new_file_name = $img_name.'.'.$file_type;

			$config['upload_path'] = './storage_img/img_divisi/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size']  = '*';
			$config['file_name']  = $new_file_name;
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('img_divisi')){
				$error = array('error' => $this->upload->display_errors());
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat upload gambar.');
				redirect($this->agent->referrer());
			}
			else{
				$data = array('upload_data' => $this->upload->data());
			}

			$data = array(
				'Nama_Divisi' =>$nama_divisi,
				'No_Urut' => 0,
				'Deskripsi_Divisi' => $deskripsi_divisi,
				'Cover_Name' => $new_file_name,
				'Create_at' =>  date("Y-m-d H:i:s")
			);

			$m_divisi->insert_data_divisi('Tbl_Divisi', $data);
			$this->session->set_flashdata('message_data', '<strong>Success </strong> Tentang berhasil di tambahkan.');
			redirect($this->agent->referrer());
		}
	}

	// bug nomor urut
	// public function action_update_devisi_no_urut() {
	// 	$m_divisi = new Divisi_model();

	// 	echo '<pre>';

	// 	$no_urut = $this->input->post('No_urut');
	// 	$no_urut_sebelumnya = $this->input->post('No_urut_sebelumnya');
	// 	$kode_divisi = $this->input->post('kode_divisi');
		
	// 	if ($kode_divisi == 0) {
	// 		$data = array('No_Urut' => $no_urut);
	// 		$default = $m_divisi->edit_data_divisi('Tbl_Divisi', $kode_divisi, $data);
	// 		if ($default > 0) {
	// 			$this->session->set_flashdata('message_data', '<strong>Success </strong> nomor urut berhasil di ubah.');
	// 			redirect($this->agent->referrer());
	// 		} else {
	// 			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Terjadi kesalahan saat update nomor urut.');
	// 			redirect($this->agent->referrer());
	// 		}
	// 	} else {

	// 		if (!empty($no_urut)) {
	// 			$data = array('No_Urut' => $no_urut);
	// 			$result = $m_divisi->edit_data_divisi_nomor_urut('Tbl_Divisi', $kode_divisi, $no_urut, $data);
	// 			if ($result > 0) {
	// 				$this->session->set_flashdata('message_data', '<strong>Success </strong> nomor urut berhasil di ubah.');
	// 				redirect($this->agent->referrer());
	// 			} else {
	// 				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Nomor urut ini sudah tersedia, setting default <strong>0</strong>.');
	// 				redirect($this->agent->referrer());
	// 			}
	// 		} else {
	// 			redirect($this->agent->referrer());
	// 		}

	// 	}
		
	// }
	
	public function action_update_deskripsi_divisi() {
		$m_divisi = new Divisi_model();

		$kode_divisi = $this->input->post('kode_divisi');
		$deskripsi_divisi = $this->input->post('deskripsi_divisi');

		$data = array('Deskripsi_Divisi' => $deskripsi_divisi);
		$result = $m_divisi->update_data_divisi('Tbl_Divisi', $kode_divisi, $data);
		if ($result > 0) {
			$this->session->set_flashdata('message_data', '<strong>Success </strong> Tentang berhasil di ubah.');
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat ubah deskripsi.');
			redirect($this->agent->referrer());
		}
	}

	public function action_update_name_cover() {
		$m_divisi = new Divisi_model();
		$m_lib = new Lib_model();

		$nama_divisi = $this->input->post('nama_divisi');
		$kode_divisi = $this->input->post('kode_divisi');
		$deskripsi_divisi = $this->input->post('deskripsi_divisi');

		if ($_FILES['img_divisi']['error'] > 0) {

			$data = array(
				'Nama_Divisi' => $nama_divisi,
				'Deskripsi_Divisi' => $deskripsi_divisi,
				'Update_at' =>  date("Y-m-d H:i:s")
			);

			$result = $m_divisi->update_data_divisi('Tbl_Divisi', $kode_divisi, $data);
			if ($result > 0) {
				$this->session->set_flashdata('message_data', '<strong>Success </strong> Tentang berhasil di ubah.');
				redirect($this->agent->referrer());
			} else {
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat ubah.');
				redirect($this->agent->referrer());
			}

		} else {

			$img_name = $m_lib->slug($_FILES['img_divisi']['name']);

			$file_name = $_FILES['img_divisi']['name'];
			$file_size = $_FILES['img_divisi']['size'];
			$file_type = pathinfo($file_name, PATHINFO_EXTENSION);
			$new_file_name = $img_name.'.'.$file_type;

			$result = $this->db->where('Kd_Divisi', $kode_divisi);
			$result = $this->db->get('Tbl_Divisi', 1)->row();

			$get_data = './storage_img/img_divisi/'.$result->Cover_Name;
			if (file_exists($get_data)) {
				unlink($get_data);
			} 

			$config['upload_path'] = './storage_img/img_divisi/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size']  = '*';
			$config['file_name']  = $new_file_name;
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('img_divisi')){
				$error = array('error' => $this->upload->display_errors());
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat upload gambar.');
				redirect($this->agent->referrer());
			}
			else{
				$data = array('upload_data' => $this->upload->data());
			}

			$data = array(
				'Nama_Divisi' => $nama_divisi,
				'Cover_Name' => $new_file_name,
				'Deskripsi_Divisi' => $deskripsi_divisi,
				'Update_at' =>  date("Y-m-d H:i:s")
			);

			$result = $m_divisi->update_data_divisi('Tbl_Divisi', $kode_divisi, $data);
			if ($result > 0) {
				$this->session->set_flashdata('message_data', '<strong>Success </strong> Tentang berhasil di ubah.');
				redirect($this->agent->referrer());
			} else {
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat ubah.');
				redirect($this->agent->referrer());
			}
		}
	}

	public function action_delete_divisi($kode_divisi) {
		$m_divisi = new Divisi_model();

		$result = $m_divisi->delete_data_divisi('Tbl_Divisi', $kode_divisi);
		if ($result > 0) {
			$this->session->set_flashdata('message_data', '<strong>Success </strong> Tentang berhasil di hapus.');
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat menghapus data.');
			redirect($this->agent->referrer());
		}
	}

/**
 *
 * backend engine testimoni
 *
 */


	public function action_save_testimoni() {
		$m_testimoni = new Testimoni_model();

		$nama_testimoni = $this->input->post('nama_testimoni');
		$alamat_testimoni = $this->input->post('alamat_testimoni');
		$isi_testimoni = $this->input->post('isi_testimoni');

		$nama_gambar   = str_replace(" ","_", $nama_testimoni);

		$error_img = $_FILES['avatar_testimoni']['error'];
		if ($error_img > 1) {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Testimoni gagal di tambahakan gambar avatar tidak boleh kosong.');
			redirect($this->agent->referrer());
		} else {

			$file_name = $_FILES['avatar_testimoni']['name'];
			$file_size = $_FILES['avatar_testimoni']['size'];
			$file_type = pathinfo($file_name, PATHINFO_EXTENSION);
			$new_file_name = $nama_gambar.'.'.$file_type;

			$config['upload_path'] = './storage_img/img_testimoni/';
			$config['allowed_types'] = '*';
			$config['file_name'] = $new_file_name;

			$this->load->library('upload', $config);

			if (!is_dir('./storage_img/img_testimoni/')) {
				mkdir('./storage_img/img_testimoni/', 0777);
				if ( ! $this->upload->do_upload('avatar_testimoni')){
					$error = array('error' => $this->upload->display_errors());
					$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Testimoni gagal di tambahakan.');
					redirect($this->agent->referrer());
				}
				else{
					$data = array('upload_data' => $this->upload->data());
				}

			} else {
				if ( ! $this->upload->do_upload('avatar_testimoni')){
					$error = array('error' => $this->upload->display_errors());
					$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Testimoni gagal di tambahakan.');
					redirect($this->agent->referrer());
				}
				else{
					$data = array('upload_data' => $this->upload->data());
				}

			}
		}

		$data = array(
			'Kd_Testimoni' => implode('-', str_split(substr(strtolower(md5(microtime().rand(1000, 9999))), 0, 12), 4)),
			'Nama' => $nama_testimoni,
			'Alamat' => $alamat_testimoni,
			'Isi_Testimoni' => $isi_testimoni,
			'Avatar' => $new_file_name,
			'Tgl_Create' => date("Y-m-d H:i:s")
		);	

		$m_testimoni->insert_data_testimoni('Tbl_Testimoni', $data);
		$this->session->set_flashdata('message_data', '<strong>Success </strong> Testimoni di tambahakan.');
		redirect($this->agent->referrer());
	}

	public function action_edit_isi_testimoni() {
		$m_testimoni = new Testimoni_model();

		$isi_testimoni = $this->input->post('isi_testimoni');
		$kode_testimoni = $this->input->post('kode_testimoni');

		$data = array(
			'Isi_Testimoni' => $isi_testimoni
		);
		$result = $m_testimoni->edit_data_testimoni('Tbl_Testimoni', $kode_testimoni, $data);
		if ($result > 0) {
			$this->session->set_flashdata('message_data', '<strong>Success </strong> Isi Testimoni di Ubah.');
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat update isi testimoni.');
			redirect($this->agent->referrer());
		}
	}

	public function action_edit_ubah_avatar() {
		$m_testimoni = new Testimoni_model();

		$kode_testimoni = $this->input->post('kode_testimoni');

		$data = $this->db->where('Kd_Testimoni', $kode_testimoni);
		$data = $this->db->get('Tbl_Testimoni')->row();
		$nama_gambar   = str_replace(" ","_", $data->Nama);

		if ($_FILES['avatar_testimoni']['error'] > 0) {

			$data = array(
				'Avatar' => $data->Avatar
			);

			$result = $m_testimoni->edit_data_testimoni('Tbl_Testimoni', $kode_testimoni, $data);
			if ($result > 0) {
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Avatar tidak boleh kosong.');
				redirect($this->agent->referrer());
			} 
		} else {

			$file_name = $_FILES['avatar_testimoni']['name'];
			$file_size = $_FILES['avatar_testimoni']['size'];
			$file_type = pathinfo($file_name, PATHINFO_EXTENSION);
			$new_file_name = $nama_gambar.'.'.$file_type;

			$config['upload_path'] = './storage_img/img_testimoni/';
			$config['allowed_types'] = '*';
			$config['file_name'] = $new_file_name;

			$this->load->library('upload', $config);

			$dir = './storage_img/img_testimoni/'.$data->Avatar;
			if (file_exists($dir)) {
				unlink($dir);
			}

			if (!is_dir('./storage_img/img_testimoni/')) {
				mkdir('./storage_img/img_testimoni/', 0777);
				if ( ! $this->upload->do_upload('avatar_testimoni')){
					$error = array('error' => $this->upload->display_errors());
					$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Testimoni gagal di tambahakan.');
					redirect($this->agent->referrer());
				}
				else{
					$data = array('upload_data' => $this->upload->data());
				}

			} else {
				if ( ! $this->upload->do_upload('avatar_testimoni')){
					$error = array('error' => $this->upload->display_errors());
					$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Testimoni gagal di tambahakan.');
					redirect($this->agent->referrer());
				}
				else{
					$data = array('upload_data' => $this->upload->data());
				}

			}

			$data = array(
				'Avatar' => $new_file_name
			);

			$result = $m_testimoni->edit_data_testimoni('Tbl_Testimoni', $kode_testimoni, $data);
			if ($result > 0) {
				$this->session->set_flashdata('message_data', '<strong>Success </strong> Avatar Testimoni di Ubah.');
				redirect($this->agent->referrer());
			} else {
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat update avatar testimoni.');
				redirect($this->agent->referrer());
			}

		}
	}

	public function action_edit_data_testimoni() {
		$m_testimoni = new Testimoni_model();

		$kode_testimoni = $this->input->post('kode_testimoni');
		$nama_testimoni = $this->input->post('nama_testimoni');
		$alamat_testimoni = $this->input->post('alamat_testimoni');

		$data = array(
			'Nama' => $nama_testimoni,
			'Alamat' => $alamat_testimoni
		);
		$result = $m_testimoni->edit_data_testimoni('Tbl_Testimoni', $kode_testimoni, $data);
		if ($result > 0) {
			$this->session->set_flashdata('message_data', '<strong>Success </strong> Data Testimoni di Ubah.');
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat update data testimoni.');
			redirect($this->agent->referrer());
		}
	}

	public function action_delete_data_testimoni($kode_testimoni) {
		$m_testimoni = new Testimoni_model();
		$result = $m_testimoni->delete_data_testimoni('Tbl_Testimoni', $kode_testimoni);
		if ($result > 0) {
			$this->session->set_flashdata('message_data', '<strong>Success </strong> Testimoni di hapus.');
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat hapus testimoni.');
			redirect($this->agent->referrer());
		}
	}


	/**
	 *
	 * backend engine portfolio
	 *
	 */
	
	public function action_save_portfolio() {
		$m_portfolio = new Portfolio_model();
		$m_berkas = new Berkas_model();
		$m_lib = new Lib_model();

		$nama_portfolio  = $this->input->post('nama');
		$alamat_portfolio = $this->input->post('alamat');

		$jum_data = count($_FILES['img_portfolio']['name']);
		$kode_portfolio = implode('-', str_split(substr(strtolower(md5(microtime().rand(1000, 9999))), 0, 12), 4));

		$Slug_Url = $m_lib->slug($nama_portfolio);

		if ($_FILES['img_portfolio']['error'][0] > 0) {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat tambah portfolio, foto tidak boleh kosong.');
			redirect($this->agent->referrer());
		} else {

			$new_name_produk = $m_lib->slug($nama_portfolio);

			$file_name_cover = $_FILES['img_portfolio']['name'];
			$file_size_cover = $_FILES['img_portfolio']['size'];
			$file_type_cover = pathinfo($file_name_cover, PATHINFO_EXTENSION);
			$new_file_name_cover_produk = 'cover_portfolio_'.str_replace('-','_', $m_lib->slug($nama_portfolio)).'.'.$file_type_cover;

			$config['upload_path'] = './storage_img/img_cover_portfolio/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size']  = '*';
			$config['file_name'] = $new_file_name_cover_produk;
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('img_portfolio')){
				$error = array('error' => $this->upload->display_errors());
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat upload cover portfolio.');
				redirect($this->agent->referrer());
			}
			else{
				$data = array('upload_data' => $this->upload->data());
			}

			$data = array(
				'Kd_Portfolio' => $kode_portfolio,
				'Cover_Img' => $new_file_name_cover_produk,
				'Nama_Portfolio' => $nama_portfolio,
				'Alamat_Portfolio' => $alamat_portfolio,
				'Slug_Url' => $Slug_Url,
				'Create_at' => date("Y-m-d H:i:s")
			);
			$m_portfolio->insert_data_portfolio('Tbl_Portfolio', $data);

			$this->session->set_flashdata('message_data', '<strong>Success </strong> Portfolio di tambahakan.');
			redirect($this->agent->referrer());
		}
	}

	public function action_edit_portfolio() {
		$m_portfolio = new Portfolio_model();
		$m_lib = new Lib_model();

		$nama_portfoliio = $this->input->post('nama');
		$alamat_portfolio = $this->input->post('alamat');
		$kode_portfolio = $this->input->post('kode_portfolio');

		$Slug_Url = $m_lib->slug($nama_portfoliio);
			
		$data = array(
			'Nama_Portfolio' => $nama_portfoliio,
			'Alamat_Portfolio' => $alamat_portfolio,
			'Slug_Url' => $Slug_Url
		);

		$result = $m_portfolio->update_data_portfolio('Tbl_Portfolio', $kode_portfolio, $data);
		if ($result > 0) {
			$this->session->set_flashdata('message_data', '<strong>Success </strong> Portfolio di ubah.');
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat ubah portfolio.');
			redirect($this->agent->referrer());
		}
	}

	public function action_delete_portfolio($kode_portfolio) {
		$m_portfolio = new Portfolio_model();
		$m_berkas = new Berkas_model();
		
		$result = $m_portfolio->check_data_portfolio('Tbl_Portfolio', $kode_portfolio);
		if ($result > 0) {
			$m_berkas->delete_data_berkas_portfolio('Tbl_Berkas', 'Kd_Portfolio', $kode_portfolio);
			$m_portfolio->delete_data_portfolio('Tbl_Portfolio', $kode_portfolio);
			$this->session->set_flashdata('message_data', '<strong>Success </strong> Portfolio di hapus.');
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat hapus portfolio.');
			redirect($this->agent->referrer());
		}
	}

	public function action_save_slide_portfolio() {
		$m_portfolio = new Portfolio_model();
		$m_berkas = new Berkas_model();
		$m_lib = new Lib_model();
		$kode_portfolio = $this->input->post('kode_portfolio');

		if ($_FILES['img_slide_portfolio']['error'][0] > 0) {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat tambah portfolio, foto tidak boleh kosong.');
			redirect($this->agent->referrer());
		} else {
			for ($i = 0; $i < count($_FILES['img_slide_portfolio']['name']) ; $i++) {
				$nama_img = $_FILES['img_slide_portfolio']['name'][$i];
				$tmp_name = $_FILES['img_slide_portfolio']['tmp_name'][$i];

				$new_file_name = "slide_portfolio_img_".str_replace(' ','_', $nama_img);

				$path = './storage_img/img_slide_portfolio/'.$new_file_name;
				move_uploaded_file($tmp_name, $path);

				$data = array(
					'Kd_Portfolio' => $kode_portfolio,
					'Nama_img' => $new_file_name,
					'Identitas' => 'Slide Portfolio'
				);
				$m_berkas->insert_data_berkas('Tbl_Berkas', $data);
			}
			$this->session->set_flashdata('message_data', '<strong>Success </strong> Portfolio di tambahakan.');
			redirect($this->agent->referrer());
		}
	}

	public function action_update_cover_portfolio() {
		$m_portfolio = new Portfolio_model();
		$m_lib = new Lib_model();

		$kode_portfolio = $this->input->post('kode_portfolio');

		if($_FILES['img_portfolio']['error'] > 0) {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat ubah cover portfolio, foto tidak boleh kosong.');
			redirect($this->agent->referrer());
		} else {

			$data = $this->db->where('Kd_Portfolio', $kode_portfolio);
			$data = $this->db->get('Tbl_Portfolio', 1)->row();

			$file_name_cover = $_FILES['img_portfolio']['name'];
			$file_size_cover = $_FILES['img_portfolio']['size'];
			$file_type_cover = pathinfo($file_name_cover, PATHINFO_EXTENSION);
			$new_file_name_cover_produk = 'cover_portfolio_'.str_replace('-','_', $m_lib->slug($data->Nama_Portfolio)).'.'.$file_type_cover;

			$dir = "./storage_img/img_cover_portfolio/".$data->Cover_Img;
			if (file_exists($dir)) {
				unlink($dir);
			} 

			echo $new_file_name_cover_produk;

			$config['upload_path'] = './storage_img/img_cover_portfolio/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size']  = '*';
			$config['file_name'] = $new_file_name_cover_produk;
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('img_portfolio')){
				$error = array('error' => $this->upload->display_errors());
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat upload cover portfolio.');
				redirect($this->agent->referrer());
			}
			else{
				$data = array('upload_data' => $this->upload->data());
			}

			$data = array(
				'Cover_Img' => $new_file_name_cover_produk
			);

			$m_portfolio->update_data_portfolio('Tbl_Portfolio', $kode_portfolio, $data);
			$this->session->set_flashdata('message_data', '<strong>Success </strong> Cover Portfolio di ubah.');
			redirect($this->agent->referrer());

		}

	}

	public function action_edit_slide_portfolio() {
		$m_portfolio = new Portfolio_model();
		$m_berkas = new Berkas_model();

		$kode_berkas = $this->input->post('kode_berkas');
		$kode_portfolio = $this->input->post('kode_portfolio');

		if ($_FILES['img_portfolio']['error'] > 0) {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat ubah slide portfolio, foto tidak boleh kosong.');
			redirect($this->agent->referrer());
		} else {

			$nama_img = $_FILES['img_portfolio']['name'];
			$tmp_name = $_FILES['img_portfolio']['tmp_name'];
			$new_file_name = "slide_portfolio_img_".str_replace(' ','_', $nama_img);

			$result = $this->db->where('Kd_Berkas', $kode_berkas);
			$result = $this->db->where('Kd_Portfolio', $kode_portfolio);
			$result = $this->db->where('Identitas', 'Slide Portfolio');
			$result = $this->db->get('Tbl_Berkas', 1)->row();

			$dir = './storage_img/img_slide_portfolio/'.$result->Nama_img;
			if (file_exists($dir)) {
				unlink($dir);
			}

			$config['upload_path'] = './storage_img/img_slide_portfolio';
			$config['allowed_types'] = '*';
			$config['max_size']  = '*';
			$config['file_name'] = $new_file_name;
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('img_portfolio')){
				$error = array('error' => $this->upload->display_errors());
			}
			else{
				$data = array('upload_data' => $this->upload->data());
			}

			$data = array( 'Nama_img' => $new_file_name);
			$result = $m_berkas->update_data_berkas('Tbl_Berkas', $kode_berkas, 'Kd_Portfolio', $kode_portfolio, $data);

			if ($result > 0) {
				$this->session->set_flashdata('message_data', '<strong>Success </strong> Slide berhasil di ubah.');
				redirect($this->agent->referrer());
			} else {
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat ubah slide portfolio.');
				redirect($this->agent->referrer());
			}
		}
	}

	public function action_delete_slide_portfolio($kode_berkas, $kode_portfolio) {
		$m_berkas = new Berkas_model();

		$result = $m_berkas->delete_data_berkas_single_img('Tbl_Berkas', 'img_slide_portfolio', $kode_berkas, 'Kd_Portfolio', $kode_portfolio);
		if ($result > 0) {
				$this->session->set_flashdata('message_data', '<strong>Success </strong> Slide portfolio berhasil di hapus.');
				redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat hapus slide portfolio');
			redirect($this->agent->referrer());
		}
	}


	/**
	 *
	 * backend engine produk
	 *
	 */
	
	public function action_save_produk() {
		$m_produk = new Produk_model();
		$m_lib = new Lib_model();
		
		$nama_produk = $this->input->post('nama_property');
		$deskripsi 	 = $this->input->post('deskripsi');
		
		$name_slug 		 = $m_lib->slug($nama_produk);
		$new_name_produk = str_replace(" ", "_", strtolower($nama_produk));

		$error_cover = $_FILES['img_cover']['error'];
		$error_logo  = $_FILES['img_logo']['error'];

		if (($error_cover > 0) || ($error_logo > 0)) {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat tambah produk, logo dan cover wajib di isi.');
			redirect($this->agent->referrer());
		} else {
			
			$file_name_cover = $_FILES['img_cover']['name'];
			$file_size_cover = $_FILES['img_cover']['size'];
			$file_type_cover = pathinfo($file_name_cover, PATHINFO_EXTENSION);
			$new_file_name_cover_produk = 'cover_'.$new_name_produk.'.'.$file_type_cover;

			$file_name_logo = $_FILES['img_logo']['name'];
			$file_size_logo = $_FILES['img_logo']['size'];
			$file_type_logo = pathinfo($file_name_logo, PATHINFO_EXTENSION);
			$new_file_name_logo_produk = 'logo_'.$new_name_produk.'.'.$file_type_logo;

			$dir_cover = "./storage_img/img_cover_produk/".$new_file_name_cover_produk;
			$dir_logo = "./storage_img/img_logo_produk/".$new_file_name_logo_produk;

			move_uploaded_file($_FILES['img_cover']['tmp_name'], $dir_cover);
			move_uploaded_file($_FILES['img_logo']['tmp_name'], $dir_logo);
			
			$data = array(
				'Kd_Produk' => implode('-', str_split(substr(strtolower(md5(microtime().rand(1000, 9999))), 0, 12), 4)),
				'Nama_Produk' => $nama_produk,
				'Deskripsi_1' => $deskripsi,
				'Slug_name' => $name_slug,
				'Logo_Produk' => $new_file_name_logo_produk,
				'Cover_Produk' => $new_file_name_cover_produk,
				'Status' => 1,
				'Create_at' => date("Y-m-d H:i:s")
			);

			$m_produk->insert_data_produk('Tbl_Produk', $data);
			$this->session->set_flashdata('message_data', '<strong>Success </strong> Produk property '.$nama_produk.' berhasil di tambahkan.');
			redirect($this->agent->referrer());

		}

	}
	
	public function action_edit_produk() {
		$m_lib = new Lib_model();
		$m_produk = new Produk_model();

		$nama_produk = $this->input->post('nama_property');
		$deskripsi = $this->input->post('deskripsi');
		$kode_produk = $this->input->post('kode_produk');
		
		$slug = $m_lib->slug($nama_produk);
		
		$data = array(
			'Nama_Produk' => $nama_produk,
			'Deskripsi_1' => $deskripsi,
			'Slug_name' => $slug
		);

		$result = $m_produk->update_data_produk('Tbl_Produk', $kode_produk, $data);
		if ($result > 0) {
			$this->session->set_flashdata('message_data', '<strong>Success </strong> Produk property '.$nama_produk.' berhasil di perbaharui.');
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat perbaharui produk');
			redirect($this->agent->referrer());
		}
	}

	public function action_edit_logo_produk() {
		$m_produk = new Produk_model();

		$kode_produk = $this->input->post('kode_produk');

		$get_data = $this->db->where('Kd_Produk', $kode_produk);
		$get_data = $this->db->get('Tbl_Produk')->row();
		
		$error_logo = $_FILES['img_logo']['error'];
		$new_name_produk = str_replace("-","_", $get_data->Slug_name);

		if($error_logo > 0) {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat perbaharui logo produk, gambar tidak boleh kosong');
			redirect($this->agent->referrer());
		} else {

			$file_name_logo = $_FILES['img_logo']['name'];
			$file_size_logo = $_FILES['img_logo']['size'];
			$file_type_logo = pathinfo($file_name_logo, PATHINFO_EXTENSION);
			$new_file_name_logo_produk = 'logo_'.$new_name_produk.'.'.$file_type_logo;

			$dir_logo = './storage_img/img_logo_produk/'.$get_data->Logo_Produk;
			if (file_exists($dir_logo)) {
				unlink($dir_logo);
			}

			$config['upload_path'] = './storage_img/img_logo_produk/';
			$config['allowed_types'] = '*';
			$config['file_name'] = $new_file_name_logo_produk;
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('img_logo')){
				$error = array('error' => $this->upload->display_errors());
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat perbaharui logo produk');
				redirect($this->agent->referrer());
			}
			else{
				$data = array('upload_data' => $this->upload->data());
			}

			$data = array(
				'Logo_Produk' => $new_file_name_logo_produk
			);

			$result = $m_produk->update_data_logo_produk('Tbl_Produk', $kode_produk, $data);
			if ($result > 0) {
				$this->session->set_flashdata('message_data', '<strong>Success </strong> Logo property '.$get_data->Nama_Produk.' berhasil di perbaharui.');
				redirect($this->agent->referrer());
			} else {
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat perbaharui logo produk');
				redirect($this->agent->referrer());
			}

		}
	}

	public function action_edit_cover_produk() {
		$m_produk = new Produk_model();

		$kode_produk = $this->input->post('kode_produk');

		$get_data = $this->db->where('Kd_Produk', $kode_produk);
		$get_data = $this->db->get('Tbl_Produk')->row();
		
		$error_logo = $_FILES['img_cover']['error'];
		$new_name_produk = str_replace("-","_", $get_data->Slug_name);

		if($error_logo > 0) {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat perbaharui cover produk, gambar tidak boleh kosong');
			redirect($this->agent->referrer());
		} else {

			$file_name_cover = $_FILES['img_cover']['name'];
			$file_size_cover = $_FILES['img_cover']['size'];
			$file_type_cover = pathinfo($file_name_cover, PATHINFO_EXTENSION);
			$new_file_name_cover_produk = 'logo_'.$new_name_produk.'.'.$file_type_cover;

			$dir_cover = './storage_img/img_cover_produk/'.$get_data->Cover_Produk;
			if (file_exists($dir_cover)) {
				unlink($dir_cover);
			}

			$config['upload_path'] = './storage_img/img_cover_produk/';
			$config['allowed_types'] = '*';
			$config['file_name'] = $new_file_name_cover_produk;
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('img_cover')){
				$error = array('error' => $this->upload->display_errors());
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat perbaharui cover produk');
				redirect($this->agent->referrer());
			}
			else{
				$data = array('upload_data' => $this->upload->data());
			}

			$data = array(
				'Cover_Produk' => $new_file_name_cover_produk
			);

			$result = $m_produk->update_data_logo_produk('Tbl_Produk', $kode_produk, $data);
			if ($result > 0) {
				$this->session->set_flashdata('message_data', '<strong>Success </strong> Cover property '.$get_data->Nama_Produk.' berhasil di perbaharui.');
				redirect($this->agent->referrer());
			} else {
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat perbaharui cover produk');
				redirect($this->agent->referrer());
			}

		}

	}

	public function action_delete_produk($kode_produk) {
		$m_produk = new Produk_model();

		$result = $m_produk->delete_data_produk('Tbl_Produk', $kode_produk);
		if ($result > 0) {
			$this->session->set_flashdata('message_data', '<strong>Success </strong> Produk property berhasil di hapus.');
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat hapus produk');
			redirect($this->agent->referrer());
		}
	}

	public function action_edit_data_status_produk() {
		$m_produk = new Produk_model();
		$kode_produk = $this->input->post('kode_produk');
		$status_produk = $this->input->post('status_produk');

		$data = array(
			'Status' => $status_produk
		);

		$nama_status = "";

		if ($status_produk > 0) {
			$nama_status = "Publish";
		} else {
			$nama_status = "Suspanse";
		}

		$result = $m_produk->update_data_produk('Tbl_Produk', $kode_produk, $data);
		if ($result > 0) {
			$this->session->set_flashdata('message_data', '<strong>Success </strong> Status Sekarang menjadi '.$nama_status.'.');
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat edit status menjadi '.$nama_status.'.');
			redirect($this->agent->referrer());
		}

	}

	public function upload_brosur_produk() {
		$m_produk = new Produk_model();

		$file_brosur = $this->input->post('file_brosur');
		$kode_produk = $this->input->post('kode_produk');

		$data = array(
			'Brosur_name' => $file_brosur
		);

		$result = $m_produk->update_data_produk('Tbl_Produk', $kode_produk, $data);
		if ($resutl > 0) {
			$this->session->set_flashdata('message_data', '<strong>Success </strong> Brosur berhasil di tambahkan');
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat menambahkan brosur');
			redirect($this->agent->referrer());
		}
	}

	/**
	 *
	 * Detail Produk
	 *
	 */
	
	public function action_save_detail_produk_slide() {
		$m_berkas = new Berkas_model();
		$kode_produk = $this->input->post('kode_produk');
		
		$error_slide = $_FILES['img_slide']['error'][0];
		if ($error_slide > 0) {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Gambar slide tidak boleh kosong');
			redirect($this->agent->referrer());
		} else{

			$get_data = $this->db->where('Kd_Produk', $kode_produk);
			$get_data = $this->db->get('Tbl_Produk')->row();

			$nama_file = $_FILES['img_slide']['name'];

			for ($i = 0; $i < count($_FILES['img_slide']['name']) ; $i++) {

				$file_name = $_FILES['img_slide']['name'][$i];
				$file_size = $_FILES['img_slide']['size'][$i];
				$file_type = pathinfo($file_name, PATHINFO_EXTENSION);
				
				$new_file_name = $file_name;
				
				$dir = './storage_img/img_slide_produk/'.$new_file_name;
				move_uploaded_file($_FILES['img_slide']['tmp_name'][$i], $dir);

				$data = array(
					'Kd_Produk' => $kode_produk,
					'Nama_img' => $new_file_name,
					'Identitas' => 'Slide Produk'
				);

				$m_berkas->insert_data_berkas('Tbl_Berkas', $data);
			}
			$this->session->set_flashdata('message_data', '<strong>Success </strong> Slide property berhasil di tambahkan.');
			redirect($this->agent->referrer());
		}
	}

	public function action_edit_detail_produk_slide() {
		$m_berkas = new Berkas_model();

		$kode_berkas = $this->input->post('kode_slide');
		$kode_produk = $this->input->post('kode_produk');

		$get_data = $this->db->where('Kd_Produk', $kode_produk);
		$get_data = $this->db->where('Kd_Berkas', $kode_berkas);
		$get_data = $this->db->get('Tbl_Berkas', 1)->row();
	
		$name_file = $_FILES['new_slide']['name'];
		
		$config['upload_path'] = './storage_img/img_slide_produk/';
		$config['allowed_types'] = '*';
		$config['max_size']  = '*';

		$dir = './storage_img/img_slide_produk/'.$get_data->Nama_img;
		if (file_exists($dir)) {
			unlink($dir);
		} 
		
		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload('new_slide')){
			$error = array('error' => $this->upload->display_errors());
		}
		else{
			$data = array('upload_data' => $this->upload->data());
		}

		$data = array(
			'Nama_img' => $name_file
		);

		$result = $m_berkas->update_data_berkas('Tbl_Berkas', $kode_berkas, 'Kd_Produk', $kode_produk, $data);
		if ($result > 0) {
			$this->session->set_flashdata('message_data', '<strong>Success </strong> Slide produk berhasil di ubah.');
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat edit slide produk');
			redirect($this->agent->referrer());
		}
	}

	public function action_delete_detail_produk_slide($kode_berkas, $kode_produk) {
		$m_berkas = new Berkas_model();

		$result = $m_berkas->delete_data_berkas('Tbl_Berkas', "Kd_Produk", $kode_berkas, $kode_produk);
		if ($result > 0) {
			$this->session->set_flashdata('message_data', '<strong>Success </strong> Slide berhasil di hapus.');
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat hapus slide');
			redirect($this->agent->referrer());
		}
	}

	/**
	 *
	 * Engine Deskripsi Lengkap Produk
	 *
	 */
	
	public function action_update_data_deskripsi_lengkap() {
		$m_produk = new Produk_model();

		$kode_produk = $this->input->post('kode_produk');
		$deskripsi_2 = $this->input->post('deskripsi_2');

		$data = array('Deskripsi_2' => $deskripsi_2);
		$result = $m_produk->update_data_produk('Tbl_Produk', $kode_produk, $data);
		if ($result > 0) {
			$this->session->set_flashdata('message_data', '<strong>Success </strong> Detail Deskripsi berhasil di ubah.');
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat menambahkan detail deskripsi');
			redirect($this->agent->referrer());
		}
	}
	

	/**
	 *
	 * Engine Spesifikasi Grid
	 *
	 */
	
	public function action_update_data_spesifikasi() {
		$m_produk = new Produk_model();

		$spesifikasi_grid_1 = $this->input->post('spesifikasi_grid_1');
		$spesifikasi_grid_2 = $this->input->post('spesifikasi_grid_2');
		$spesifikasi_grid_3 = $this->input->post('spesifikasi_grid_3');
		$kode_produk        = $this->input->post('kode_produk');

		$data = array(
			'Grid_1' => $spesifikasi_grid_1,
			'Grid_2' => $spesifikasi_grid_2,
			'Grid_3' => $spesifikasi_grid_3
		);

		$result = $m_produk->update_data_produk('Tbl_Produk', $kode_produk, $data);
		if ($result > 0) {
			$this->session->set_flashdata('message_data', '<strong>Success </strong> Spesifikasi berhasil di ubah.');
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat menambahkan spesifikasi');
			redirect($this->agent->referrer());
		}
	}

	/**
	 *
	 * Engine Fasilitas
	 *
	 */
	
	public function action_save_fasilitas() {
		$m_fasilitas = new Fasilitas_model();
		$m_lib = new Lib_model();

		$title = $this->input->post('title');
		$kode_produk = $this->input->post('kode_produk');
		$body_text = $this->input->post('body_text');
		$link_url = $this->input->post('link_url');
		$new_name_file = str_replace(" ", "_", strtolower($title));

		if ($_FILES['img_fasilitas']['error'] > 0) {
			// $this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Gambar fasilitas tidak boleh kosong');
			// redirect($this->agent->referrer());
			$data = array(
				'Kd_Fasilitas' => implode('-', str_split(substr(strtolower(md5(microtime().rand(1000, 9999))), 0, 8), 3)),
				'Kd_Produk' => $kode_produk,
				'Title' => $title,
				'Body_text' => $body_text,
				'link_url' => $link_url
			);

			$m_fasilitas->insert_data_fasilitas('Tbl_Fasilitas', $data);
			$this->session->set_flashdata('message_data', '<strong>Success </strong> Fasilitas berhasil di tambahkan.');
			redirect($this->agent->referrer());
		} else {

			$get_data = $this->db->where('Kd_Produk', $kode_produk);
			$get_data = $this->db->get('Tbl_Produk',1)->row();

			$file_name = $_FILES['img_fasilitas']['name'];
			$file_size = $_FILES['img_fasilitas']['size'];
			$file_type = pathinfo($file_name, PATHINFO_EXTENSION);
			$new_file_name = 'fasilitas_'.str_replace(" ", "_", strtolower($get_data->Nama_Produk)).'_'.$new_name_file.'.'.$file_type;

			$config['upload_path'] = './storage_img/img_fasilitas/';
			$config['allowed_types'] = '*';
			$config['max_size']  = '*';
			$config['file_name'] = $new_file_name;
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('img_fasilitas')){
				$error = array('error' => $this->upload->display_errors());
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat upload gambar fasilitas');
				redirect($this->agent->referrer());
			}
			else{
				$data = array('upload_data' => $this->upload->data());
			}

			$data = array(
				'Kd_Fasilitas' => implode('-', str_split(substr(strtolower(md5(microtime().rand(1000, 9999))), 0, 8), 3)),
				'Kd_Produk' => $kode_produk,
				'Title' => $title,
				'Body_text' => $body_text,
				'link_url' => $link_url,
				'Nama_img' => $new_file_name
			);

			$m_fasilitas->insert_data_fasilitas('Tbl_Fasilitas', $data);
			$this->session->set_flashdata('message_data', '<strong>Success </strong> Fasilitas berhasil di tambahkan.');
			redirect($this->agent->referrer());

		}
	}

	public function action_update_data_fasilitas() {
		$m_fasilitas = new Fasilitas_model();
		$m_lib = new Lib_model();
		$title 		 = $this->input->post('title');
		$kode_produk = $this->input->post('kode_produk');
		$kode_fasilitas = $this->input->post('kode_fasilitas');
		$link_url = $this->input->post('link_url');
		$body_text 	 = $this->input->post('body_text');
		$old_img_fasilitas = $this->input->post('old_img_fasilitas');

		$new_name_file = str_replace(" ", "_", strtolower($title));

		if ($_FILES['new_img_fasilitas']['error'] > 0) {
			$data = array(
				'Title' => $title,
				'Body_text' => $body_text,
				'Link_Url' => $link_url
			);
			$m_fasilitas->update_data_fasilitas('Tbl_Fasilitas', $kode_produk, $kode_fasilitas, $data);
			
			$this->session->set_flashdata('message_data', '<strong>Success </strong> Fasilitas berhasil di ubah.');
			redirect($this->agent->referrer());
		} else {

			$get_data = $this->db->where('Kd_Produk', $kode_produk);
			$get_data = $this->db->get('Tbl_Produk',1)->row();

			$file_name = $_FILES['new_img_fasilitas']['name'];
			$file_size = $_FILES['new_img_fasilitas']['size'];
			$file_type = pathinfo($file_name, PATHINFO_EXTENSION);
			$new_file_name = 'fasilitas_'.str_replace(" ", "_", strtolower($get_data->Nama_Produk)).'_'.$new_name_file.'.'.$file_type;

			$dir = './storage_img/img_fasilitas/'.$old_img_fasilitas;
			if (file_exists($dir)) {
				unlink($dir);
			}

			$config['upload_path'] = './storage_img/img_fasilitas/';
			$config['allowed_types'] = '*';
			$config['max_size']  = '*';
			$config['file_name'] = $new_file_name;
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('new_img_fasilitas')){
				$error = array('error' => $this->upload->display_errors());
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat upload gambar fasilitas');
				redirect($this->agent->referrer());
			}
			else{
				$data = array('upload_data' => $this->upload->data());
			}

			$data = array(
				'Title' => $title,
				'Body_text' => $body_text,
				'Link_Url' => $link_url,
				'Nama_img' => $new_file_name
			);

			$m_fasilitas->update_data_fasilitas('Tbl_Fasilitas', $kode_produk, $kode_fasilitas, $data);
			$this->session->set_flashdata('message_data', '<strong>Success </strong> Fasilitas berhasil di ubah.');
			redirect($this->agent->referrer());
		}
	}


	public function action_delete_fasilitas($kode_fasilitas, $kode_produk) {
		$m_fasilitas = new Fasilitas_model();
		$result = $m_fasilitas->delete_data_fasilitas('Tbl_Fasilitas', $kode_fasilitas, $kode_produk);
		if ($result > 0) {
			$this->session->set_flashdata('message_data', '<strong>Success </strong> Fasilitas berhasil di hapus.');
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat hapus fasilitas');
			redirect($this->agent->referrer());
		}
	}
	
	/**
	 *
	 * Engine Property Produk
	 *
	 */
	
	public function action_save_property() {
		$m_property = new Property_model();
		$m_produk = new Produk_model();
		$m_lib = new Lib_model();

		$nama_produk = $this->input->post('nama_produk');
		$kode_produk    = $this->input->post('kode_produk');
		$nama_property  = $this->input->post('nama_property');
		$luas_tanah 	= $this->input->post('luas_tanah');
		$kategori_rumah = $this->input->post('kategori_rumah');
		$informasi_bagunan = $this->input->post('informasi_bagunan');
		$deskripsi = $this->input->post('deskripsi');

		// get data produk
		$get_data = $this->db->select('Kd_Produk, Slug_name, Nama_Produk');
		$get_data = $this->db->where('Kd_Produk', $kode_produk);
		$get_data = $this->db->get('Tbl_Produk',1)->row();

		$new_name_file = str_replace(" ","_", strtolower($nama_property));
		$slug_propery = $get_data->Slug_name.'-'.$m_lib->slug($nama_property);

		if ($_FILES['img_property']['error'] > 0) {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan cover tidak boleh kosong');
			redirect($this->agent->referrer());
		} else {

			$file_name = $_FILES['img_property']['name'];
			$file_size = $_FILES['img_property']['size'];
			$file_type = pathinfo($file_name, PATHINFO_EXTENSION);
			$new_file_name = 'cover_property_'.str_replace(" ", "_", strtolower($get_data->Nama_Produk)).'_'.$new_name_file.'.'.$file_type;

			$config['upload_path'] = './storage_img/img_cover_property/';
			$config['allowed_types'] = '*';
			$config['max_size']  = '*';
			$config['file_name'] =$new_file_name;
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('img_property')){
				$error = array('error' => $this->upload->display_errors());
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat menambahkan cover');
				redirect($this->agent->referrer());
			}
			else{
				$data = array('upload_data' => $this->upload->data());
			}

			$data = array(
				'Kd_Property' => implode('-', str_split(substr(strtolower(md5(microtime().rand(1000, 9999))), 0, 14), 2)),
				'Kd_Produk' => $kode_produk,
				'Nama_Produk' => $get_data->Nama_Produk,
				'Nama_Property' => $nama_property,
				'Slug_Property' => $slug_propery,
				'Kategori' => $kategori_rumah,
				'Luas_Tanah' => $luas_tanah,
				'Info_Bangunan' => $informasi_bagunan,
				'Deskripsi' => $deskripsi,
				'Cover_Name' => $new_file_name,
				'Create_at' => date("Y-m-d H:i:s")
			);

			$m_property->insert_data_property('Tbl_Property', $data);
			$this->session->set_flashdata('message_data', '<strong>Success </strong> Property berhasil di tambahkan.');
			// redirect($this->agent->referrer());
			redirect('admin/new-property','refresh');

		}

	}

	public function action_edit_cover_property() {
		$m_lib = new Lib_model();
		$m_property = new Property_model();

		$kode_produk = $this->input->post('kode_produk');
		$kode_property = $this->input->post('kode_property');

		// get data property
		$get_data = $this->db->where('Kd_Produk', $kode_produk);
		$get_data = $this->db->where('Kd_Property', $kode_property);
		$get_data = $this->db->get('Tbl_Property',1)->row();

		$new_name_file = str_replace(" ","_", strtolower($get_data->Nama_Property));

		if ($_FILES['new_img_cover_property']['error'] > 0) {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan cover tidak boleh kosong');
			redirect($this->agent->referrer());
		} else {

			$file_name = $_FILES['new_img_cover_property']['name'];
			$file_size = $_FILES['new_img_cover_property']['size'];
			$file_type = pathinfo($file_name, PATHINFO_EXTENSION);
			$new_file_name = 'cover_property_'.str_replace(" ", "_", strtolower($get_data->Nama_Produk)).'_'.$new_name_file.'.'.$file_type;

			$config['upload_path'] = './storage_img/img_cover_property/';
			$config['allowed_types'] = '*';
			$config['max_size']  = '*';
			$config['file_name'] =$new_file_name;

			$dir = './storage_img/img_cover_property/'.$get_data->Cover_Name;
			if (file_exists($dir)) {
				unlink($dir);
			}
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('new_img_cover_property')){
				$error = array('error' => $this->upload->display_errors());
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat mengubah cover');
				redirect($this->agent->referrer());
			}
			else{
				$data = array('upload_data' => $this->upload->data());
			}

			$data = array(
				'Cover_Name' => $new_file_name,
				'Update_at' => date("Y-m-d H:i:s")
			);

			$m_property->update_data_property('Tbl_Property', $kode_property, $kode_produk, $data);
			$this->session->set_flashdata('message_data', '<strong>Success </strong> Cover Property berhasil di Ubah.');
			redirect($this->agent->referrer());

		}
	}
	
	public function action_tambah_slide_property() {
		$m_lib = new Lib_model();
		$m_berkas = new Berkas_model();

		$kode_property = $this->input->post('kode_property');

		if ($_FILES['img_slide_property']['error'][0] > 0) {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan slide tidak boleh kosong');
			redirect($this->agent->referrer());
		} else {

			for ($i = 0; $i < count($_FILES['img_slide_property']['name']) ; $i++) {

				$new_name_file = $m_lib->slug($_FILES['img_slide_property']['name'][$i]);

				$file_name = $_FILES['img_slide_property']['name'][$i];
				$file_size = $_FILES['img_slide_property']['size'][$i];
				$file_type = pathinfo($file_name, PATHINFO_EXTENSION);

				$new_file_name = 'slide_property_'.str_replace('-','_',$kode_property).'_'.str_replace('-','_', $m_lib->slug($file_name)).'.'.$file_type;

				$dir = './storage_img/img_slide_property/'.$new_file_name;

				move_uploaded_file($_FILES['img_slide_property']['tmp_name'][$i], $dir);

				$data = array(
					'Kd_Property' => $kode_property,
					'Nama_img' => $new_file_name,
					'Identitas' => 'Slide Property'
				);

				$m_berkas->insert_data_berkas('Tbl_Berkas', $data);
			}

			$this->session->set_flashdata('message_data', '<strong>Success </strong> Slide Property berhasil di Tambahkan.');
			redirect($this->agent->referrer());

		}
	}

	public function action_edit_slide_property() {
		$m_lib = new Lib_model();
		$m_berkas = new Berkas_model();

		$kode_berkas = $this->input->post('kode_berkas');
		$kode_property = $this->input->post('kode_property');

		if ($_FILES['img_slide_property']['error'] > 0) {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan slide tidak boleh kosong');
			redirect($this->agent->referrer());
		} else {

			$new_name_file = $m_lib->slug($_FILES['img_slide_property']['name']);
			$file_name = $_FILES['img_slide_property']['name'];
			$file_size = $_FILES['img_slide_property']['size'];
			$file_type = pathinfo($file_name, PATHINFO_EXTENSION);
			$new_file_name = 'slide_property_'.str_replace('-','_',$kode_property).'_'.str_replace('-','_', $m_lib->slug($file_name)).'.'.$file_type;

			$result = $this->db->where('Kd_Berkas', $kode_berkas);
			$result = $this->db->where('Kd_Property', $kode_property);
			$result = $this->db->get('Tbl_Berkas', 1)->row();

			$dir = './storage_img/img_slide_property/'.$result->Nama_img;

			if (file_exists($dir)) {
				unlink($dir);
			}

			$config['upload_path'] = './storage_img/img_slide_property/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size']  = '*';
			$config['file_name'] = $new_file_name;
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('img_slide_property')){
				$error = array('error' => $this->upload->display_errors());
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan upload slide cek ukuran dan size slide');
				redirect($this->agent->referrer());
			}
			else{
				$data = array('upload_data' => $this->upload->data());
			}

			$data = array(
				'Nama_img' => $new_file_name
			);
			$result = $m_berkas->update_data_berkas("Tbl_Berkas", $kode_berkas, 'Kd_Property', $kode_property, $data);

			if ($result > 0) {
				$this->session->set_flashdata('message_data', '<strong>Success </strong> Slide berhasil di ubah.');
				redirect($this->agent->referrer());
			} else {
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan slide tidak berhasil di ubah');
				redirect($this->agent->referrer());
			}

		}	
	}

	public function action_delete_slide_property($kode_berkas, $kode_property) {
		$m_berkas = new Berkas_model();

		$result = $m_berkas->delete_data_berkas_single_img('Tbl_Berkas', 'img_slide_property', $kode_berkas, 'Kd_Property', $kode_property);
		if ($result > 0) {
			$this->session->set_flashdata('message_data', '<strong>Success </strong> Slide berhasil di hapus.');
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat hapus slide property');
			redirect($this->agent->referrer());
		}
	}
	
	public function action_update_data_property() {
		$m_property = new Property_model();
		$m_lib = new Lib_model();

		$nama_produk = $this->input->post('nama_produk');
		$kode_produk = $this->input->post('kode_produk');
		$kode_property = $this->input->post('kode_property');
		$nama_property = $this->input->post('nama_property');
		$luas_tanah = $this->input->post('luas_tanah');
		$kategori_rumah = $this->input->post('kategori_rumah');
		$informasi_bagunan = $this->input->post('informasi_bagunan');
		$deskripsi = $this->input->post('deskripsi');

		// get data produk
		$get_data = $this->db->select('Kd_Produk, Slug_name, Nama_Produk');
		$get_data = $this->db->where('Kd_Produk', $kode_produk);
		$get_data = $this->db->get('Tbl_Produk',1)->row();

		$new_name_file = str_replace(" ","_", strtolower($nama_property));
		$slug_propery = $get_data->Slug_name.'-'.$m_lib->slug($nama_property);

		$data = array(
				'Nama_Produk' => $get_data->Nama_Produk,
				'Nama_Property' => $nama_property,
				'Slug_Property' => $slug_propery,
				'Kategori' => $kategori_rumah,
				'Luas_Tanah' => $luas_tanah,
				'Info_Bangunan' => $informasi_bagunan,
				'Deskripsi' => $deskripsi,
				'Update_at' => date("Y-m-d H:i:s")
			);

		$result = $m_property->update_data_property('Tbl_Property', $kode_property, $kode_produk, $data);
		if ($result > 0) {
			$this->session->set_flashdata('message_data', '<strong>Success </strong> Type Property berhasil di ubah.');
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat update property');
			redirect($this->agent->referrer());
		}
	}

	public function action_delete_property($kode_property, $kode_produk) {
		$m_property = new Property_model();
		$m_berkas = new Berkas_model();

		$result_propery = $m_property->delete_data_property('Tbl_Property', $kode_property);
		
		if ($result_propery > 0) {
				$m_berkas->delete_data_berkas_property('Tbl_Berkas', 'Kd_Property', $kode_property);
				$this->session->set_flashdata('message_data', '<strong>Success </strong> Property Dan Berkas berhasil di hapus.');
				redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat hapus property');
			redirect($this->agent->referrer());
		}
	}


	/**
	 *
	 * Engine Album
	 *
	 */
	
	public function action_save_album() {
		$m_lib = new Lib_model();
		$m_gallery = new Gallery_model();

		$nama_album = $this->input->post('nama_album');
		$body_nama_album = $this->input->post('body_nama_album');
		$kategori = $this->input->post('kategori');
		
		if ($_FILES['img_cover_gallery']['error'] > 0) {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Cover Gallery harus di lengkapi');
			redirect($this->agent->referrer());
		} else {

			$new_name = $m_lib->slug($nama_album);

			$file_name = $_FILES['img_cover_gallery']['name'];
			$file_size = $_FILES['img_cover_gallery']['size'];
			$file_type = pathinfo($file_name, PATHINFO_EXTENSION);
			$new_file_name = 'cover_album_'.str_replace('-','_', $new_name).'_'.date('Y').'.'.$file_type;

			$config['upload_path'] = './storage_img/img_cover_gallery/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size']  = '*';
			$config['file_name'] = $new_file_name;
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('img_cover_gallery')){
				$error = array('error' => $this->upload->display_errors());
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat upload cover gallery');
				redirect($this->agent->referrer());
			}
			else{
				$data = array('upload_data' => $this->upload->data());
			}

			$data = array(
				'Kd_Gallery' => implode('-', str_split(substr(strtolower(md5(microtime().rand(1000, 9999))), 0, 16), 4)),
				'Nama_Gallery' => $nama_album,
				'Slug_Url' => $new_name,
				'Body_Nama_Gallery' => $body_nama_album,
				'Kategori' => $kategori,
				'Cover_Name' => $new_file_name, 
				'Create_At' => date("Y-m-d H:i:s")
			);

			$m_gallery->insert_data_gallery('Tbl_Gallery', $data);
			$this->session->set_flashdata('message_data', '<strong>Success </strong> Album berhasil di buat.');
			redirect($this->agent->referrer());

		}
	}

	public function action_update_data_gallery() {
		$m_gallery = new Gallery_model();
		$m_lib = new Lib_model();

		$nama_album = $this->input->post('nama_album');
		$body_nama_album = $this->input->post('body_nama_album');
		$kode_gallery = $this->input->post('kode_gallery');
		$kategori = $this->input->post('kategori');
		$old_img_gallery = $this->input->post('old_img_gallery');

		if ($_FILES['img_cover_gallery']['error'] > 0) {

			$new_name = $m_lib->slug($nama_album);

			$data = array(
				'Nama_Gallery' => $nama_album,
				'Body_Nama_Gallery' => $body_nama_album,
				'Kategori' => $kategori,
				'Slug_Url' => $new_name,
				'Cover_Name' => $old_img_gallery
			);

			$m_gallery->update_data_gallery('Tbl_Gallery', $kode_gallery, $data);
			$this->session->set_flashdata('message_data', '<strong>Success </strong> Album berhasil di perbaharui.');
			redirect($this->agent->referrer());

		} else {

			$new_name = $m_lib->slug($nama_album);

			$file_name = $_FILES['img_cover_gallery']['name'];
			$file_size = $_FILES['img_cover_gallery']['size'];
			$file_type = pathinfo($file_name, PATHINFO_EXTENSION);
			$new_file_name = 'cover_album_'.str_replace('-','_', $new_name).'_'.date('Y').'.'.$file_type;

			$dir = './storage_img/img_cover_gallery/'.$old_img_gallery;
			if (file_exists($dir)) {
				unlink($dir);
			}

			$config['upload_path'] = './storage_img/img_cover_gallery/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size']  = '*';
			$config['file_name'] = $new_file_name;
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('img_cover_gallery')){
				$error = array('error' => $this->upload->display_errors());
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat upload cover gallery');
				redirect($this->agent->referrer());
			}
			else{
				$data = array('upload_data' => $this->upload->data());
			}

			$data = array(
				'Nama_Gallery' => $nama_album,
				'Slug_Url' => $new_name,
				'Body_Nama_Gallery' => $body_nama_album,
				'Kategori' => $kategori,
				'Cover_Name' => $new_file_name
			);

			$m_gallery->update_data_gallery('Tbl_Gallery', $kode_gallery, $data);
			$this->session->set_flashdata('message_data', '<strong>Success </strong> Album berhasil di perbaharui.');
			redirect($this->agent->referrer());

		}
	}
	
	public function action_delete_gallery($kode_gallery) {
		$m_gallery = new Gallery_model();
		$m_berkas = new Berkas_model();

		$result = $m_gallery->check_data_gallery('Tbl_Gallery', $kode_gallery);
		if ($result > 0) {
			$m_gallery->delete_data_gallery('Tbl_Gallery', $kode_gallery);
			$m_berkas->delete_data_berkas_album('Tbl_Berkas', 'Kd_Gallery', $kode_gallery);
			$this->session->set_flashdata('message_data', '<strong>Success </strong> Album berhasil di hapus.');
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat hapus Album');
			redirect($this->agent->referrer());
		}
	}

	public function action_save_create_album() {
		$m_lib = new Lib_model();
		$m_gallery = new Gallery_model();
		$m_berkas = new Berkas_model();
		$kode_gallery = $this->input->post('kode_gallery');
		$result = $this->db->where('Kd_Gallery', $kode_gallery);
		$result = $this->db->get('Tbl_Gallery', 1)->row();

		if ($_FILES['img_album']['error'][0] > 0) {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat upload Album');
			redirect($this->agent->referrer());
		} else {

			for ($i = 0; $i < count($_FILES['img_album']['name']); $i++) {

				$file_name = $_FILES['img_album']['name'][$i];
				$file_size = $_FILES['img_album']['size'][$i];
				$file_type = pathinfo($file_name, PATHINFO_EXTENSION);
				$new_file_name = 'album_'.implode('_', str_split(substr(strtolower(md5(microtime().rand(1000, 9999))), 0, 2), 2)).'_'.str_replace('-','_', $result->Slug_Url).'_'.str_replace('-','_', $m_lib->slug($file_name)).'_'.date('Y').'.'.$file_type;

				$dir = './storage_img/img_album/'.$new_file_name;
				move_uploaded_file($_FILES['img_album']['tmp_name'][$i], $dir);
				$data = array(
					'Nama_img' => $new_file_name,
					'Kd_Gallery' => $kode_gallery,
					'Identitas' => 'Gallery'
				);
				$m_berkas->insert_data_berkas('Tbl_Berkas', $data);
			}

			$this->session->set_flashdata('message_data', '<strong>Success </strong> Foto Album berhasil di tambahkan.');
			redirect($this->agent->referrer());
		}
	}

	public function action_update_foto_gallery() {
		$m_berkas = new Berkas_model();
		$m_lib = new Lib_model();
		$kode_berkas = $this->input->post('kode_berkas');
		$kode_gallery = $this->input->post('kode_gallery');

		if ($_FILES['img_new_galleri']['error'] > 0) {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat upload gambar');
			redirect($this->agent->referrer());

		} else {

			$data = $this->db->where('Kd_Gallery', $kode_gallery);
			$data = $this->db->get('Tbl_Gallery', 1)->row();

			$data_berkas = $this->db->where('Kd_Berkas', $kode_berkas);
			$data_berkas = $this->db->where('Kd_Gallery', $kode_gallery);
			$data_berkas = $this->db->get('Tbl_Berkas', 1)->row();

			$dir = './storage_img/img_album/'.$data_berkas->Nama_img;
			if (file_exists($dir)) {
				unlink($dir);
			}

			$file_name = $_FILES['img_new_galleri']['name'];
			$file_size = $_FILES['img_new_galleri']['size'];
			$file_type = pathinfo($file_name, PATHINFO_EXTENSION);
			$new_file_name = 'album_'.implode('_', str_split(substr(strtolower(md5(microtime().rand(1000, 9999))), 0, 2), 2)).'_'.$data->Slug_Url.'_'.str_replace('-','_', $m_lib->slug($file_name)).'_'.date('Y').'.'.$file_type;

			$config['upload_path'] = './storage_img/img_album/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size']  = '*';
			$config['file_name'] = $new_file_name;
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('img_new_galleri')){
				$error = array('error' => $this->upload->display_errors());
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat upload gambar gallery');
				redirect($this->agent->referrer());
			}
			else{
				$data = array('upload_data' => $this->upload->data());
			}

			$data = array('Nama_img' => $new_file_name);

			$m_berkas->update_data_berkas('Tbl_Berkas', $kode_berkas, 'Kd_Gallery', $kode_gallery, $data);
			$this->session->set_flashdata('message_data', '<strong>Success </strong> Gambar berhasil diubah.');
			redirect($this->agent->referrer());

		}
	}

	public function action_delete_foto_gallery($kode_berkas, $kode_gallery) {
		$m_berkas = new Berkas_model();

		$result = $m_berkas->delete_data_berkas_single_img('Tbl_Berkas', 'img_album', $kode_berkas, 'Kd_Gallery', $kode_gallery);
		if ($result > 0) {
			$this->session->set_flashdata('message_data', '<strong>Success </strong> Foto Album berhasil di hapus.');
			redirect($this->agent->referrer());

		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat hapus gambar gallery');
			redirect($this->agent->referrer());
		}
	}



}

/* End of file Admin.php */
/* Location: ./application/controllers/Admin.php */