<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Front extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		
		$this->load->library('user_agent');
		date_default_timezone_set('Asia/Jakarta');

		// load model
		$this->load->model('Pesan_model');
		$this->load->model('Tentang_model');
		$this->load->model('Testimoni_model');
		$this->load->model('Produk_model');
		$this->load->model('Property_model');
		$this->load->model('Portfolio_model');
		$this->load->model('Fasilitas_model');
		$this->load->model('Divisi_model');
		$this->load->model('Berkas_model');
		$this->load->model('Gallery_model');
		$this->load->model('Gallery_proyek_model');
		$this->load->model('Slide_model');
	}

	public function index()
	{
		$m_testimoni = new Testimoni_model();
		$m_produk = new Produk_model();
		$m_property = new Property_model();

		$data['data_testimoni'] = $m_testimoni->view_data_testimoni('Tbl_Testimoni')->result();
		$data['data_produk'] = $m_produk->view_data_produk_by_status('Tbl_Produk')->result();
		$data['data_property'] = $m_property->view_data_property('Tbl_Property')->result();

		$this->load->view('frontpages/page/home_default_view', $data);
	}

	public function about_view() {
		$m_tentang = new Tentang_model();
		$m_testimoni = new Testimoni_model();
		$m_divisi = new Divisi_model();

		$data['data_tentang'] = $m_tentang->view_data_tentang('Tbl_Tentang');
		$data['data_testimoni'] = $m_testimoni->view_data_testimoni('Tbl_Testimoni')->result();
		$data['data_divisi'] = $m_divisi->view_data_divisi('Tbl_Divisi')->result();
		$this->load->view('frontpages/page/about_view', $data);
	}

	public function detail_produk_view($slug_name) {
		$m_produk = new Produk_model();
		$m_property = new Property_model();
		$m_fasilitas = new Fasilitas_model();
		$m_berkas = new Berkas_model();
		$m_g_proyek = new Gallery_proyek_model();

		$result = $m_produk->check_data_status_by_slug('Tbl_Produk', $slug_name);
		if ($result > 0) {

			$data['data_produk'] = $m_produk->view_data_produk_by_slug('Tbl_Produk', $slug_name);
			$data['data_berkas_slide'] = $m_berkas->view_data_berkas('Tbl_Berkas','Kd_Produk' ,$data['data_produk']->Kd_Produk)->result();
			$data['data_property'] = $m_property->view_data_property_by_kode_produk('Tbl_Property', $data['data_produk']->Kd_Produk)->result();
			$data['total_property'] = $m_property->count_data_property_by_kode('Tbl_Property', $data['data_produk']->Kd_Produk);
			$data['data_fasilitas'] = $m_fasilitas->view_fasilitas_by_kode_produk('Tbl_Fasilitas', $data['data_produk']->Kd_Produk)->result();
			$data['data_gallery_proyek'] = $m_g_proyek->view_data_g_proyek_by_kode_proyek('Tbl_Gproyek', $data['data_produk']->Kd_Produk)->result();
			$this->load->view('frontpages/page/detail_produk_view', $data);
		} else {
			redirect('/','refresh');
		}
	}

	public function kontak_view() {
		$this->load->view('frontpages/page/kontak_view');
	}

	public function portfolio_view() {
		$m_portfolio = new Portfolio_model();

		$data['data_portfolio'] = $m_portfolio->view_data_portfolio('Tbl_Portfolio')->result();
		$this->load->view('frontpages/page/portfolio_view', $data);
	}

	public function galeri_view() {
		$m_gallery = new Gallery_model();

		$data['group_name'] = $m_gallery->view_groub_kategori_gallery('Tbl_Gallery')->result();
 		$data['data_gallery'] = $m_gallery->view_data_gallery('Tbl_Gallery')->result();
		$this->load->view('frontpages/page/galeri_view', $data);
	}

	public function view_detail_portfolio($Slug_Url) {
		$m_portfolio = new Portfolio_model();
		$m_berkas = new Berkas_model();

		$data['data_portfolio'] = $m_portfolio->view_data_by_slug('Tbl_Portfolio', $Slug_Url);
		$data['data_berkas_portfolio'] = $m_berkas->view_data_berkas('Tbl_Berkas', 'Kd_Portfolio', $data['data_portfolio']->Kd_Portfolio)->result();
		$data['data_portfolio_lainnya'] = $m_portfolio->view_data_portfolio_lainnya_by_slug('Tbl_Portfolio', $Slug_Url)->result();
		
		$this->load->view('frontpages/page/detail_portfolio_view', $data);
	}

	public function detail_album_view($slug_gallery) {
		$m_gallery = new Gallery_model();
		$m_berkas = new Berkas_model();

		$result = $m_gallery->check_data_gallery_by_slug('Tbl_Gallery', $slug_gallery);
		if ($result > 0) {
			$data['data_gallery'] = $m_gallery->view_data_gallery_by_slug('Tbl_Gallery', $slug_gallery)->row();
			$data['data_berkas_album'] = $m_berkas->view_data_berkas('Tbl_Berkas', 'Kd_Gallery', $data['data_gallery']->Kd_Gallery)->result();
			$this->load->view('frontpages/page/detail_album_view.php', $data);
		} else {
			redirect('/','refresh');
		}
	}

	public function detail_tlogoadi_view() {
		$this->load->view('frontpages/page/single_tlogoadi_view');
	}

	public function detail_villa_gardenia_view() {
		$this->load->view('frontpages/page/single_villa_gardenia_view');
	}

	public function view_data_slide_json() {
		$m_slide = new Slide_model();
		$data_slide = $m_slide->view_data_slide('Tbl_Slide')->result();

		$data = array();
		foreach ($data_slide as $key => $dt_slide) {
			$data[] = array('src' => base_url('storage_img/img_slide/'.$dt_slide->Cover_name));
		}
		$dt = json_encode($data);
		print_r($dt);
		return $dt;
	}

	public function shop_view() {
		$m_property = new Property_model();
		$m_produk = new Produk_model();

		$data['data_property'] = $m_property->view_data_property('Tbl_Property')->result();
		$data['data_produk'] = $m_produk->view_data_produk('Tbl_Produk')->result();
		$data['group_name'] = $m_property->view_data_group_by_nama_produk('Tbl_Property')->result();
		$this->load->view('frontpages/page/shop_view', $data);
	}

	public function detail_rumah_view($slug_name) {
		$m_property = new Property_model();
		$m_berkas = new Berkas_model();

		$data['data_property'] = $m_property->view_data_property_by_slug('Tbl_Property', $slug_name);
		$data['data_property_lainnya'] = $m_property->view_data_property_lainnya_by_slug('Tbl_Property', $slug_name)->result();
		$data['data_berkas'] = $m_berkas->view_data_berkas('Tbl_Berkas', 'Kd_Property', $data['data_property']->Kd_Property)->result();

		$this->load->view('frontpages/page/detail_rumah_view', $data);
	}

	public function not_found() {
		$this->load->view('frontpages/page/404_view');
	}

	public function download_brosur() {
		$m_produk = new Produk_model();

		$data['data_produk'] = $m_produk->view_data_produk('Tbl_Produk')->result();
		$this->load->view('frontpages/page/download_brosur_view', $data);
	}

	public function action_pesan() {

		$m_pesan = new Pesan_model();

		$nama 	= $this->input->post('name');
		$email 	= $this->input->post('email');
		$nomor_telepon = $this->input->post('nomor_telepon');
		$pesan 	= $this->input->post('message');

		$data = array(
			'Kd_Pesan' => implode('-', str_split(substr(strtolower(md5(microtime().rand(1000, 9999))), 0, 12), 4)),
			'Nama' => $nama,
			'Email' => $email,
			'No_Tlpn' => $nomor_telepon,
			'Pesan' => $pesan,
			'Status' => '0',
			'Create_at' => date("Y-m-d H:i:s")
		);

		$m_pesan->insert_data_pesan('Tbl_Pesan', $data);
		$this->session->set_flashdata('message_data', '<strong>Pesan Berhasil Terkirim </strong>.');
		redirect($this->agent->referrer());
	}

}

/* End of file Front.php */
/* Location: ./application/controllers/Front.php */