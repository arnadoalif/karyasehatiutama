<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fasilitas_model extends CI_Model {

	public function view_fasilitas_by_kode_produk($table_name, $kode_produk) {
		$this->db->select('*');
		$this->db->where('Kd_Produk', $kode_produk);
		return $this->db->get($table_name);
	}

	public function insert_data_fasilitas($table_name, $data) {
		$this->db->insert($table_name, $data);
	} 

	public function update_data_fasilitas($table_name, $kode_produk, $kode_fasilitas, $data) {
		$result = $this->db->where('Kd_Produk', $kode_produk);
		$result = $this->db->where('Kd_Fasilitas', $kode_fasilitas);
		$result = $this->db->get($table_name);
		if ($result->num_rows() > 0) {
			$this->db->where('Kd_Produk', $kode_produk);
			$this->db->where('Kd_Fasilitas', $kode_fasilitas);
			$this->db->update($table_name, $data);
			return true;
		} else {
			return false;
		}
	}

	public function delete_data_fasilitas($table_name, $kode_fasilitas, $kode_produk) {
		$result = $this->db->where('Kd_Fasilitas', $kode_fasilitas);
		$result = $this->db->where('Kd_Produk', $kode_produk);
		$result = $this->db->get($table_name);
		if ($result->num_rows() > 0) {
			$get_data = $result->row();
			$img_dir = './storage_img/img_fasilitas/'.$get_data->Nama_img;
			if (file_exists($img_dir)) {
				unlink($img_dir);
			}
			$this->db->where('Kd_Fasilitas', $kode_fasilitas);
			$this->db->where('Kd_Produk', $kode_produk);
			$this->db->delete($table_name);
			return true;
		} else {
			return false;
		}
	}

}

/* End of file Fasilitas_model.php */
/* Location: ./application/models/Fasilitas_model.php */