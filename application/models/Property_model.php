<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Property_model extends CI_Model {

	public function view_data_property($table_name) {
		$this->db->select('*');
		return $this->db->get($table_name);
	}

	public function view_data_property_by_slug($table_name, $slug_name) {
		$result = $this->db->where('Slug_Property', $slug_name);
		$result = $this->db->get($table_name, 1);

		if ($result->num_rows() > 0) {
			$this->db->select('*');
			$this->db->where('Slug_Property', $slug_name);
			return $this->db->get($table_name, 1)->row();
		} else {
			redirect('page/shop','refresh');
		}
	}

	public function view_data_property_by_kode_property($table_name, $kode_property) {
		$this->db->select('*');
		$this->db->where('Kd_Property', $kode_property);
		return $this->db->get($table_name);
	}

	public function view_data_property_lainnya_by_slug($table_name, $slug_name) {
		$this->db->select('*');
		$this->db->where('Slug_Property !=', $slug_name);
		return $this->db->get($table_name);
	}

	public function count_data_property_by_kode($table_name, $kode_produk) {
		$this->db->select('*');
		$this->db->where('Kd_Produk', $kode_produk);
		return $this->db->get($table_name)->num_rows();
	}

	public function view_data_property_by_kode_produk($table_name, $kode_produk) {
		$this->db->select('*');
		$this->db->where('Kd_Produk', $kode_produk);
		return $this->db->get($table_name);
	}

	public function view_data_group_by_nama_produk($table_name) {
		$this->db->select('Nama_Produk');
		$this->db->group_by('Nama_Produk');
		return $this->db->get($table_name);
	} 

	public function insert_data_property($table_name, $data) {
		$this->db->insert($table_name, $data);
	}

	public function update_data_property($table_name, $kode_property, $kode_produk, $data) {
		$result = $this->db->select('*');
		$result = $this->db->where('Kd_Property', $kode_property);
		$result = $this->db->where('Kd_Produk', $kode_produk);
		$result = $this->db->get($table_name, 1);

		if ($result->num_rows() > 0) {
			$this->db->where('Kd_Produk', $kode_produk);
			$this->db->where('Kd_Property', $kode_property);
			$this->db->update($table_name, $data);
			return true;
		} else {
			return false;
		}
	}

	public function delete_data_property($table_name, $kode_property) {
		$result = $this->db->select('*');
		$result = $this->db->where('Kd_Property', $kode_property);
		$result = $this->db->get($table_name, 1);

		if ($result->num_rows() > 0) {
			$get_data = $result->row();
			$dir = './storage_img/img_cover_property/'.$get_data->Cover_Name;
			if(file_exists($dir)) {
				unlink($dir);
			} 

			$this->db->where('Kd_Property', $kode_property);
			$this->db->delete($table_name);
			return true;
		} else {
			return false;
		}
	}

}

/* End of file Property_model.php */
/* Location: ./application/models/Property_model.php */