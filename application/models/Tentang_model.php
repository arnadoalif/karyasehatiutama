<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tentang_model extends CI_Model {

	public function view_data_tentang($table_name) {
		$this->db->select('*');
		return $this->db->get($table_name, 1)->row();
	}

	public function update_data_tentang($table_name, $kode_tentang, $data) {
		$result = $this->db->where('Kd_Tentang', $kode_tentang);
		$result = $this->db->get($table_name, 1);
		if ($result->num_rows() > 0) {
			$this->db->where('Kd_Tentang', $kode_tentang);
			$this->db->update($table_name, $data);
			return true;
		} else {
			return false;
		}
	}

}

/* End of file Tentang_model.php */
/* Location: ./application/models/Tentang_model.php */