<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Portfolio_model extends CI_Model {

	public function view_data_portfolio($table_name) {
		$this->db->select('*');
		return $this->db->get($table_name);
	}

	public function insert_data_portfolio($table_name, $data) {
		$this->db->insert($table_name, $data);
	}

	public function view_data_by_slug($table_name, $slug_name) {
		$result = $this->db->where('Slug_Url', $slug_name);
		$result = $this->db->get($table_name, 1);
		if ($result->num_rows() > 0) {
			$this->db->where('Slug_Url', $slug_name);
			return $this->db->get($table_name, 1)->row();
		} else {
			redirect('/','refresh');
		}
	}

	public function view_data_portfolio_lainnya_by_slug($table_name, $slug_name) {
		$this->db->select('*');
		$this->db->where('Slug_Url !=', $slug_name);
		return $this->db->get($table_name);
	}

	public function update_data_portfolio($table_name, $kode_data, $data) {
		$result = $this->db->where('Kd_Portfolio', $kode_data);
		$result = $this->db->get($table_name);
		if ($result->num_rows() > 0 ) {
			
			$this->db->where('Kd_Portfolio', $kode_data);
			$this->db->update($table_name, $data);
			return true;
		} else {
			return false;
		}
	}

	public function check_data_portfolio($table_name, $kode_portfolio) {
		$result = $this->db->where('Kd_Portfolio', $kode_portfolio);
		$result = $this->db->get($table_name);
		if ($result->num_rows() > 0 ) {
			return true;
		} else {
			return false;
		}
	}

	public function delete_data_portfolio($table_name, $kode_data) {
		$result = $this->db->where('Kd_Portfolio', $kode_data);
		$result = $this->db->get($table_name);
		if ($result->num_rows() > 0 ) {
			$get_data = $result->row();
			$dir = "./storage_img/img_cover_portfolio/".$get_data->Cover_Img;
			if(file_exists($dir)) {
				unlink($dir);
			}
			$this->db->where('Kd_Portfolio', $kode_data);
			$this->db->delete($table_name);
			return true;
		} else {
			return false;
		}
	}

}

/* End of file Portfolio_model.php */
/* Location: ./application/models/Portfolio_model.php */