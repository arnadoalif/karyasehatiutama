<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery_proyek_model extends CI_Model {

	public function view_data_g_proyek_by_kode_proyek($table_name, $kode_produk) {
		$this->db->select('*');
		return $this->db->get($table_name);
	}

	public function insert_data_g_proyek($table_name, $data) {
		$this->db->insert($table_name, $data);
	}

	public function view_data_g_proyek_by_kode($table_name, $kode_produk, $kode_g_proyek) {
		$result = $this->db->where('Kd_Gproyek', $kode_g_proyek);
		$result = $this->db->where('Kd_Produk', $kode_produk);
		$result = $this->db->get($table_name, 1);

		if ($result->num_rows() > 0) {
			$this->db->where('Kd_Gproyek', $kode_g_proyek);
			$this->db->where('Kd_Produk', $kode_produk);
			return $this->db->get($table_name, 1)->row();
		} else {
			return false;
		}
	}

	public function edit_data_g_proyek($table_name, $kode_produk, $kode_g_proyek, $data) {
		$result = $this->db->where('Kd_Gproyek', $kode_g_proyek);
		$result = $this->db->where('Kd_Produk', $kode_produk);
		$result = $this->db->get($table_name, 1);

		if($result->num_rows() > 0) {
			$this->db->where('Kd_Gproyek', $kode_g_proyek);
			$this->db->where('Kd_Produk', $kode_produk);
			$this->db->update($table_name, $data);
			return true;
		} else{
			return false;
		}
	}

	public function delete_data_g_proyek($table_name, $kode_produk, $kode_g_proyek) {
		$result = $this->db->where('Kd_Gproyek', $kode_g_proyek);
		$result = $this->db->where('Kd_Produk', $kode_produk);
		$result = $this->db->get($table_name, 1);

		if($result->num_rows() > 0) {
			$get_data = $result->row();

			$dir = './storage_img/img_galleri_proyek/'.$get_data->Nama_img;
			if (file_exists($dir)) {
				unlink($dir);
			}
			
			$this->db->where('Kd_Gproyek', $kode_g_proyek);
			$this->db->where('Kd_Produk', $kode_produk);
			$this->db->delete($table_name);
			return true;
		} else{
			return false;
		}
	}

}

/* End of file Gallery_proyek_model.php */
/* Location: ./application/models/Gallery_proyek_model.php */