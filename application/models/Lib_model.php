<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lib_model extends CI_Model {

	public function slug($string, $space="-") {
        $string = utf8_encode($string);

        $string = preg_replace("/[^a-zA-Z0-9 \-]/", "", $string);
        $string = trim(preg_replace("/\\s+/", " ", $string));
        $string = strtolower($string);
        $string = str_replace(" ", $space, $string);

        return $string;
    }

}

/* End of file Lib_model.php */
/* Location: ./application/models/Lib_model.php */