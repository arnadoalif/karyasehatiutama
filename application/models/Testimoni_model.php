<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testimoni_model extends CI_Model {

	public function view_data_testimoni($table_name) {
		$this->db->select('*');
		return $this->db->get($table_name);
	}

	public function insert_data_testimoni($table_name, $data) {
		$this->db->insert($table_name, $data);
	}

	public function edit_data_testimoni($table_name, $kode_data, $data) {
		$result = $this->db->where('Kd_Testimoni', $kode_data);
		$result = $this->db->get($table_name);
		if ($result->num_rows() > 0) {
			$this->db->where('Kd_Testimoni', $kode_data);
			$this->db->update($table_name, $data);
			 return true;
		} else {
			return false;
		}
	}

	public function delete_data_testimoni($table_name, $kode_data) {
		$result = $this->db->where('Kd_Testimoni', $kode_data);
		$result = $this->db->get($table_name);
		if ($result->num_rows() > 0) {
			$data = $this->db->where('Kd_Testimoni', $kode_data);
			$data = $this->db->get($table_name, 1)->row();

			$dir_file = "./storage_img/img_testimoni/".$data->Avatar;
			if (file_exists($dir_file)) {
				unlink($dir_file);
			} 

			$this->db->where('Kd_Testimoni', $kode_data);
			$this->db->delete($table_name);
			return true;
		} else {
			return false;
		}
	}

}

/* End of file Testimoni_model.php */
/* Location: ./application/models/Testimoni_model.php */