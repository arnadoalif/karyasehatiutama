<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Berkas_model extends CI_Model {

	public function view_data_berkas($table_name, $key_col, $kode_data) {
		$this->db->select('*');
		$this->db->where($key_col, $kode_data);
		return $this->db->get($table_name);
	}

	public function view_data_berkas_by_identitas($table_name, $key_col, $kode_data, $identas_name) {
		$this->db->select('*');
		$this->db->where($key_col, $kode_data);
		$this->db->where('Identitas', $identas_name);
		return $this->db->get($table_name);
	}

	public function view_data_one_data_berkas($table_name, $kode_portfolio) {
		$data = $this->db->where('Kd_Portfolio', $kode_portfolio);
		$this->db->order_by('Kd_Berkas', 'desc');
		$data = $this->db->get($table_name, '1')->row();
		return $data->Nama_img;
	}

	public function view_data_berkas_by_col($table_name, $key_col, $kode_data) {
		$this->db->where($key_col, $kode_data);
		return $this->db->get($table_name);
	}

	public function insert_data_berkas($table_name, $data) {
		$this->db->insert($table_name, $data);
	}

	public function update_data_berkas($table_name, $kode_data, $key_col, $kode_key, $data) {
		$result = $this->db->where('Kd_Berkas', $kode_data);
		$result = $this->db->get($table_name);

		if($result->num_rows() > 0) {
			$this->db->where('Kd_Berkas', $kode_data);
			$this->db->where($key_col, $kode_key);
			$this->db->update($table_name, $data);
			return true;
		} else {
			return false;
		}
	}

	public function delete_data_berkas($table_name, $key_col, $kode_berkas, $kode_data) {
		$result = $this->db->where('Kd_Berkas', $kode_berkas);
		$result = $this->db->where($key_col, $kode_data);
		$result = $this->db->get($table_name, 1);

		if ($result->num_rows() > 0) {
			$get_data = $result->row();
			$dir = "";
			if ($key_col == "Kd_Produk") {
				$dir = "./storage_img/img_slide_produk/".$get_data->Nama_img;
			} 

			if (file_exists($dir)) {
				unlink($dir);
			}

			$this->db->where('Kd_Berkas', $kode_berkas);
			$this->db->where($key_col, $kode_data);
			$this->db->delete($table_name);
			return true;
		} else {
			return false;
		}
	}

	public function delete_data_berkas_portfolio($table_name, $key_col, $kode_data) {
		$result = $this->db->where($key_col, $kode_data);
		$result = $this->db->get($table_name);

		if($result->num_rows() > 0) {
			$data_img = $result->result();
			foreach ($data_img as $key => $dt_img) {
				$dir_img = './storage_img/img_slide_portfolio/'.$data_img[$key]->Nama_img;
				if (file_exists($dir_img)) {
					unlink($dir_img);
				}
			}

			$this->db->where($key_col, $kode_data);
			$this->db->delete($table_name);
			
			return true;
		} else {
			return false;
		}
	}

	public function delete_data_berkas_property($table_name, $key_col, $kode_data) {
		$result = $this->db->where($key_col, $kode_data);
		$result = $this->db->get($table_name);

		if($result->num_rows() > 0) {
			$data_img = $result->result();
			foreach ($data_img as $key => $dt_img) {
				$dir_img = './storage_img/img_slide_property/'.$data_img[$key]->Nama_img;
				if (file_exists($dir_img)) {
					unlink($dir_img);
				}
			}
			$this->db->where($key_col, $kode_data);
			$this->db->delete($table_name);
			return true;
		} else {
			return false;
		}
	}

	public function delete_data_berkas_album($table_name, $key_col, $kode_data) {
		$result = $this->db->where($key_col, $kode_data);
		$result = $this->db->get($table_name);

		if($result->num_rows() > 0) {
			$data_img = $result->result();
			foreach ($data_img as $key => $dt_img) {
				$dir_img = './storage_img/img_album/'.$data_img[$key]->Nama_img;
				if (file_exists($dir_img)) {
					unlink($dir_img);
				}
			}
			$this->db->where($key_col, $kode_data);
			$this->db->delete($table_name);
			return true;
		} else {
			return false;
		}
	}

	public function delete_data_berkas_single_img($table_name, $folder_name, $kode_berkas, $name_col, $key_col) {
		$result = $this->db->where('Kd_Berkas', $kode_berkas);
		$result = $this->db->where($name_col, $key_col);
		$result = $this->db->get($table_name);
		if($result->num_rows() > 0) {
			$data_img = $result->row();
				$dir_img = './storage_img/'.$folder_name.'/'.$data_img->Nama_img;
				if (file_exists($dir_img)) {
					unlink($dir_img);
				}
			$this->db->where('Kd_Berkas', $kode_berkas);
			$this->db->where($name_col, $key_col);
			$this->db->delete($table_name);
			return true;
		} else {
			return false;
		}
	}

}

/* End of file Berkas_model.php */
/* Location: ./application/models/Berkas_model.php */