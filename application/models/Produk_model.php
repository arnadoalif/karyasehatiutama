<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk_model extends CI_Model {

	public function view_data_produk($table_name) {
		$this->db->select('Kd_Produk,Nama_Produk,Slug_name,Deskripsi_1,Logo_Produk,Cover_Produk,Status,Brosur_name,Create_At');
		return $this->db->get($table_name);
	}

	public function view_data_produk_by_status($table_name) {
		$this->db->where('Status', '1');
		$this->db->select('Kd_Produk,Nama_Produk,Slug_name,Deskripsi_1,Logo_Produk,Cover_Produk,Status,Create_At');
		return $this->db->get($table_name);
	}

	public function view_data_produk_by_status_non_active($table_name) {
		$this->db->where('Status', '0');
		$this->db->select('Kd_Produk,Nama_Produk,Slug_name,Deskripsi_1,Logo_Produk,Cover_Produk,Status,Create_At');
		return $this->db->get($table_name);
	}

	public function view_data_produk_just_one($table_name, $kode_produk) {
		$this->db->where('Kd_Produk', $kode_produk);
		return $this->db->get($table_name, 1)->row();
	}

	public function view_data_produk_by_slug($table_name, $slug_name) {
		$this->db->select('*');
		$this->db->where('Slug_name', $slug_name);
		return $this->db->get($table_name, 1)->row();
	}

	public function check_data_status_by_slug($table_name, $slug_name) {
		$result = $this->db->where('Slug_name', $slug_name);
		$resutl = $this->db->where('Status', '1');
		$result = $this->db->get($table_name, 1);
		if ($result->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function insert_data_produk($table_name, $data) {
		$this->db->insert($table_name, $data);
	}

	public function update_data_produk($table_name, $kode_produk, $data) {
		$result = $this->db->where('Kd_Produk', $kode_produk);
		$result = $this->db->get($table_name);
		if ($result->num_rows() > 0) {
			$this->db->where('Kd_Produk', $kode_produk);
			$this->db->update($table_name, $data);
			return true;
		} else {
			return false;
		}
	}

	public function update_data_cover_produk($table_name, $kode_produk, $data) {
		$result = $this->db->where('Kd_Produk', $kode_produk);
		$result = $this->db->get($table_name);
		if ($result->num_rows() > 0) {
			$data_img = $result->row();
			$dir_cover = './storage_img/img_cover_produk/'.$data_img->Cover_Produk;
			if (file_exists($dir_cover)) {
				unlink($dir_cover);
			}
			$this->db->where('Kd_Produk', $kode_produk);
			$this->db->update($table_name, $data);
			return true;
		} else {
			return false;
		}
	}

	public function update_data_logo_produk($table_name, $kode_produk, $data) {
		$result = $this->db->where('Kd_Produk', $kode_produk);
		$result = $this->db->get($table_name);
		if ($result->num_rows() > 0) {
			$this->db->where('Kd_Produk', $kode_produk);
			$this->db->update($table_name, $data);
			return true;
		} else {
			return false;
		}
	}

	public function delete_data_produk($table_name, $kode_produk) {
		$result = $this->db->where('Kd_Produk', $kode_produk);
		$result = $this->db->get($table_name);
		if ($result->num_rows() > 0) {
			$data_img = $result->row();
			$dir_cover = './storage_img/img_cover_produk/'.$data_img->Cover_Produk;
			$dir_logo = './storage_img/img_logo_produk/'.$data_img->Logo_Produk;
			if (file_exists($dir_cover)) {
				unlink($dir_cover);
				unlink($dir_logo);
			}
			$this->db->where('Kd_Produk', $kode_produk);
			$this->db->delete($table_name);
			return true;
		} else {
			return false;
		}
	}



}

/* End of file Produk_model.php */
/* Location: ./application/models/Produk_model.php */