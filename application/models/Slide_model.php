<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slide_model extends CI_Model {

	public function view_data_slide($table_name) {
		$this->db->select('*');
		return $this->db->get($table_name);
	}

	public function insert_data_slide($table_name, $data) {
		$this->db->insert($table_name, $data);
	}

	public function update_data_slide($table_name, $kode_key, $data) {
		$this->db->where('Kd_Slide', $kode_key);
		$result = $this->db->get($table_name);

		if($result->num_rows() > 0) {

			$this->db->where('Kd_Slide', $kode_key);
			$this->db->update($table_name, $kode_key);
			return true;
		} else{
			return false;
		}
	}

	public function delete_data_slide($table_name, $kode_key) {
		$result = $this->db->where('Kd_Slide', $kode_key);
		$result = $this->db->get($table_name, 1);
		if ($result->num_rows() > 0) {
			$get_data = $result->row();
			$dir = './storage_img/img_slide/'.$get_data->Cover_name;
			if (file_exists($dir)) {
				unlink($dir);
			} 
			$this->db->where('Kd_Slide', $kode_key);
			$this->db->delete($table_name);
			return true;
		} else {
			return false;
		}
	}

}

/* End of file Slide_model.php */
/* Location: ./application/models/Slide_model.php */