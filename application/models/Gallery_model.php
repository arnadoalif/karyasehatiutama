<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery_model extends CI_Model {

	public function view_data_gallery($table_name) {
		$this->db->select('*');
		return $this->db->get($table_name);
	}

	public function view_data_gallery_by_slug($table_name, $slug_url) {
		$result = $this->db->where('Slug_Url', $slug_url);
		$result = $this->db->get($table_name, 1);
		if ($result->num_rows() > 0) {
			$this->db->where('Slug_Url', $slug_url);
			return $this->db->get($table_name);
		} else {
			return false;
		}
	}

	public function view_groub_kategori_gallery($table_name) {
		$this->db->select('Kategori');
		$this->db->group_by('Kategori');
		return $this->db->get($table_name);
	}

	public function insert_data_gallery($table_name, $data) {
		$this->db->insert($table_name, $data);
	}

	public function update_data_gallery($table_name, $kode_key, $data) {
		$result = $this->db->where('Kd_Gallery', $kode_key);
		$result = $this->db->get($table_name, 1);
		if ($result->num_rows() > 0) {
			$this->db->where('Kd_Gallery', $kode_key);
			$this->db->update($table_name, $data);
			return true;
		} else {
			return false;
		}
	}

	public function check_data_gallery_by_slug($table_name, $slug_url) {
		$result = $this->db->where('Slug_Url', $slug_url);
		$result = $this->db->get($table_name, 1);
		if ($result->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function check_data_gallery($table_name, $kode_gallery) {
		$result = $this->db->where('Kd_Gallery', $kode_gallery);
		$result = $this->db->get($table_name, 1);

		if ($result->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function delete_data_gallery($table_name, $kode_key) {
		$result = $this->db->where('Kd_Gallery', $kode_key);
		$result = $this->db->get($table_name, 1);
		if ($result->num_rows() > 0) {
			$get_data = $result->row();
			$dir = './storage_img/img_cover_gallery/'.$get_data->Cover_Name;
			if (file_exists($dir)) {
				unlink($dir);
			}
			$this->db->where('Kd_Gallery', $kode_key);
			$this->db->delete($table_name);
			return true;
		} else {
			return false;
		}
	}

}

/* End of file Gallery_model.php */
/* Location: ./application/models/Gallery_model.php */