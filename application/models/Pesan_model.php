<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pesan_model extends CI_Model {

	public function view_data_pesan($table_name) {
		$this->db->select('*');
		return $this->db->get($table_name);
	}

	public function view_data_pesan_by_id($table_name, $kode_pesan) {
		$this->db->select('*');
		$this->db->where('Kd_pesan', $kode_pesan);
		return $this->db->get($table_name);
	}

	public function view_data_pesan_by_status($table_name, $status) {
		$this->db->select('*');
		$this->db->where('Status', $status);
		return $this->db->get($table_name);
	}

	public function insert_data_pesan($table_name, $data){
		$this->db->insert($table_name, $data);
	}

	public function update_data_pesan($table_name, $kode_pesan, $data) {
		$result = $this->db->where('Kd_Pesan', $kode_pesan);
		$result = $this->db->get($table_name, 1);
		if ($result > 0) {
			$this->db->where('Kd_pesan', $kode_pesan);
			$this->db->update($table_name, $data);
			return true;
		} else {
			return false;
		}
	}

	public function delete_data_pesan($table_name, $kode_pesan) {
		$result = $this->db->where('Kd_Pesan', $kode_pesan);
		$result = $this->db->get($table_name, 1);

		if ($result->num_rows() > 0) {
			$this->db->where('Kd_pesan', $kode_pesan);
			$this->db->delete($table_name);
			return true;
		} else {
			return false;
		}
		
	}



}

/* End of file Pesan_mode.php */
/* Location: ./application/models/Pesan_model.php */