<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Divisi_model extends CI_Model {

	public function view_data_divisi($table_name) {
		$this->db->select('*');
		return $this->db->get($table_name);
	}

	public function insert_data_divisi($table_name, $data) {
		$this->db->insert($table_name, $data);
	}

	public function edit_data_divisi_nomor_urut($table_name, $kode_divisi, $no_urut_sebelumnya, $data) {
		$result = $this->db->where('Kd_Divisi', $kode_divisi);
		$result = $this->db->get($table_name, 1);
		if ($result->num_rows() > 0) {
			return false;
		} else {
			$this->db->where('Kd_Divisi', $kode_divisi);
			$this->db->update($table_name, $data);
			return true;
		}
	}

	public function update_data_divisi($table_name, $kode_divisi, $data) {
		$result = $this->db->where('Kd_Divisi', $kode_divisi);
		$result = $this->db->get($table_name, 1);
		if ($result->num_rows() > 0) {
			$this->db->where('Kd_Divisi', $kode_divisi);
			$this->db->update($table_name, $data);
			return true;
		} else {
			return false;
		}
	}

	public function delete_data_divisi($table_name, $kode_divisi) {
		$result = $this->db->where('Kd_Divisi', $kode_divisi);
		$result = $this->db->get($table_name, 1);

		if($result->num_rows() > 0) {
			$get_data = $result->row();
			$dir = './storage_img/img_divisi/'.$get_data->Cover_Name;
			if (file_exists($dir)) {
				unlink($dir);
			} 
			$this->db->where('Kd_Divisi', $kode_divisi);
			$this->db->delete($table_name);
			return true;
		} else {
			return false;
		}
	}

}

/* End of file Divisi_model.php */
/* Location: ./application/models/Divisi_model.php */