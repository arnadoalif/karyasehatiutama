<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {

	public function check_login($table_name, $username, $password) {

		$valid = $this->db->where('Username', $username);
		$valid = $this->db->where('Password', md5($password));
		$valid = $this->db->get($table_name);
		if ($valid->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function update_data_log($table_name, $key_log, $username, $password) {
		$this->db->where('Username', $username);
		$this->db->where('password', md5($password));
		$result = $this->db->get($table_name);

		if ($key_log == "login") {
			$data = array(
				'Log_time' => date("Y-m-d H:i:s")
			);
			$this->db->where('Username', $username);
			$this->db->where('password', md5($password));
			$this->db->update($table_name, $data);

			$data_session = array(
				'Kd_Login' => $result->row('Kd_Login'),
				'Username' => $result->row('Username'),
				'Session_log' => 1
			);
			$this->session->set_userdata($data_session);

			return true;
		} else if ($key_log == "logout") {
			$data = array(
				'Out_time' => date("Y-m-d H:i:s")
			);
			$this->db->where('Username', $username);
			$this->db->where('password', md5($password));
			$this->db->update($table_name, $data);

			$this->session->unset_userdata(array('Kd_Login'=> '', 'Username' => '', 'Session_log' => ''));
			session_destroy();
			return;
		} else {
			return false;
		}
	}	

}

/* End of file Login_model.php */
/* Location: ./application/models/Login_model.php */